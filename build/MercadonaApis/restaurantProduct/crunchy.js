"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.data = void 0;
exports.data = [
    {
        title: "Crunchy Big Dog",
        ingredientes: "",
        price: "2.50",
        imagen: null,
        restaurant: "5fe3ddb90e4b1961b7197b24",
        menu: "5fe3eaa10e4b1961b7197b3c",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Super Crunchy",
        ingredientes: "",
        price: "5.50",
        imagen: null,
        restaurant: "5fe3ddb90e4b1961b7197b24",
        menu: "5fe3eaa10e4b1961b7197b3c",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Super Crunchy Dúo",
        ingredientes: "",
        price: "11.00",
        imagen: null,
        restaurant: "5fe3ddb90e4b1961b7197b24",
        menu: "5fe3eaa10e4b1961b7197b3c",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Crunchy Family",
        ingredientes: "",
        price: "15.00",
        imagen: null,
        restaurant: "5fe3ddb90e4b1961b7197b24",
        menu: "5fe3eaa10e4b1961b7197b3c",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Crunchy Dog Pizza",
        ingredientes: "",
        price: "3.00",
        imagen: null,
        restaurant: "5fe3ddb90e4b1961b7197b24",
        menu: "5fe3eaae0e4b1961b7197b3d",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Crunchy Dúo Pizza",
        ingredientes: "",
        price: "7.50",
        imagen: null,
        restaurant: "5fe3ddb90e4b1961b7197b24",
        menu: "5fe3eaae0e4b1961b7197b3d",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Crunchy Fries",
        ingredientes: "",
        price: "2.07",
        imagen: null,
        restaurant: "5fe3ddb90e4b1961b7197b24",
        menu: "5fe3eab80e4b1961b7197b3e",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
];
