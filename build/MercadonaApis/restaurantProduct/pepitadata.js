"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.data = void 0;
exports.data = [
    {
        title: "Patatas gruesas caseras",
        ingredientes: "",
        price: "2.45",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/wuo7txwqxppbgfhnhbdg",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eaf985c1c068931a382f",
        previous_price: "3.50",
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Rianxeira",
        ingredientes: "Queso San Simón gratinado, panceta crujiente, lechuga, tomate y cebolla roja",
        price: "9.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/q4yyrhlkgdqb5pgi8tgl",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eaf985c1c068931a382f",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Sorrentina",
        ingredientes: "Rúcula, mozzarella fresca fundida, orégano, tomate seco en aceite de oliva y cebolla crujiente",
        price: "9.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/gxquetakgvomelva7es9",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eaf985c1c068931a382f",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Ensalada Tropical",
        ingredientes: "Mezclum de lechugas, langostinos salteados, piña a la parrilla, aguacate, papaya y anacardos con vinagreta cítrica",
        price: "10.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/c1r671k0vwvcedloastz",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb3f85c1c068931a3830",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Ensalada d'Anjou",
        ingredientes: "Canónigos y rúcula, pera, almendras tostadas, rabanitos, granada y queso de Mahón con vinagreta de frutos rojos",
        price: "8.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/naimmpbmtjnttsczdxf6",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb3f85c1c068931a3830",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Ensalada de pollo",
        ingredientes: "Mezclum de lechugas, pechuga a la parrilla, Parmesano, bacon y tomate con vinagreta de mostaza y miel",
        price: "8.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/vq1xsk1rpfhtujzap09q",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb3f85c1c068931a3830",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Guanajuatos",
        ingredientes: "Totopos de maíz blanco y azul, guacamole, crema agria, pico de gallo, aceituna negra y jalapeños",
        price: "9.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/xn9mtojrfqrd6nfra3wx",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb3f85c1c068931a3830",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Huevos camperos fritos",
        ingredientes: "Con cecina o jamón de castaña y patatas fritas caseras",
        price: "9.00",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/xbsmmjpinaihocmbtwz0",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb3f85c1c068931a3830",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Croquetas a elegir (8 uds.)",
        ingredientes: "Combina dos variedades entre, centolla al azafrán, ternera guisada a la cerveza negra o espinacas al ajillo",
        price: "8.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/j2rqapypnqeqgikbr3e2",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb3f85c1c068931a3830",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Langostinos empanados",
        ingredientes: "Langostinos empanados con salsa dulce y picante",
        price: "9.00",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/ce2dbabneewegah9eto7",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb3f85c1c068931a3830",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Corleone",
        ingredientes: "Pesto rojo, lascas de queso Parmesano y rúcula",
        price: "10.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/b3g6fvgzhsfhnsm0icie",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Rouge",
        ingredientes: "Foie fresco a la parrilla, manzana caramelizada, lechuga y tomate",
        price: "10.75",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/z8vupr37id9dylqweei9",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Pampera",
        ingredientes: "Queso provolone, chimichurri, lechuga, tomate y cebolla roja en pan artesano",
        price: "9.95",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/fg0iyaa3p9avdyqn22sk",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Rianxeira",
        ingredientes: "Queso San Simón gratinado, panceta crujiente, lechuga, tomate y cebolla roja",
        price: "9.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/q4yyrhlkgdqb5pgi8tgl",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Chaparrita",
        ingredientes: "Guacamole, jalapeños, bacon, lechuga, tomate y cebolla roja",
        price: "10.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/urfeh9up2mpwqkgu48g3",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Cheese Burger",
        ingredientes: "Queso cheddar",
        price: "7.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/bnj3mmsokoq414gtvn6o",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Sorrentina",
        ingredientes: "Rúcula, mozzarella fresca fundida, orégano, tomate seco en aceite de oliva y cebolla crujiente",
        price: "9.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/gxquetakgvomelva7es9",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Reilly",
        ingredientes: "Bacon, pepinillos, queso cheddar, salsa barbacoa, lechuga, tomate y cebolla roja",
        price: "9.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/mnmksqb2elf2oiifcgb1",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "La Pepita",
        ingredientes: "Queso cheddar, lechuga, tomate y cebolla roja en pan artesano",
        price: "6.26",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/lkndtsjr6nwfdraje7mu",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb4e85c1c068931a3831",
        previous_price: "8.95",
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Summum",
        ingredientes: "Queso azul, lechuga, tomate y cebolla caramelizada en pan artesano",
        price: "15.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/hd9rjj8ssa0avok1sndu",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb5b85c1c068931a3832",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Manteca",
        ingredientes: "Doble de queso cheddar, lechuga, tomate y cebolla roja",
        price: "15.25",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/tmnmojzfdz4lqirkax14",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb5b85c1c068931a3832",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Gocha",
        ingredientes: "Burger de cochinillo (150gr), torreznos, chutney de tomate, salsa de naranja, hierbas provenzales y espinaca",
        price: "12.95",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/vllvzewgptmwqwved2cg",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb6a85c1c068931a3833",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Laponia",
        ingredientes: "Burger de salmón fresco a la parrilla con salsa tártara, lechuga y tomate",
        price: "10.95",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/ca1mpyse81kij9tz09fg",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb6a85c1c068931a3833",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Pita",
        ingredientes: "Contramuslo de pollo de corral marinado en especias cajún, cheddar, lechuga, tomate y cebolla caramelizada en pan artesano",
        price: "8.95",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/n5xtakvulbrfacras64o",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb6a85c1c068931a3833",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Hindi",
        ingredientes: "Burger de pollo de corral, salsa de curry y mango, anacardos, lechuga, tomate y cebolla roja",
        price: "8.95",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/uahwoe4f4be5s8bqdw9o",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb6a85c1c068931a3833",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Vegana",
        ingredientes: "Burger vegana, queso vegano, lechuga, tomate y cebolla en pan artesano",
        price: "12.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/smotljcb4xtmyll9urch",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb6a85c1c068931a3833",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Serranita",
        ingredientes: "Burger de cerdo ibérico, crema de queso manchego, crujiente de jamón serrano, lechuga, tomate y cebolla roja",
        price: "10.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/lpjgo5vquhtkli6mxh6y",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb6a85c1c068931a3833",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita Bonita",
        ingredientes: "Burger de atún, crema de soja, wasabi, canónigos, aguacate  y perlas de aceite picante",
        price: "15.00",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/fisg1114hdqwqnlhpb4q",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb6a85c1c068931a3833",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pepita pulgarcita",
        ingredientes: "Burger de ternera (100 g.) con queso cheddar, acompañado de patatas fritas",
        price: "5.25",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/arcvkhohpnaj2vyyw8oj",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0eb9b85c1c068931a3834",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Dos salsas a elegir",
        ingredientes: "",
        price: "0.75",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/rpqxjgyecvhvrznsubmq",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebaa85c1c068931a3835",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Brotes verdes",
        ingredientes: "",
        price: "2.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/hwpiev2jx12iwn9eqamh",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebaa85c1c068931a3835",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Patatas gruesas caseras",
        ingredientes: "",
        price: "2.45",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/wuo7txwqxppbgfhnhbdg",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebaa85c1c068931a3835",
        previous_price: "3.50",
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Patatas finas",
        ingredientes: "",
        price: "2.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/jky1xdqkxj3qz0vbsmep",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebaa85c1c068931a3835",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Yuca frita",
        ingredientes: "",
        price: "3.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/ry1rhyveuemzbgkbk1tu",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebaa85c1c068931a3835",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Coulant",
        ingredientes: "Bizcocho de chocolate con crema de avellana",
        price: "5.00",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/ycwzubcl4mmpdvbuigle",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebb485c1c068931a3836",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Brownie",
        ingredientes: "Brownie de chocolate belga con helado de vainilla",
        price: "2.73",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/uaupha8uos7zjwkjaxs7",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebb485c1c068931a3836",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Crumble",
        ingredientes: "Crumble de manzana asada con sorbete de mora",
        price: "3.90",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/hhlwmx7am40xcjjwl1yv",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebb485c1c068931a3836",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Pudding de tofee",
        ingredientes: "Bizcocho cremoso y caliente recubierto de toffee con helado de plátano",
        price: "4.50",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/imvg7qb93o3xwniilzln",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebb485c1c068931a3836",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "CERVEZA MICA SIN GLUTEN",
        ingredientes: "",
        price: "3.00",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "CERVEZA MICA ORO",
        ingredientes: "",
        price: "3.00",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "CERVEZA GROLSCHRADLER 33 CL",
        ingredientes: "",
        price: "2.50",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "CERVEZA ERDINGER WEISSBIER HEFE 50 CL",
        ingredientes: "",
        price: "3.75",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Blanco Rueda Mocén verdejo Seleción Especial",
        ingredientes: "",
        price: "15.00",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "ABADÍA DE SAN QUIRCE ROBLE (RIB. DUERO)",
        ingredientes: "",
        price: "15.00",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Agua (50cl)",
        ingredientes: "",
        price: "1.50",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Coca-Cola Sabor Original lata 330ml.",
        ingredientes: "",
        price: "1.80",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/vbvo5arvbz6i91fsapji",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Coca-Cola Zero Azúcar lata 330ml.",
        ingredientes: "",
        price: "1.80",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/kqt7x3rf7fresbliplif",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Aquarius Limón lata 330ml.",
        ingredientes: "",
        price: "2.00",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/utjrnlypz8wjmkderm6o",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Fanta Naranja lata 330ml.",
        ingredientes: "",
        price: "1.80",
        imagen: "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/oqjxkidumivf2oevi9fi",
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sidra Maeloc (33cl)",
        ingredientes: "Sidra ecológica gallega",
        price: "2.10",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Zumo de piña (20 cl)",
        ingredientes: "",
        price: "1.80",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Zumo de melocotón (20 cl)",
        ingredientes: "",
        price: "1.80",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Estrella Galicia (33cl)",
        ingredientes: "",
        price: "1.80",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Cerveza artesana La Pepita (33cl)",
        ingredientes: "",
        price: "2.25",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Cerveza Estrella Galicia sin Gluten (33cl)",
        ingredientes: "",
        price: "2.10",
        imagen: null,
        restaurant: "5fe49e81a55336531bb12a4d",
        menu: "5ff0ebc485c1c068931a3837",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
];
