"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.data = void 0;
exports.data = [
    {
        title: "Nº1",
        ingredientes: "Lomo, Queso.",
        price: "4.75 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº2",
        ingredientes: "Lomo, Queso y Pimientos.",
        price: "4.95 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº3",
        ingredientes: "Lomo, Bacón, Huevos y Alegría.",
        price: "5.15 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº4",
        ingredientes: "Lomo, Bacón y Queso",
        price: "5.20 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº5",
        ingredientes: "Lomo, Bacón, Lechuga y Mayonesa.",
        price: "5.20 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº7",
        ingredientes: "Bacón y Queso.",
        price: "4.65 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº8",
        ingredientes: "Bacón, Pollo, Lechuga y Mayonesa.",
        price: "5.10 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº10",
        ingredientes: "Bacón y Pollo.",
        price: "4.95 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº14",
        ingredientes: "Jamón Serrano, Tomate y Aceite Virgen.",
        price: "4.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº15",
        ingredientes: "Pollo, Curry, Lech, Vinagreta y Pepinillo.",
        price: "4.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº17",
        ingredientes: "Pollo, Lechuga, Tomate y Mayonesa.",
        price: "5.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº18",
        ingredientes: "Pollo, Cebolla, Pimiento y Queso.",
        price: "5.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº20",
        ingredientes: "Lechuga, Tomate, Bonito, Huevo y Mayonesa.",
        price: "4.20 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº21",
        ingredientes: "Calamares.",
        price: "4.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº22",
        ingredientes: "Pollo Empanado, Queso Cabra y Cebolla.",
        price: "5.25 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Nº25",
        ingredientes: "Bonito y Anchoas.",
        price: "4.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afb28427f911d22b12f4",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Bravas Doble",
        ingredientes: "",
        price: "3.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afbc8427f911d22b12f5",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Calamares Fritos",
        ingredientes: "",
        price: "6.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afbc8427f911d22b12f5",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Calamares Bravos",
        ingredientes: "",
        price: "6.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afbc8427f911d22b12f5",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Alitas Barbacoa (12 uds.)",
        ingredientes: "",
        price: "7.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afbc8427f911d22b12f5",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Fingers De Pollo",
        ingredientes: "",
        price: "7.35 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afbc8427f911d22b12f5",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Perrito  (Salchicha y dos salsas)",
        ingredientes: "(Salchicha y dos salsas)",
        price: "3.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afc98427f911d22b12f6",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Perrito Completo",
        ingredientes: "Salchicha, dos salsas, queso, bacón, cebolla, virutas de patata, vinagreta",
        price: "3.90 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afc98427f911d22b12f6",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Perrito XXL  (Salchicha y dos salsas)",
        ingredientes: "(Salchicha y dos salsas)",
        price: "5.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afc98427f911d22b12f6",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Perrito Completo XXL",
        ingredientes: "Salchicha, dos salsas, queso, bacón, cebolla, virutas de patata, vinagreta",
        price: "7.20 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afc98427f911d22b12f6",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Hamburguesa de la casa",
        ingredientes: "Con lechuga, tomate, cebolla, pepinillo, cheddar y bacon",
        price: "4.60 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afd48427f911d22b12f7",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Hamburguesa Bugerpool",
        ingredientes: "Con lechuga, tomate, cebolla, pepinillo, cheddar, bacon y huevo frito",
        price: "5.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afd48427f911d22b12f7",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Hamburguesa rústica",
        ingredientes: "Hamburguesa en pan, tomate, bacon, cheddar y huevo frito",
        price: "5.10 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afd48427f911d22b12f7",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Hamburguesa doble de carne",
        ingredientes: "Hamburguesa de vaca (200g.) ,cheddar, cebolla y bacon",
        price: "5.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afd48427f911d22b12f7",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Hamburguesas vacuno extra",
        ingredientes: "Hamburguesa de vaca (200 g.), lechuga, tomate, bacon, cebolla, cheddar  y viruta de patata",
        price: "5.90 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afd48427f911d22b12f7",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Hamburguesa infierno",
        ingredientes: "Hamburguesa de vaca (200 g.), cheddar, bacon, cebolla, pepinillo, chile picante y huevo frito",
        price: "6.10 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afd48427f911d22b12f7",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Hamburguesas de buey",
        ingredientes: "Hamburguesa de buey (200 g.), tomate plancha, cebolla caramelizada y bacon",
        price: "7.20 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afd48427f911d22b12f7",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Duo",
        ingredientes: "Jamón york y queso",
        price: "3.30 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Pechugón",
        ingredientes: "Pollo, lechuga, queso, jamón serrano y mayonesa",
        price: "4.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Trío",
        ingredientes: "Jamón york, queso y huevo frito",
        price: "3.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Revolcón",
        ingredientes: "Con tomate, lechuga, cebolla, bonito, huevo cocido y mayonesa",
        price: "4.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Bacanal",
        ingredientes: "Con tomate, lechuga, cebolla, york, queso y huevo frito",
        price: "4.40 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Pollón",
        ingredientes: "Pollo, huevo frito, lechuga, tomate y mayonesa",
        price: "4.60 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Paquete",
        ingredientes: "1º piso: Lechuga, tomate y bonito 2º piso: Jamón york, queso, huevo cocido y mayonesa",
        price: "5.30 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Huevón",
        ingredientes: "huevo frito, bacon y queso",
        price: "3.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich Huevón Completo",
        ingredientes: "Huevo frito, lomo adobado, pimiento verde y cebolla",
        price: "4.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Sándwich XXL",
        ingredientes: "1º piso: Jamón york, queso, lechuga y tomate 2º piso: Pollo, bacon, huevo frito y mayonesa",
        price: "5.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afe28427f911d22b12f8",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Fº1",
        ingredientes: "Pollo +Queso De Cabra +Cebolla.",
        price: "3.90 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afec8427f911d22b12f9",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Fº4",
        ingredientes: "Bacón +Huevo Frito +Cheddar.",
        price: "3.90 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afec8427f911d22b12f9",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Fº5",
        ingredientes: "Carne Picada +Cebolla Caramelizada +Salsa De Tomate Picante.",
        price: "3.90 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012afec8427f911d22b12f9",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Tarta de queso casera",
        ingredientes: "",
        price: "3.10 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012aff58427f911d22b12fa",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Brownie de chocolate casero",
        ingredientes: "",
        price: "3.30 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012aff58427f911d22b12fa",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Choco fudge brown (465 ml.)",
        ingredientes: "",
        price: "8.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012aff58427f911d22b12fa",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Chunky monkey (465 ml.)",
        ingredientes: "",
        price: "8.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012aff58427f911d22b12fa",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Cookie dough (465 ml.)",
        ingredientes: "",
        price: "8.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012aff58427f911d22b12fa",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Straw cheesecake (465 ml.)",
        ingredientes: "",
        price: "8.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012aff58427f911d22b12fa",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Botella de Agua Pequeña (33 cl.)",
        ingredientes: "",
        price: "1.15 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Agua Con Gas (25 cl. )",
        ingredientes: "",
        price: "1.60 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Batido Vainilla (195 ml.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Batido Fresa (195 ml.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Batido Chocolate (195 ml.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Zumo Pago Piña ( 20 cl.)",
        ingredientes: "",
        price: "1.85 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Zumo Pago Melocotón ( 20 cl.)",
        ingredientes: "",
        price: "1.85 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Zumo Pago Naranja ( 20 cl.)",
        ingredientes: "",
        price: "1.85 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Zumo Pago Pera ( 20 cl.)",
        ingredientes: "",
        price: "1.85 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Bitter Kas (20 cl.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Kas Naranja Lata ( 33 cl.)",
        ingredientes: "",
        price: "1.85 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Coca-Cola Lata (33 cl.)",
        ingredientes: "",
        price: "1.85 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Aquarius Naranja Lata (33 cl.)",
        ingredientes: "",
        price: "1.95 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Aquarius Limón Lata (33 cl.)",
        ingredientes: "",
        price: "1.95 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Coca-Cola Zero Lata ( 33 cl.)",
        ingredientes: "",
        price: "1.85 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Tonica Schweppes (20 cl.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Bi Frutas Pacífico ( 33 cl.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Bi Frutas Mediterraneo (33 cl.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Bi Frutas Tropical (33 cl.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Bi Solan (33 cl.)",
        ingredientes: "",
        price: "2.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Kas Limón (1.25 lt.)",
        ingredientes: "",
        price: "2.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0028427f911d22b12fb",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "San Miguel 1516 (33 cl.)",
        ingredientes: "",
        price: "2.40 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b00c8427f911d22b12fc",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Mahou 5 Estrellas Lata (33 cl.)",
        ingredientes: "",
        price: "1.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b00c8427f911d22b12fc",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "San Miguel Sin Gluten (33 cl.)",
        ingredientes: "",
        price: "2.20 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b00c8427f911d22b12fc",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "San Miguel Manila (33 cl.)",
        ingredientes: "",
        price: "2.40 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b00c8427f911d22b12fc",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Alhambra Verde (33 cl.)",
        ingredientes: "",
        price: "2.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b00c8427f911d22b12fc",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Alhambra Roja (33 cl.)",
        ingredientes: "",
        price: "2.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b00c8427f911d22b12fc",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Corona (355 ml.)",
        ingredientes: "",
        price: "2.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b00c8427f911d22b12fc",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Ipa Mahou (33 cl.)",
        ingredientes: "",
        price: "3.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b00c8427f911d22b12fc",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Val Aranda Joven (75 cl.) Ribera Del Duero",
        ingredientes: "",
        price: "12.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Añares (75 cl.) Rioja Crianza",
        ingredientes: "",
        price: "12.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "David Sebastian Joven (75 cl.) Ribera Del Duero",
        ingredientes: "",
        price: "12.90 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "David Sebastian 86400 (75 cl.) Ribera Del Duero",
        ingredientes: "",
        price: "16.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Carmelo Rodero 9 Meses (75 cl.) Ribera Del Duero",
        ingredientes: "",
        price: "16.90 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Vega Cubillas Roble (75 cl.) Ribera Del Duero",
        ingredientes: "",
        price: "14.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Heraclio Alfaro (75 cl.) Rioja Crianza",
        ingredientes: "",
        price: "14.30 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Lerma Crianza (75 cl.) Ribera Del Arlanza",
        ingredientes: "",
        price: "15.70 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Bordon (75 cl.) Rioja Crianza",
        ingredientes: "",
        price: "13.30 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Cune (75 cl.). Rioja Crianza",
        ingredientes: "",
        price: "13.60 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Valle De Nabal 9 Mesas (75 cl.) Ribera Del Duero",
        ingredientes: "",
        price: "17.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b0158427f911d22b12fd",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "X Siglos (75 cl.) Rueda Verdejo",
        ingredientes: "",
        price: "12.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b01e8427f911d22b12fe",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Martí Villí (75 cl.) Rueda Verdejo",
        ingredientes: "",
        price: "14.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b01e8427f911d22b12fe",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Yllera 5.5 (75 cl.) Espumoso",
        ingredientes: "",
        price: "15.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b01e8427f911d22b12fe",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Marqués Del Riscal Orgánico (75 cl.) Rueda Verdejo",
        ingredientes: "",
        price: "15.80 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b01e8427f911d22b12fe",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "José Pariente (75 cl.) Rueda Verdejo",
        ingredientes: "",
        price: "16.50 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b01e8427f911d22b12fe",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Caraballas (75 cl.) Ecológico Verdejo",
        ingredientes: "",
        price: "17.20 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b01e8427f911d22b12fe",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Vega Cubillas (75 cl.) Rosado Ribera Del Duero",
        ingredientes: "",
        price: "12.00 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b02a8427f911d22b12ff",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "David Moreno (75 cl.) Rosado Rioja",
        ingredientes: "",
        price: "13.90 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b02a8427f911d22b12ff",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Marqués Del Riscal (75 cl.) Rosado Rioja",
        ingredientes: "",
        price: "14.20 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b02a8427f911d22b12ff",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
    {
        title: "Hoyo De La Vega (75 cl.) Rosado Ribera Del Duero",
        ingredientes: "",
        price: "14.40 ",
        imagen: null,
        restaurant: "6012add08427f911d22b12f2",
        menu: "6012b02a8427f911d22b12ff",
        previous_price: null,
        oferta: false,
        popular: false,
        news: false,
    },
];
