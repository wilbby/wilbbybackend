"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const menu_1 = __importDefault(require("../models/nemu/menu"));
const platos_1 = __importDefault(require("../models/nemu/platos"));
const fetch = require("node-fetch");
const Bluebird = require("bluebird");
fetch.Promise = Bluebird;
class MercadonaRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    createProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query;
            yield fetch(`https://tienda.mercadona.es/api/categories/${id.id}?lang=es&wh=mad`)
                .then((res) => res.json())
                .then((datos) => {
                datos.categories.forEach(function (item) {
                    item.products.forEach(function (product) {
                        const produto = new platos_1.default({
                            title: product.display_name,
                            ingredientes: `${product.packaging} ${product.price_instructions.unit_size} ${product.price_instructions.reference_format} | ${product.price_instructions.bulk_price}€ /${product.price_instructions.reference_format}`,
                            price: product.price_instructions.unit_price,
                            imagen: product.thumbnail,
                            restaurant: "5fa4042e54923e6c9709da54",
                            menu: "5fa58205e24e003a21c6a3fc",
                            oferta: false,
                            popular: false,
                            news: product.price_instructions.is_new,
                        });
                        //produto.save();
                        console.log(product);
                    });
                });
            });
        });
    }
    createCategory(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            fetch("https://tienda.mercadona.es/api/categories/?lang=es&wh=mad1")
                .then((res) => res.json())
                .then((datos) => {
                datos.results.forEach(function (item) {
                    item.categories.forEach((element) => {
                        const menu = new menu_1.default({
                            title: element.name,
                            subtitle: "Todos los productos",
                            restaurant: "5fb24df8c292c23bed148076",
                        });
                        //menu.save();
                    });
                });
            });
        });
    }
    routes() {
        this.router.get("/create-product-mercadona", this.createProduct);
        this.router.get("/create-category-mercadona", this.createCategory);
    }
}
const mercadonaRouter = new MercadonaRouter();
mercadonaRouter.routes();
exports.default = mercadonaRouter.router;
