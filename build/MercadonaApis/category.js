"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const subcollection_1 = __importDefault(require("../models/subcollection"));
const platos_1 = __importDefault(require("../models/nemu/platos"));
const fetch = require("node-fetch");
const Bluebird = require("bluebird");
fetch.Promise = Bluebird;
class MercadonaRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    createProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query;
            const cat = req.query;
            yield fetch(`https://tienda.mercadona.es/api/categories/${id.id}?lang=es&wh=mad`)
                .then((res) => res.json())
                .then((datos) => {
                datos.categories.forEach(function (item) {
                    item.products.forEach(function (product) {
                        const produto = new platos_1.default({
                            title: product.display_name,
                            ingredientes: `${product.packaging} ${product.price_instructions.unit_size} ${product.price_instructions.reference_format} | ${product.price_instructions.bulk_price}€ /${product.price_instructions.reference_format}`,
                            price: product.price_instructions.unit_price,
                            imagen: product.thumbnail.replace("?fit=crop&h=300&w=300", "?fit=crop&h=600&w=600"),
                            restaurant: "5fb24df8c292c23bed148076",
                            menu: cat.cat,
                            oferta: false,
                            popular: false,
                            news: product.price_instructions.is_new,
                            previous_price: null,
                        });
                        //produto.save();
                        res.status(200).end();
                    });
                });
            });
        });
    }
    createCategory(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            fetch("https://tienda.mercadona.es/api/categories/?lang=es&wh=mad1")
                .then((res) => res.json())
                .then((datos) => {
                datos.results[25].categories.forEach(function (item) {
                    const menu = new subcollection_1.default({
                        title: item.name,
                        collectiontype: "604e4f5073d989bfa541f879",
                    });
                    //enu.save();
                    res.status(200).end();
                });
            });
        });
    }
    routes() {
        this.router.get("/create-product-mercadona", this.createProduct);
        this.router.get("/create-category-mercadona", this.createCategory);
    }
}
const mercadonaRouter = new MercadonaRouter();
mercadonaRouter.routes();
exports.default = mercadonaRouter.router;
