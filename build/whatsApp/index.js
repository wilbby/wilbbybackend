"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
const accountSid = process.env.accountSid;
const authToken = process.env.authToken;
const client = require("twilio")(accountSid, authToken);
class WhatsAppRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    sendWhatsApp(req, res) {
        client.messages
            .create({
            from: "whatsapp:+15102983479",
            body: "Hola hemos recibido tu pedido y vamos a por el para hacerte la entrega",
            to: "whatsapp:+34664028161",
        })
            .then((message) => {
            res.status(200).json(message).end();
        });
    }
    routes() {
        this.router.get("/whatsappp", this.sendWhatsApp);
        this.router.post("/whatsapp-hook", (res, req) => {
            console.log(req.ServerResponse);
        });
    }
}
const whatsAppRouterRouter = new WhatsAppRouter();
whatsAppRouterRouter.routes();
exports.default = whatsAppRouterRouter.router;
