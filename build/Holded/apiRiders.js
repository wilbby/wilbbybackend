"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setOrderToHolded = void 0;
const request_1 = __importDefault(require("request"));
const config_1 = require("./config");
const dataRiders_1 = require("./dataRiders");
const order_1 = __importDefault(require("../models/order"));
exports.setOrderToHolded = (data) => {
    var options = {
        method: "POST",
        url: config_1.urlCompra,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            key: config_1.key,
        },
        body: JSON.stringify(dataRiders_1.datasHoldedCompra(data)),
    };
    request_1.default(options, function (error, response) {
        if (error)
            throw new Error(error);
        const respo = JSON.parse(response.body);
        order_1.default.findOneAndUpdate({ _id: data._id }, {
            $set: {
                holdedRidersID: respo.id,
            },
        }, (err, order) => { });
    });
};
