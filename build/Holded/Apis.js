"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setOrderToHolded = void 0;
const request_1 = __importDefault(require("request"));
const config_1 = require("./config");
const data_1 = require("./data");
const order_1 = __importDefault(require("../models/order"));
const getTikectPDF_1 = require("./getTikectPDF");
const dataCompra_1 = require("./dataCompra");
exports.setOrderToHolded = (data) => {
    var options = {
        method: "POST",
        url: config_1.url,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            key: config_1.key,
        },
        body: JSON.stringify(data_1.datasHolded(data)),
    };
    var optionsCompra = {
        method: "POST",
        url: config_1.urlCompra,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            key: config_1.key,
        },
        body: JSON.stringify(dataCompra_1.datasHoldedCompra(data)),
    };
    request_1.default(options, function (error, response) {
        if (error)
            throw new Error(error);
        const resp = JSON.parse(response.body);
        var optionsPay = {
            method: "POST",
            url: `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${resp.id}/pay`,
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                key: config_1.key,
            },
            body: JSON.stringify({
                date: data.created_at,
                amount: Number(data.total),
            }),
        };
        request_1.default(optionsCompra, function (error, response) {
            if (error)
                throw new Error(error);
            const respo = JSON.parse(response.body);
            order_1.default.findOneAndUpdate({ _id: data._id }, {
                $set: {
                    holdedPartnerID: respo.id,
                },
            }, (err, order) => { });
        });
        request_1.default(optionsPay, function (error, responses) {
            if (error)
                throw new Error(error);
            const url = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${resp.id}/send`;
            const optiones = {
                method: "POST",
                url: url,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    key: config_1.key,
                },
                body: JSON.stringify({
                    emails: data.userData.email,
                    subject: `Wilbby - Factura de tu pedido en ${data.StoreData.title}`,
                    message: `Hola ${data.userData.name} aquí tiene los detalle y la factura de tu pedido en Wilbby`,
                }),
            };
            request_1.default(optiones, function (error, respon) {
                if (error)
                    throw new Error(error);
            });
        });
        order_1.default.findOneAndUpdate({ _id: data._id }, {
            $set: {
                holdedID: resp.id,
            },
        }, (err, order) => { });
        getTikectPDF_1.getInvoicePDF(resp.id, data._id, data.display_id);
    });
};
