"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.datasHolded = void 0;
exports.datasHolded = (data) => {
    const tot = Number(data.envio) + Number(data.tarifa);
    const shipping = tot / data.platos.length;
    const items = data.platos.map((element) => {
        const price = Number(element.plato.price) + shipping;
        const item = {
            name: element.plato.title,
            desc: element.plato.ingredientes ? element.plato.ingredientes : "",
            units: element.plato.cant,
            subtotal: price,
        };
        return item;
    });
    return {
        applyContactDefaults: false,
        contactName: `${data.userData.name} ${data.userData.lastName}`,
        contactEmail: data.userData.email,
        contactCity: data.userData.city,
        desc: "Pedido Wilbby",
        date: data.created_at,
        notes: "Gatos de envío y tarifas de servicio incluido en el total",
        items: items,
        invoiceNum: Number(data.display_id),
        currency: "EUR",
        paymentMethodId: "605671642ca976325330606f",
    };
};
