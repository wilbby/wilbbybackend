"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getInvoicePDF = void 0;
const request_1 = __importDefault(require("request"));
const config_1 = require("./config");
const fs_1 = __importDefault(require("fs"));
const order_1 = __importDefault(require("../models/order"));
exports.getInvoicePDF = (id, orderID, order_display_ID) => {
    const options = {
        method: "GET",
        url: `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${id}/pdf`,
        headers: { Accept: "application/json", key: config_1.key },
    };
    request_1.default(options, function (error, respon) {
        if (error)
            throw new Error(error);
        const dat = JSON.parse(respon.body);
        const pdfBlob = "data:application/pdf;base64," + dat.data;
        const matches = pdfBlob.match(/^data:.+\/(.+);base64,(.*)$/);
        //@ts-ignore
        const ext = matches[1];
        //@ts-ignore
        const base64_data = matches[2];
        const buffer = Buffer.from(base64_data, "base64");
        const filename = `${order_display_ID}.${ext}`;
        const filenameWithPath = `${__dirname}/../../uploads/invoice/${filename}`;
        fs_1.default.writeFileSync(filenameWithPath, buffer, "binary");
        order_1.default.findOneAndUpdate({ _id: orderID }, {
            $set: {
                invoiceUrl: `https://api.wilbby.com/assets/invoice/${filename}`,
            },
        }, (err, order) => { });
    });
};
