"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.activateAccount = void 0;
const riders_1 = __importDefault(require("../models/riders"));
const crypto_1 = __importDefault(require("crypto"));
exports.activateAccount = (email) => {
    const token = crypto_1.default.randomBytes(20).toString("hex");
    var newvalues = { $set: { forgotPasswordToken: token } };
    riders_1.default.findOneAndUpdate({ email: email }, newvalues, {
        //options
        new: true,
        strict: false,
        useFindAndModify: false,
    }, (err, updated) => { });
};
