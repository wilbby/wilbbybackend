"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.facturaEmail = void 0;
const nodemailer = require("nodemailer");
const facturacliente_1 = __importDefault(require("./facturacliente"));
exports.facturaEmail = (data) => {
    const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        service: "gmail",
        port: 465,
        secure: true,
        auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: process.env.EMAIL_PASSWORD,
        },
    });
    const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: data.userData.email,
        subject: `WILBBY - Detalles de tu pedido en ${data.StoreData.title}`,
        text: `WILBBY - Detalles de tu pedido en ${data.StoreData.title}`,
        html: facturacliente_1.default(data),
    };
    transporter.sendMail(mailOptions);
};
