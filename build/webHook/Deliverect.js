"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setOrderToDeliverect = void 0;
const request_1 = __importDefault(require("request"));
const config_1 = require("./config");
const data_1 = require("./data");
exports.setOrderToDeliverect = (data, token, token_type) => {
    var options = {
        method: "POST",
        url: `${config_1.DeliverectApi}/${config_1.ChannelName}/order/${data.StoreData.channelLinkId}`,
        headers: {
            "Content-Type": "application/json",
            Authorization: `${token_type} ${token}`,
        },
        body: JSON.stringify(data_1.dataOrder(data)),
    };
    request_1.default(options, function (error, response) {
        if (error)
            throw new Error(error);
        console.log(response.body);
    });
};
