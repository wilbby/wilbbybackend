"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ordenUpdate = void 0;
const order_1 = __importDefault(require("../models/order"));
exports.ordenUpdate = (orderID, estado, progreso, status) => {
    order_1.default.findOneAndUpdate({ _id: orderID }, {
        $set: {
            estado: estado,
            progreso: progreso,
            status: status,
        },
    }, (err, order) => { });
};
