"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const cuponesSchema = new mongoose_1.default.Schema({
    clave: { type: String, unique: true },
    descuento: Number,
    tipo: {
        type: String,
        enum: ["porcentaje", "dinero"],
        default: "porcentaje",
    },
});
exports.default = mongoose_1.default.model("cupones", cuponesSchema);
