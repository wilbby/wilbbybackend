"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const cardSchema = new mongoose_1.default.Schema({
    userId: {
        type: String,
        required: true,
    },
    restaurant: {
        type: String,
        required: true,
    },
    plato: {
        type: mongoose_1.default.Schema.Types.Mixed,
        required: true,
    },
    complementos: [mongoose_1.default.Schema.Types.Mixed],
    platoID: {
        type: String,
    },
    total: {
        type: String,
    },
    extra: {
        type: String,
    },
});
exports.default = mongoose_1.default.model("card", cardSchema);
