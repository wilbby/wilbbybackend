"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const MenuSchema = new mongoose_1.default.Schema({
    user: {
        type: String,
    },
    comment: {
        type: String,
        required: true,
        text: true,
    },
    value: {
        type: Number,
        required: true,
    },
    restaurant: {
        type: String,
        required: true,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("menu", MenuSchema);
