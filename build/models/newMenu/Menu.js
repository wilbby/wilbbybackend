"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const MenuSchema = new mongoose_1.default.Schema({
    menu: { type: String },
    menuId: { type: String },
    description: { type: String },
    menuImageURL: { type: String },
    menuType: { type: Number },
    availabilities: { type: [String] },
    snoozedProducts: { type: mongoose_1.default.Schema.Types.Mixed },
    productTags: { type: [Number] },
    currency: { type: Number },
    validations: { type: [String] },
    nestedModifiers: { type: Boolean },
    channelLinkId: { type: String },
    storeId: { type: String },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("newmenu", MenuSchema);
