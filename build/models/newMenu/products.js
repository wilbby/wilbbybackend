"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const productSchema = new mongoose_1.default.Schema({
    name: { type: String, text: true },
    description: { type: String, text: true },
    nameTranslations: { type: mongoose_1.default.Schema.Types.Mixed },
    descriptionTranslations: { type: mongoose_1.default.Schema.Types.Mixed },
    account: { type: String },
    location: { type: String },
    productType: { type: Number },
    plu: { type: String },
    price: { type: Number },
    previous_price: { type: Number },
    priceLevels: { type: mongoose_1.default.Schema.Types.Mixed },
    sortOrder: { type: Number },
    deliveryTax: { type: Number },
    takeawayTax: { type: Number },
    multiply: { type: Number },
    posProductId: { type: String },
    posProductCategoryId: { type: [String] },
    subProducts: { type: [String] },
    productTags: { type: [String] },
    posCategoryIds: { type: [String] },
    imageUrl: { type: String },
    max: { type: Number },
    min: { type: Number },
    capacityUsages: { type: [String] },
    parentId: { type: String },
    visible: { type: Boolean, default: true },
    snoozed: { type: Boolean },
    subProductSortOrder: { type: [String] },
    internalId: { type: String },
    new: { type: Boolean, default: false },
    popular: { type: Boolean, default: false },
    offert: { type: Boolean, default: false },
    recomended: { type: Boolean, default: false },
    quantity: { type: Number, default: 1 },
    storeId: { type: String },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("product", productSchema);
