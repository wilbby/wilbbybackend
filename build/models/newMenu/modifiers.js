"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const modifiersSchema = new mongoose_1.default.Schema({
    name: { type: String },
    description: { type: String },
    nameTranslations: { type: mongoose_1.default.Schema.Types.Mixed },
    descriptionTranslations: { type: mongoose_1.default.Schema.Types.Mixed },
    account: { type: String },
    location: { type: String },
    productType: { type: Number },
    plu: { type: String },
    price: { type: Number },
    priceLevels: { type: mongoose_1.default.Schema.Types.Mixed },
    sortOrder: { type: Number },
    deliveryTax: { type: Number },
    takeawayTax: { type: Number },
    multiply: { type: Number },
    posProductId: { type: String },
    posProductCategoryId: { type: [String] },
    subProducts: { type: [String] },
    productTags: { type: [String] },
    posCategoryIds: { type: [String] },
    imageUrl: { type: String },
    max: { type: Number },
    min: { type: Number },
    multiMax: { type: Number },
    capacityUsages: { type: [String] },
    parentId: { type: String },
    snoozed: { type: Boolean },
    subProductSortOrder: { type: [String] },
    internalId: { type: String },
    recomended: { type: Boolean, default: false },
    quantity: { type: Number, default: 1 },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("modifier", modifiersSchema);
