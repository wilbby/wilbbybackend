"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const categorySchema = new mongoose_1.default.Schema({
    name: { type: String },
    description: { type: String },
    nameTranslations: { type: mongoose_1.default.Schema.Types.Mixed },
    descriptionTranslations: { type: mongoose_1.default.Schema.Types.Mixed },
    account: { type: String },
    posLocationId: { type: String },
    posCategoryType: { type: String },
    subCategories: { type: [String] },
    posCategoryId: { type: String },
    imageUrl: { type: String },
    products: { type: [String] },
    menu: { type: String },
    sortedChannelProductIds: { type: [String] },
    subProductSortOrder: { type: [String] },
    subProducts: { type: [String] },
    level: { type: Number },
    availabilities: { type: [String] },
    internalId: { type: String },
    storeId: { type: String },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("categoryProduct", categorySchema);
