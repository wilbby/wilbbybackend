"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const offertsSchema = new mongoose_1.default.Schema({
    imagen: {
        type: String,
    },
    city: {
        type: String,
    },
    store: {
        type: String,
    },
    slug: {
        type: String,
    },
    apertura: {
        type: Number,
    },
    cierre: {
        type: Number,
    },
    open: {
        type: Boolean,
        default: true,
    },
});
exports.default = mongoose_1.default.model("offert", offertsSchema);
