"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const adressStoreSchema = new mongoose_1.default.Schema({
    calle: {
        type: String,
        required: true,
    },
    numero: {
        type: String,
    },
    codigoPostal: {
        type: String,
    },
    ciudad: {
        type: String,
    },
    store: {
        type: String,
    },
    lat: {
        type: String,
    },
    lgn: {
        type: String,
    },
});
exports.default = mongoose_1.default.model("adressStore", adressStoreSchema);
