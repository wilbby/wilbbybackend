"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const adressSchema = new mongoose_1.default.Schema({
    formatted_address: {
        type: String,
        required: true,
    },
    puertaPiso: {
        type: String,
    },
    type: {
        type: String,
    },
    usuario: {
        type: String,
    },
    lat: {
        type: String,
    },
    lgn: {
        type: String,
    },
});
exports.default = mongoose_1.default.model("adress", adressSchema);
