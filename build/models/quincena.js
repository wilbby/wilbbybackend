"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const quincenaSchema = new mongoose_1.default.Schema({
    fromDate: {
        type: String,
        required: true,
    },
    toDate: {
        type: String,
        required: true,
    },
    numberQuincena: {
        type: Number,
        required: true,
    },
});
exports.default = mongoose_1.default.model("quincena", quincenaSchema);
