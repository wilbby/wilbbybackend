"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const customorderSchema = new mongoose_1.default.Schema({
    display_id: {
        type: Number,
    },
    userID: {
        type: String,
    },
    origin: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    destination: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    city: {
        type: String,
    },
    riders: {
        type: String,
    },
    pagoPaypal: { type: mongoose_1.default.Schema.Types.Mixed },
    stripePaymentIntent: { type: mongoose_1.default.Schema.Types.Mixed },
    estado: {
        type: String,
        enum: [
            "Pendiente de pago",
            "Pagado",
            "Asignada",
            "Preparando para el envío",
            "En camino",
            "Entregado",
            "Rechazada",
            "Devuelto",
            "Valorada",
        ],
        default: "Pendiente de pago",
    },
    progreso: {
        type: String,
        enum: ["0", "25", "50", "75", "100"],
        default: "25",
    },
    status: {
        type: String,
        enum: ["success", "exception", "normal", "active"],
        default: "active",
    },
    date: {
        type: String,
    },
    nota: {
        type: String,
    },
    total: {
        type: String,
    },
    schedule: {
        type: Boolean,
    },
    distance: {
        type: String,
    },
    product_stimate_price: {
        type: String,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("customorder", customorderSchema);
