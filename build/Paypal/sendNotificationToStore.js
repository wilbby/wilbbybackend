"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendNotification = void 0;
exports.sendNotification = (IdOnesignal, textmessage) => {
    var sendNotification = function (data) {
        var headers = {
            "Content-Type": "application/json; charset=utf-8",
        };
        var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
        };
        var https = require("https");
        var req = https.request(options, function (res) {
            res.on("data", function (data) {
                console.log("Response:");
                console.log(JSON.parse(data));
            });
        });
        req.on("error", function (e) {
            console.log("ERROR:");
            console.log(e);
        });
        req.write(JSON.stringify(data));
        req.end();
    };
    var message = {
        app_id: "9a1893e6-8423-43cc-807a-d536a833b8e8",
        headings: { en: "Nuevo pedido Wilbby" },
        android_channel_id: "9009455c-8429-41f2-bb18-bd63a08a9d63",
        ios_sound: "despertador_minion.wav",
        contents: { en: `${textmessage}` },
        ios_badgeCount: 1,
        ios_badgeType: "Increase",
        include_player_ids: [IdOnesignal],
    };
    sendNotification(message);
};
