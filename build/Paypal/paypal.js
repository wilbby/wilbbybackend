"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = __importDefault(require("dotenv"));
const order_1 = __importDefault(require("../models/order"));
const card_1 = __importDefault(require("../models/card"));
const cartAdd_1 = __importDefault(require("../models/cartAdd"));
const paypalData_1 = __importDefault(require("../models/paypalData"));
const user_1 = __importDefault(require("../models/user"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
dotenv_1.default.config({ path: "variables.env" });
const braintree = require("braintree");
const gateway = new braintree.BraintreeGateway({
    environment: braintree.Environment.Sandbox,
    merchantId: "sbg9bxfj6z6432qf",
    publicKey: "gj8k74zxw296gh4r",
    privateKey: "15d79f7fd1989d5a77ab1e417b6d6bd9",
});
class PaypalRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/save-paypal", (req, res) => {
            const data = req.body;
            const newPaypal = new paypalData_1.default({
                email: data.email,
                firstName: data.firstName,
                lastName: data.lastName,
                nonce: data.nonce,
                payerId: data.payerId,
                phone: data.phone,
                userID: data.userID,
            });
            return new Promise((resolve, object) => {
                newPaypal.save((error) => __awaiter(this, void 0, void 0, function* () {
                    if (error) {
                        object(res.json({
                            success: false,
                            message: "Hubo un problema con su solicitud",
                            data: null,
                        }));
                    }
                    else {
                        resolve(res.json({
                            success: true,
                            message: "Cuenta añadida con éxito",
                            data: newPaypal,
                        }));
                    }
                }));
            });
        });
        this.router.get("/v1/paypal-account", (req, res) => {
            const { userID } = req.query;
            return new Promise((resolve, rejects) => {
                paypalData_1.default.find({ userID: userID }, (err, resp) => {
                    if (err) {
                        rejects(res.json({
                            messages: "Algo va mas intentalo de nuevo",
                            success: false,
                            data: {},
                        }));
                    }
                    else {
                        resolve(res.json({
                            messages: "Datos obtenido con éxito",
                            success: true,
                            data: resp,
                        }));
                    }
                });
            });
        });
        this.router.post("/payment-paypal", (req, res) => {
            const { nonce, amount, orderID, restaurantID, usuarioID } = req.body;
            gateway.transaction.sale({
                amount: amount,
                paymentMethodNonce: nonce,
                options: {
                    submitForSettlement: true,
                },
            }, (err, result) => {
                if (!err) {
                    order_1.default.findOneAndUpdate({ _id: orderID }, {
                        $set: {
                            estado: "Pagado",
                            progreso: "25",
                            status: "active",
                            pagoPaypal: result,
                        },
                    }, (err, order) => {
                        card_1.default
                            .deleteMany({ restaurant: String(restaurantID) })
                            .then(function () {
                            console.log("Data deleted card"); // Success
                        })
                            .catch(function (error) {
                            console.log(error); // Failure
                        });
                        cartAdd_1.default
                            .deleteMany({ usuarioId: String(usuarioID) })
                            .then(function () {
                            console.log("Data deleted cart"); // Success
                        })
                            .catch(function (error) {
                            console.log(error); // Failure
                        });
                        res.render("success");
                    });
                    res.json({ message: "Pago realizado con éxito", success: true });
                }
                else {
                    res.json({
                        message: "Algo no va bien intentalo de nuevo",
                        success: false,
                    });
                }
            });
        });
        this.router.post("/payment-paypal-custom-aorder", (req, res) => {
            const { nonce, amount, orderID } = req.body;
            gateway.transaction.sale({
                amount: amount,
                paymentMethodNonce: nonce,
                options: {
                    submitForSettlement: true,
                },
            }, (err, result) => {
                if (result.success) {
                    custonorder_1.default.findOneAndUpdate({ _id: orderID }, {
                        $set: {
                            estado: "Pagado",
                            progreso: "25",
                            status: "active",
                            pagoPaypal: result,
                        },
                    }, (err, order) => { });
                    res.json({ message: "Pago realizado con éxito", success: true });
                }
                else {
                    res.json({
                        message: "Algo no va bien intentalo de nuevo",
                        success: false,
                    });
                }
            });
        });
        this.router.get("/client_token", (req, res) => {
            const { paypalID } = req.query;
            gateway.clientToken.generate({ customerId: paypalID }, (err, response) => {
                if (err) {
                    res.json(err);
                }
                else {
                    res.json(response.clientToken);
                }
            });
        });
        this.router.post("/create-paypal-client", (req, res) => {
            const { name, lastName, email, userID } = req.body;
            gateway.customer.create({
                firstName: name,
                lastName: lastName,
                company: "Wilbby",
                email: email,
                phone: "34 664 028 161",
                fax: "34 664 028 161",
                website: "www.wilbby.com",
            }, (err, result) => {
                if (result.success) {
                    return (new Promise((resolve, reject) => {
                        user_1.default.findOneAndUpdate({ _id: userID }, { PaypalID: result.customer.id }, { new: true }, (error, _usuario) => {
                            if (error)
                                reject(error);
                            else
                                resolve(_usuario);
                        });
                    }),
                        res.json(result.customer.id));
                }
            });
        });
    }
}
const paypalRouter = new PaypalRouter();
paypalRouter.routes();
exports.default = paypalRouter.router;
