"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const paypal_rest_sdk_1 = __importDefault(require("paypal-rest-sdk"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
const order_1 = __importDefault(require("../models/order"));
const card_1 = __importDefault(require("../models/card"));
const cartAdd_1 = __importDefault(require("../models/cartAdd"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const config_1 = require("../webHook/config");
const Deliverect_1 = require("../webHook/Deliverect");
const Apis_1 = require("../Holded/Apis");
const sendNotificationToStore_1 = require("./sendNotificationToStore");
var request = require("request");
var options = {
    method: "POST",
    url: config_1.oauthURL,
    headers: {
        "content-type": "application/json",
    },
    body: JSON.stringify({
        client_id: config_1.client_id,
        client_secret: config_1.client_secret,
        audience: config_1.audience,
        grant_type: config_1.grant_type,
    }),
};
paypal_rest_sdk_1.default.configure({
    mode: "live",
    client_id: process.env.PAYPALCLIENTID || "",
    client_secret: process.env.PAYPALCLIENTSECRET || "",
});
class PaypalRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/paypal", (req, res) => {
            const { price, order, cart, plato } = req.query;
            let create_payment_json = {
                intent: "sale",
                payer: {
                    payment_method: "paypal",
                },
                redirect_urls: {
                    return_url: `https://api.wilbby.com/success?price=${price}&order=${order}&cart=${cart}&plato=${plato}`,
                    cancel_url: "https://api.wilbby.com/cancel",
                },
                transactions: [
                    {
                        amount: {
                            currency: "EUR",
                            total: req.query.price,
                        },
                        description: "Locatefit S.L (Wilbby)",
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.create(create_payment_json, function (error, payment) {
                if (error) {
                    res.redirect("/cancel");
                }
                else {
                    // @ts-ignore
                    res.redirect(payment.links[1].href);
                }
            });
        });
        this.router.get("/success", (req, res) => {
            var PayerID = req.query.PayerID;
            var paymentId = req.query.paymentId;
            const { price, order, cart, plato } = req.query;
            var execute_payment_json = {
                payer_id: PayerID,
                transactions: [
                    {
                        amount: {
                            currency: "EUR",
                            total: price,
                        },
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.execute(
            // @ts-ignore
            paymentId, execute_payment_json, function (error, payment) {
                if (error) {
                    res.redirect("/cancel");
                }
                if (payment) {
                    order_1.default.findOneAndUpdate({ _id: order }, {
                        $set: {
                            estado: "Pagado",
                            progreso: "25",
                            status: "active",
                            pagoPaypal: payment,
                        },
                    }, (err, order) => {
                        if (!err) {
                            Apis_1.setOrderToHolded(order);
                            if (order === null || order === void 0 ? void 0 : order.StoreData.channelLinkId) {
                                request(options, function (error, response) {
                                    if (error)
                                        throw new Error(error);
                                    var resp = JSON.parse(response.body);
                                    Deliverect_1.setOrderToDeliverect(order, resp.access_token, resp.token_type);
                                });
                            }
                            else {
                                sendNotificationToStore_1.sendNotification(order === null || order === void 0 ? void 0 : order.StoreData.OnesignalID, "Tienes un nuevo pedido a por todas");
                            }
                        }
                        card_1.default
                            .deleteMany({ restaurant: String(cart) })
                            .then(function () {
                            console.log("Data deleted card"); // Success
                        })
                            .catch(function (error) {
                            console.log(error); // Failure
                        });
                        cartAdd_1.default
                            .deleteMany({ usuarioId: String(plato) })
                            .then(function () {
                            console.log("Data deleted cart"); // Success
                        })
                            .catch(function (error) {
                            console.log(error); // Failure
                        });
                        res.render("success");
                    });
                    console.log("Get Payment Response");
                }
            });
        });
        this.router.get("/cancel", (req, res) => {
            res.render("cancel");
        });
        this.router.get("/paypal-custom-order", (req, res) => {
            const { price, order, currency } = req.query;
            let create_payment_json = {
                intent: "sale",
                payer: {
                    payment_method: "paypal",
                },
                redirect_urls: {
                    return_url: `https://api.Wilbby.com/success?price=${price}&order=${order}&currency=${currency}`,
                    cancel_url: "https://api.Wilbby.com/cancel",
                },
                transactions: [
                    {
                        amount: {
                            currency: currency ? currency : "EUR",
                            total: req.query.price,
                        },
                        description: "Locatefit S.L (Wilbby)",
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.create(create_payment_json, function (error, payment) {
                if (error) {
                    res.render("cancel");
                }
                else {
                    res.render("cancel");
                    // @ts-ignore
                    res.redirect(payment.links[1].href);
                }
            });
        });
        this.router.get("/success", (req, res) => {
            var PayerID = req.query.PayerID;
            var paymentId = req.query.paymentId;
            const { price, order, currency } = req.query;
            var execute_payment_json = {
                payer_id: PayerID,
                transactions: [
                    {
                        amount: {
                            currency: currency ? currency : "EUR",
                            total: price,
                        },
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.execute(
            // @ts-ignore
            paymentId, execute_payment_json, function (error, payment) {
                if (error) {
                    res.render("cancel");
                }
                if (payment) {
                    custonorder_1.default.findOneAndUpdate({ _id: order }, {
                        $set: {
                            estado: "Pagado",
                            progreso: "25",
                            status: "active",
                            pagoPaypal: payment,
                        },
                    });
                }
            });
        });
        this.router.get("/cancel", (req, res) => {
            res.render("cancel");
        });
    }
}
const paypalRouter = new PaypalRouter();
paypalRouter.routes();
exports.default = paypalRouter.router;
