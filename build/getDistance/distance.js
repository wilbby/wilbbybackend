"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setDistanceToOrder = void 0;
const order_1 = __importDefault(require("../models/order"));
var distance = require("google-distance-matrix");
distance.key("AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA");
distance.units("imperial");
exports.setDistanceToOrder = (OrderID) => __awaiter(void 0, void 0, void 0, function* () {
    const order = yield order_1.default.findOne({ _id: OrderID });
    if (!(order === null || order === void 0 ? void 0 : order.llevar)) {
        var origins = [`${order === null || order === void 0 ? void 0 : order.AdreesData.lat},${order === null || order === void 0 ? void 0 : order.AdreesData.lgn}`];
        var destinations = [
            `${order === null || order === void 0 ? void 0 : order.StoreData.adress.lat},${order === null || order === void 0 ? void 0 : order.StoreData.adress.lgn}`,
        ];
        distance.matrix(origins, destinations, function (err, distances) {
            if (err) {
                return console.log(err);
            }
            if (!distances) {
                return console.log("no distances");
            }
            if (distances.status === "OK") {
                for (var i = 0; i < origins.length; i++) {
                    for (var j = 0; j < destinations.length; j++) {
                        var origin = distances.origin_addresses[i];
                        var destination = distances.destination_addresses[j];
                        if (distances.rows[0].elements[j].status == "OK") {
                            var distance = distances.rows[i].elements[j].distance.value;
                            const km = distance / 1000;
                            const kmPrice = km * 0.4;
                            const base = 2.8;
                            const time = 0;
                            const extras = 0;
                            const lluvia = 0;
                            const combinado = 0;
                            const propina = Number(order === null || order === void 0 ? void 0 : order.propinaTotal);
                            const total = kmPrice + base + time + extras + lluvia + combinado;
                            const iva = (21 / 100) * total;
                            const totalFinal = total - iva;
                            const ivaformat = iva.toFixed(2);
                            const totalformat = totalFinal.toFixed(2);
                            const input = {
                                km: String(km.toFixed(2)),
                                base: String(base),
                                time: String(time),
                                order: String(order === null || order === void 0 ? void 0 : order.display_id),
                                extra: String(extras),
                                combinado: String(combinado),
                                lluvia: String(lluvia),
                                total: String(totalformat),
                                propina: String(propina),
                                iva: String(ivaformat),
                            };
                            order_1.default.findOneAndUpdate({ _id: OrderID }, {
                                $set: {
                                    reportRiders: input,
                                },
                            }, (err, order) => { });
                        }
                        else {
                            console.log(destination + " is not reachable by land from " + origin);
                        }
                    }
                }
            }
        });
    }
    else {
        null;
    }
});
