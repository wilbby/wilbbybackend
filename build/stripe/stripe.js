"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = __importDefault(require("dotenv"));
const user_1 = __importDefault(require("../models/user"));
const changeToken_1 = require("./changeToken");
const order_1 = __importDefault(require("../models/order"));
const card_1 = __importDefault(require("../models/card"));
const cartAdd_1 = __importDefault(require("../models/cartAdd"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const config_1 = require("../webHook/config");
const Deliverect_1 = require("../webHook/Deliverect");
const Apis_1 = require("../Holded/Apis");
const sendNotificationToStore_1 = require("./sendNotificationToStore");
var request = require("request");
var stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
dotenv_1.default.config({ path: "variables.env" });
var options = {
    method: "POST",
    url: config_1.oauthURL,
    headers: {
        "content-type": "application/json",
    },
    body: JSON.stringify({
        client_id: config_1.client_id,
        client_secret: config_1.client_secret,
        audience: config_1.audience,
        grant_type: config_1.grant_type,
    }),
};
class StripeRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/create-client", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { userID, nameclient, email } = req.query;
            yield stripe.customers.create({
                name: nameclient,
                email: email,
                description: "Clientes de Wilbby",
            }, function (err, customer) {
                user_1.default.findOneAndUpdate({ _id: userID }, {
                    $set: {
                        StripeID: customer.id,
                    },
                }, (err, customers) => {
                    if (err) {
                        console.log(err);
                    }
                });
            });
        }));
        this.router.get("/card-create", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customers, token } = req.query;
            const paymentMethod = yield stripe.paymentMethods.attach(token, {
                customer: customers,
            });
            if (paymentMethod) {
                res.json({ success: true, data: paymentMethod });
            }
            else {
                res.json({ success: false, data: "Algo salio mas intentalo de nuevo" });
            }
        }));
        this.router.get("/get-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customers } = req.query;
            const paymentMethods = yield stripe.paymentMethods.list({
                customer: customers,
                type: "card",
            });
            res.json(paymentMethods);
        }));
        this.router.get("/delete-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { cardID, customers } = req.query;
            yield stripe.customers.deleteSource(customers, cardID, function (err, confirmation) {
                res.json(confirmation);
            });
        }));
        this.router.get("/payment-existing-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { card, customers, amount, order, cart, userID, currency, } = req.query;
            const total = Number(amount).toFixed(0);
            try {
                yield stripe.paymentIntents.create({
                    amount: total,
                    currency: currency ? currency : "EUR",
                    customer: customers,
                    payment_method: card,
                    setup_future_usage: "off_session",
                    confirm: true,
                }, function (err, payment) {
                    if (err) {
                        res.json(err);
                    }
                    else {
                        res.json(payment);
                        if (payment.status === "succeeded") {
                            order_1.default.findOneAndUpdate({ _id: order }, {
                                $set: {
                                    estado: "Pagado",
                                    progreso: "25",
                                    status: "active",
                                    stripePaymentIntent: payment,
                                },
                            }, (err, order) => {
                                if (!err) {
                                    if (order === null || order === void 0 ? void 0 : order.StoreData.channelLinkId) {
                                        request(options, function (error, response) {
                                            if (error)
                                                throw new Error(error);
                                            var resp = JSON.parse(response.body);
                                            Deliverect_1.setOrderToDeliverect(order, resp.access_token, resp.token_type);
                                        });
                                    }
                                    else {
                                        sendNotificationToStore_1.sendNotification(order === null || order === void 0 ? void 0 : order.StoreData.OnesignalID, "Tienes un nuevo pedido a por todas");
                                    }
                                    Apis_1.setOrderToHolded(order);
                                }
                                card_1.default
                                    .deleteMany({ restaurant: String(cart) })
                                    .then(function () {
                                    console.log("Data deleted card"); // Success
                                })
                                    .catch(function (error) {
                                    console.log(error); // Failure
                                });
                                cartAdd_1.default
                                    .deleteMany({ usuarioId: String(userID) })
                                    .then(function () {
                                    console.log("Data deleted cart"); // Success
                                })
                                    .catch(function (error) {
                                    console.log(error); // Failure
                                });
                            });
                        }
                    }
                });
            }
            catch (err) {
                console.log("Error code is: ", err);
                const paymentIntentRetrieved = yield stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
                console.log("PI retrieved: ", paymentIntentRetrieved.id);
            }
        }));
        this.router.post("/payment-auth-new", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { card, customers, amount, currency, order } = req.body;
            const total = amount.toFixed(0);
            try {
                const paymentIntent = yield stripe.paymentIntents.create({
                    payment_method: card,
                    amount: total,
                    currency: currency ? currency : "EUR",
                    customer: customers,
                    confirmation_method: "automatic",
                    confirm: true,
                });
                if (paymentIntent.status === "succeeded") {
                    order_1.default.findOneAndUpdate({ _id: order }, {
                        $set: {
                            stripePaymentIntent: paymentIntent,
                        },
                    }, (err, order) => {
                        if (!err) {
                            if (order === null || order === void 0 ? void 0 : order.StoreData.channelLinkId) {
                                request(options, function (error, response) {
                                    if (error)
                                        throw new Error(error);
                                    var resp = JSON.parse(response.body);
                                    Deliverect_1.setOrderToDeliverect(order, resp.access_token, resp.token_type);
                                });
                            }
                            else {
                                sendNotificationToStore_1.sendNotification(order === null || order === void 0 ? void 0 : order.StoreData.OnesignalID, "Tienes un nuevo pedido a por todas");
                            }
                            Apis_1.setOrderToHolded(order);
                        }
                    });
                    res.status(200).json(paymentIntent).end();
                }
                else if (paymentIntent.status === "canceled") {
                    res.status(200).json(paymentIntent).end();
                }
                else if (paymentIntent.status === "requires_action") {
                    order_1.default.findOneAndUpdate({ _id: order }, {
                        $set: {
                            stripePaymentIntent: paymentIntent,
                        },
                    }, (err, order) => {
                        if (!err) {
                            res.status(200).json(paymentIntent).end();
                        }
                    });
                }
                else {
                    res.status(200).json(paymentIntent).end();
                }
            }
            catch (err) {
                console.log("Error code is: ", err);
                const paymentIntentRetrieved = yield stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
                console.log("PI retrieved: ", paymentIntentRetrieved.id);
                res.status(400).json(paymentIntentRetrieved).end();
            }
        }));
        this.router.post("/clean-cart", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { order, cart, userID } = req.body;
            order_1.default.findOneAndUpdate({ _id: order }, {
                $set: {
                    estado: "Pagado",
                    progreso: "25",
                    status: "active",
                },
            }, (err, order) => {
                if (!err) {
                    if (order === null || order === void 0 ? void 0 : order.StoreData.channelLinkId) {
                        request(options, function (error, response) {
                            if (error)
                                throw new Error(error);
                            var resp = JSON.parse(response.body);
                            Deliverect_1.setOrderToDeliverect(order, resp.access_token, resp.token_type);
                        });
                    }
                    else {
                        sendNotificationToStore_1.sendNotification(order === null || order === void 0 ? void 0 : order.StoreData.OnesignalID, "Tienes un nuevo pedido a por todas");
                    }
                    Apis_1.setOrderToHolded(order);
                }
                card_1.default
                    .deleteMany({ restaurant: String(cart) })
                    .then(function () {
                    console.log("Data deleted card"); // Success
                })
                    .catch(function (error) {
                    console.log(error); // Failure
                });
                cartAdd_1.default
                    .deleteMany({ usuarioId: String(userID) })
                    .then(function () {
                    console.log("Data deleted cart"); // Success
                })
                    .catch(function (error) {
                    console.log(error); // Failure
                });
            });
            res.status(200).json({ success: true }).end();
        }));
        this.router.post("/payment-existing-card-custom-order", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { card, customers, amount, order, currency } = req.body;
            const total = Number(amount).toFixed(0);
            try {
                const paymentIntent = yield stripe.paymentIntents.create({
                    payment_method: card,
                    amount: total,
                    currency: currency ? currency : "EUR",
                    customer: customers,
                    confirmation_method: "automatic",
                    confirm: true,
                });
                if (paymentIntent.status === "succeeded") {
                    res.status(200).json(paymentIntent).end();
                }
                else if (paymentIntent.status === "canceled") {
                    res.status(200).json(paymentIntent).end();
                }
                else if (paymentIntent.status === "requires_action") {
                    custonorder_1.default.findOneAndUpdate({ _id: order }, {
                        $set: {
                            estado: "Pagado",
                            progreso: "25",
                            status: "active",
                            stripePaymentIntent: paymentIntent,
                        },
                    }, (err, order) => {
                        if (err) {
                            res.status(400).json(err).end();
                        }
                        else {
                            res.status(200).json(paymentIntent).end();
                        }
                    });
                }
                else {
                    res.status(400).json(paymentIntent).end();
                }
            }
            catch (err) {
                console.log("Error code is: ", err);
                const paymentIntentRetrieved = yield stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
                console.log("PI retrieved: ", paymentIntentRetrieved.id);
                res.status(400).json(paymentIntentRetrieved).end();
            }
        }));
        this.router.post("/stripe/chargeToken", (req, res) => {
            const { stripeToken, amount, orders, currency } = req.body;
            if (stripeToken && amount != undefined) {
                changeToken_1.chargeToken(amount, stripeToken, currency)
                    .then((response) => __awaiter(this, void 0, void 0, function* () {
                    if (response.status == "succeeded") {
                        custonorder_1.default.findOneAndUpdate({ _id: orders }, {
                            $set: {
                                estado: "Pagado",
                                progreso: "25",
                                status: "active",
                                stripePaymentIntent: response,
                            },
                        }, (err, order) => {
                            res.json(response);
                        });
                    }
                    else {
                        res.send(response);
                    }
                }))
                    .catch((err) => {
                    res.status(401).send({ success: false, error: err });
                });
            }
            else {
                res.status(403).send({ success: false, error: "Invalid token" });
            }
        });
        this.router.post("/stripe/chargeToken/order", (req, res) => {
            const { stripeToken, amount, orders, cart, userID, currency, } = req.body;
            if (stripeToken && amount != undefined) {
                changeToken_1.chargeToken(amount, stripeToken, currency)
                    .then((response) => __awaiter(this, void 0, void 0, function* () {
                    if (response.status == "succeeded") {
                        order_1.default.findOneAndUpdate({ _id: orders }, {
                            $set: {
                                estado: "Pagado",
                                progreso: "25",
                                status: "active",
                                stripePaymentIntent: response,
                            },
                        }, (err, order) => {
                            if (!err) {
                                if (order === null || order === void 0 ? void 0 : order.StoreData.channelLinkId) {
                                    request(options, function (error, response) {
                                        if (error)
                                            throw new Error(error);
                                        var resp = JSON.parse(response.body);
                                        Deliverect_1.setOrderToDeliverect(order, resp.access_token, resp.token_type);
                                    });
                                }
                                else {
                                    sendNotificationToStore_1.sendNotification(order === null || order === void 0 ? void 0 : order.StoreData.OnesignalID, "Tienes un nuevo pedido a por todas");
                                }
                                Apis_1.setOrderToHolded(order);
                            }
                            card_1.default
                                .deleteMany({ restaurant: String(cart) })
                                .then(function () {
                                console.log("Data deleted card"); // Success
                            })
                                .catch(function (error) {
                                console.log(error); // Failure
                            });
                            cartAdd_1.default
                                .deleteMany({ usuarioId: String(userID) })
                                .then(function () {
                                console.log("Data deleted cart"); // Success
                            })
                                .catch(function (error) {
                                console.log(error); // Failure
                            });
                            res.send(response);
                        });
                    }
                    else {
                        res.send(response);
                    }
                }))
                    .catch((err) => {
                    console.log(err);
                    res.status(401).send({ success: false, error: err });
                });
            }
            else {
                res.status(403).send({ success: false, error: "Invalid token" });
            }
        });
        this.router.post("/create-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customer, paymentMethod } = req.body;
            const paymentMethods = yield stripe.paymentMethods.attach(paymentMethod, {
                customer: customer,
            });
            if (paymentMethods) {
                res.json({ success: true, data: paymentMethods });
            }
            else {
                res.json({ success: false, data: "Algo salio mas intentalo de nuevo" });
            }
        }));
        this.router.get("/delete-card-web", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { cardID } = req.query;
            const done = yield stripe.paymentMethods.detach(cardID);
            if (done) {
                res.send(done);
            }
            else {
                console.log("hay un error");
            }
        }));
    }
}
const stripeRouter = new StripeRouter();
stripeRouter.routes();
exports.default = stripeRouter.router;
