"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
var WooCommerceAPI = require("woocommerce-api");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
var WooCommerce = new WooCommerceAPI({
    url: process.env.WOOCOMERCE_URL,
    consumerKey: process.env.WOOCOMERCE_KEY,
    consumerSecret: process.env.WOOCOMERCE_SECRET,
    wpAPI: true,
    version: "v3",
});
class PharmacyRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    createProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            WooCommerce.get("products")
                .then((response) => {
                console.log(response.data);
            })
                .catch((error) => {
                console.log(error.response.data);
            });
        });
    }
    routes() {
        this.router.get("/create-product-pharmacy", this.createProduct);
    }
}
const pharmacyRouter = new PharmacyRouter();
pharmacyRouter.routes();
exports.default = pharmacyRouter.router;
