"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const mongoose_1 = require("mongoose");
const user_1 = __importDefault(require("../models/user"));
const verifyToken_1 = require("../libs/verifyToken");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const dotenv_1 = __importDefault(require("dotenv"));
const SendEmail_1 = require("../RecoveryPassword/Email/SendEmail");
const saveEmail_1 = require("../SaveEmailList/saveEmail");
const SavedataLogin_1 = require("../funtions/SavedataLogin");
const LoginEmailSend_1 = require("../RecoveryPassword/Email/LoginEmailSend");
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
const mailjet = require("node-mailjet").connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE);
dotenv_1.default.config({ path: "variables.env" });
const { ObjectId } = mongoose_1.Types;
const crearToken = (usuarioLogin, secreto, expiresIn) => {
    const { _id } = usuarioLogin;
    return jsonwebtoken_1.default.sign({ _id }, secreto, { expiresIn });
};
class userRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    profileUser(req, res) {
        res.json(req.userId);
    }
    createUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const input = req.body;
            const existentEmail = yield user_1.default.findOne({ email: req.body.email });
            if (existentEmail) {
                res.json({
                    success: false,
                    messages: "Ya hay un usuario con este email",
                    data: null,
                });
                return null;
            }
            else {
                const newUser = new user_1.default({
                    name: input.name,
                    lastName: input.lastName,
                    email: input.email,
                    password: input.password,
                    city: input.city,
                    termAndConditions: input.termAndConditions,
                });
                newUser.id = newUser._id;
                return new Promise((resolve, rejects) => {
                    newUser.save((err) => __awaiter(this, void 0, void 0, function* () {
                        if (err) {
                            rejects(err);
                            res.json({
                                success: false,
                                messages: "Hubo un error al crear tu cuenta intentalo de nuevo por favor",
                                data: err,
                            });
                        }
                        else {
                            resolve(newUser);
                            SendEmail_1.welcomeEmail(input.email, input.name);
                            saveEmail_1.SaveEmailWilbby(input.email, input.name, input.lastName);
                            /* CratePaypalUser(
                              input.name,
                              input.latName,
                              input.email,
                              newUser._id
                            ); */
                            yield stripe.customers.create({
                                name: input.nombre,
                                email: input.email,
                                description: "Clientes de Wilbby",
                            }, function (err, customer) {
                                user_1.default.findOneAndUpdate({ _id: newUser._id }, {
                                    $set: {
                                        StripeID: customer.id,
                                    },
                                }, (err, customers) => {
                                    if (err) {
                                        console.log(err);
                                    }
                                });
                            });
                            res.json({
                                success: true,
                                messages: "Tu cuenta ha sido creada con éxito",
                                data: newUser,
                            });
                            const request = mailjet.post("contact").request({
                                Email: input.email,
                                IsExcludedFromCampaigns: "false",
                                Name: input.name,
                            });
                            request
                                .then((result) => {
                                console.log(result.body.Data[0].ID);
                                const request = mailjet
                                    .post("contact")
                                    .id(result.body.Data[0].ID)
                                    .action("managecontactslists")
                                    .request({
                                    ContactsLists: [
                                        {
                                            ListID: 10241868,
                                            Action: "addnoforce",
                                        },
                                    ],
                                });
                                request
                                    .then((result) => {
                                    res.json(result.body);
                                })
                                    .catch((err) => {
                                    console.log(err.statusCode);
                                });
                            })
                                .catch((err) => {
                                console.log(err.statusCode);
                            });
                        }
                    }));
                });
            }
        });
    }
    getUser(req, res) {
        return new Promise((resolve, rejects) => __awaiter(this, void 0, void 0, function* () {
            yield user_1.default.findOne({ _id: ObjectId(req.params.id) }, (err, resp) => {
                if (err) {
                    rejects(err);
                }
                else {
                    resolve(res.json(resp));
                }
            });
        }));
    }
    getUsers(req, res) {
        return new Promise((resolve, rejects) => __awaiter(this, void 0, void 0, function* () {
            yield user_1.default.find((err, resp) => {
                if (err) {
                    rejects(err);
                }
                else {
                    resolve(res.json(resp));
                }
            });
        }));
    }
    authenticateteUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password, dataLogin } = req.body;
            const userRes = yield user_1.default.findOne({ email: email });
            if (!userRes) {
                res.json({
                    success: false,
                    messages: "Aún no de te has registrado en Wilbby",
                    data: null,
                });
            }
            const comparePassword = yield bcryptjs_1.default.compare(password, userRes.password);
            if (!comparePassword) {
                res.json({
                    success: false,
                    messages: "Contraseña incorrecta",
                    data: null,
                });
            }
            else {
                LoginEmailSend_1.LoginEmail(email, dataLogin, userRes);
                SavedataLogin_1.saveDataLogin(userRes._id, dataLogin);
                res
                    .header("auth-token", crearToken(userRes, process.env.SECRETO, "20000hr"))
                    .json({
                    success: true,
                    message: "Bienvenido a Wilbby",
                    data: {
                        token: crearToken(userRes, process.env.SECRETO, "20000hr"),
                        id: userRes._id,
                        verifyPhone: userRes.verifyPhone,
                    },
                });
            }
        });
    }
    updateUser(req, res) {
        const data = req.body;
        const input = {
            name: data.name,
            lastName: data.lastName,
            email: data.email,
        };
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            yield user_1.default.findOneAndUpdate({ _id: req.params.id }, input, { new: true }, (error, user) => {
                if (error) {
                    reject(error);
                    res.json({
                        success: false,
                        messages: "Hubo un error con su solicitud",
                        data: null,
                    });
                }
                else {
                    resolve(user);
                    res.json({
                        success: true,
                        messages: "Cuenta actualizada con éxito",
                        data: user,
                    });
                }
            });
        }));
    }
    deleteUser(req, res) {
        user_1.default
            .deleteOne({ _id: ObjectId(req.params.id) })
            .then((resp) => {
            if (resp) {
                res.json({
                    success: true,
                    messages: "Cuenta eliminado con éxito",
                    data: resp,
                });
            }
        })
            .catch((error) => console.log(error));
    }
    routes() {
        this.router.get("/profile", verifyToken_1.TokenValidations, this.profileUser);
        this.router.get("/:id", this.getUser);
        this.router.get("/", this.getUsers);
        this.router.post("/auth", this.authenticateteUser);
        this.router.post("/", this.createUser);
        this.router.put("/:id", this.updateUser);
        this.router.delete("/:id", this.deleteUser);
    }
}
const usersRouter = new userRouter();
exports.default = usersRouter.router;
