"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const order_1 = __importDefault(require("../models/order"));
const config_1 = require("../Holded/config");
const request_1 = __importDefault(require("request"));
const Apis_1 = require("../Holded/Apis");
class IndexRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/", (req, res) => res.send("Wilcome to Wilbby App®"));
        this.router.get("/update-orden", (req, res) => {
            const { id } = req.query;
            order_1.default.findOneAndUpdate({ _id: id }, {
                $set: {
                    isvalored: true,
                },
            }, (err, order) => {
                // @ts-ignore
                console.log("done update");
            });
        });
        this.router.get("/delete-holded-recipt", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { id, idriders, partnerid } = req.query;
            const url = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${id}`;
            const optiones = {
                method: "DELETE",
                url: url,
                headers: { Accept: "application/json", key: config_1.key },
            };
            request_1.default(optiones, function (error, responses) {
                if (error)
                    throw new Error(error);
            });
            const urlriedr = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${idriders}`;
            const optione = {
                method: "DELETE",
                url: urlriedr,
                headers: { Accept: "application/json", key: config_1.key },
            };
            request_1.default(optione, function (error, responses) {
                if (error)
                    throw new Error(error);
            });
            const urlpartner = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${partnerid}`;
            const optionespartner = {
                method: "DELETE",
                url: urlpartner,
                headers: { Accept: "application/json", key: config_1.key },
            };
            request_1.default(optionespartner, function (error, responses) {
                if (error)
                    throw new Error(error);
            });
            res.json({ data: "Recibo eliminado de Holded", success: true }).end();
        }));
        this.router.get("/test-holded", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { id } = req.query;
            const dat = yield order_1.default.findOne({
                _id: id,
            });
            Apis_1.setOrderToHolded(dat);
            res.status(200).end();
        }));
        this.router.get("/send-test-notfication-store", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { id } = req.query;
            var sendNotification = function (data) {
                var headers = {
                    "Content-Type": "application/json; charset=utf-8",
                };
                var options = {
                    host: "onesignal.com",
                    port: 443,
                    path: "/api/v1/notifications",
                    method: "POST",
                    headers: headers,
                };
                var https = require("https");
                var req = https.request(options, function (res) {
                    res.on("data", function (data) {
                        console.log("Response:");
                        console.log(JSON.parse(data));
                    });
                });
                req.on("error", function (e) {
                    console.log("ERROR:");
                    console.log(e);
                });
                req.write(JSON.stringify(data));
                req.end();
            };
            var message = {
                app_id: "9a1893e6-8423-43cc-807a-d536a833b8e8",
                headings: { en: "Nuevo pedido Wilbby" },
                android_channel_id: "9009455c-8429-41f2-bb18-bd63a08a9d63",
                ios_sound: "despertador_minion.wav",
                contents: {
                    en: `Estos es una notificacion de prueba para la app de restaurantes`,
                },
                ios_badgeCount: 1,
                ios_badgeType: "Increase",
                include_player_ids: [id],
            };
            sendNotification(message);
            res
                .send({
                data: "Restaurante notificado de tu pedido",
                success: true,
            })
                .end();
        }));
        this.router.get("/send-test-notfication-rider", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { id } = req.query;
            var sendNotification = function (data) {
                var headers = {
                    "Content-Type": "application/json; charset=utf-8",
                };
                var options = {
                    host: "onesignal.com",
                    port: 443,
                    path: "/api/v1/notifications",
                    method: "POST",
                    headers: headers,
                };
                var https = require("https");
                var req = https.request(options, function (res) {
                    res.on("data", function (data) {
                        console.log("Response:");
                        console.log(JSON.parse(data));
                    });
                });
                req.on("error", function (e) {
                    console.log("ERROR:");
                    console.log(e);
                });
                req.write(JSON.stringify(data));
                req.end();
            };
            var message = {
                app_id: "e21467de-eaf6-4090-9b6c-a311d37c0b5e",
                headings: { en: "Wilbby" },
                android_channel_id: "9b0682ea-6f2d-4228-8802-280df76cb033",
                ios_sound: "despertador_minion.wav",
                contents: { en: `Hola tienes un nuevo pedido de Wilbby` },
                ios_badgeCount: 1,
                ios_badgeType: "Increase",
                include_player_ids: [id],
            };
            sendNotification(message);
            res
                .send({
                data: "Rider notificado de tu pedido",
                success: true,
            })
                .end();
        }));
    }
}
const indexRouter = new IndexRouter();
indexRouter.routes();
exports.default = indexRouter.router;
