"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const ContactEmail_1 = __importDefault(require("../RecoveryPassword/Email/ContactEmail"));
const ContactRider_1 = __importDefault(require("../RecoveryPassword/Email/ContactRider"));
const nodemailer = require("nodemailer");
class ContactRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/contact-for-restaurant", (req, res) => {
            const { values } = req.body;
            const transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                service: "gmail",
                port: 465,
                secure: true,
                auth: {
                    user: process.env.EMAIL_ADDRESS,
                    pass: process.env.EMAIL_PASSWORD,
                },
            });
            const mailOptions = {
                from: process.env.EMAIL_ADDRESS,
                to: `info@wilbby.com`,
                subject: "Solicitud de para unirse a Wilbby",
                text: "Contacto nuevo establecimiento",
                html: ContactEmail_1.default(values),
            };
            transporter.sendMail(mailOptions, (err) => {
                if (err) {
                    res.json({
                        success: false,
                        messages: "Algo va mal intentalo de nuevo",
                    });
                }
                else {
                    res.json({
                        success: true,
                        messages: "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
                    });
                }
            });
        });
        this.router.post("/contact-for-rider", (req, res) => {
            const { values } = req.body;
            const transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                service: "gmail",
                port: 465,
                secure: true,
                auth: {
                    user: process.env.EMAIL_ADDRESS,
                    pass: process.env.EMAIL_PASSWORD,
                },
            });
            const mailOptions = {
                from: process.env.EMAIL_ADDRESS,
                to: `info@wilbby.com`,
                subject: "Solicitud Riders",
                text: "Solicitud para repartir con Wilbby",
                html: ContactRider_1.default(values),
            };
            transporter.sendMail(mailOptions, (err) => {
                if (err) {
                    res.json({
                        success: false,
                        messages: "Algo va mal intentalo de nuevo",
                    });
                }
                else {
                    res.json({
                        success: true,
                        messages: "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
                    });
                }
            });
        });
    }
}
const constctRouter = new ContactRouter();
constctRouter.routes();
exports.default = constctRouter.router;
