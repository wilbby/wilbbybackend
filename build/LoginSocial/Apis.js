"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../models/user"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const crypto_1 = __importDefault(require("crypto"));
const apple_signin_auth_1 = __importDefault(require("apple-signin-auth"));
const dotenv_1 = __importDefault(require("dotenv"));
const mailJet_1 = require("../mailJet");
const SendEmail_1 = require("../RecoveryPassword/Email/SendEmail");
const saveEmail_1 = require("../SaveEmailList/saveEmail");
const config_js_1 = require("./config.js");
const request = require("request");
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
dotenv_1.default.config({ path: "variables.env" });
var bcrypt = require("bcryptjs");
var passport = require("passport");
require("./passport")();
var createToken = function (auth) {
    return jsonwebtoken_1.default.sign({
        id: auth.id,
    }, process.env.SECRETO || "secretToken", {
        expiresIn: 60 * 120,
    });
};
function generateToken(req, res, next) {
    req.token = createToken(req.auth);
    return next();
}
function sendToken(req, res) {
    res.setHeader("x-auth-token", req.token);
    return res.status(200).send(JSON.stringify(req.user));
}
class SocialRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/api/v1/auth/twitter/reverse", function (req, res) {
            request.post({
                url: "https://api.twitter.com/oauth/request_token",
                oauth: {
                    oauth_callback: "https%3A%2F%2Flocalhost%3A3000%2Flogin",
                    consumer_key: config_js_1.twiterr_api_key,
                    consumer_secret: config_js_1.twitter_api_secret,
                },
            }, function (err, r, body) {
                if (err) {
                    //@ts-ignore
                    return res.send(500, { message: e.message });
                }
                var jsonStr = '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
                res.send(JSON.parse(jsonStr));
            });
        });
        this.router.post("/api/v1/auth/twitter", (req, res, next) => {
            request.post({
                url: `https://api.twitter.com/oauth/access_token?oauth_verifier`,
                oauth: {
                    consumer_key: config_js_1.twiterr_api_key,
                    consumer_secret: config_js_1.twitter_api_secret,
                    token: req.query.oauth_token,
                },
                form: { oauth_verifier: req.query.oauth_verifier },
            }, function (err, r, body) {
                if (err) {
                    //@ts-ignore
                    return res.send(500, { message: err.message });
                }
                const bodyString = '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
                const parsedBody = JSON.parse(bodyString);
                req.body["oauth_token"] = parsedBody.oauth_token;
                req.body["oauth_token_secret"] = parsedBody.oauth_token_secret;
                req.body["user_id"] = parsedBody.user_id;
                next();
            });
        }, passport.authenticate("twitter-token", { session: false }), function (req, res, next) {
            if (!req.user) {
                //@ts-ignore
                return res.send(401, "User Not Authenticated");
            }
            req.auth = {
                id: req.user.id,
            };
            return next();
        }, generateToken, sendToken);
        this.router.post("/api/v1/auth/facebook", passport.authenticate("facebook-token", { session: false }), function (req, res, next) {
            if (!req.user) {
                return res.send(401);
            }
            req.auth = {
                id: req.user.id,
            };
            next();
        }, generateToken, sendToken);
        this.router.post("/api/auth/apple-web", function (req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                require("mongoose").model("user").schema.add({
                    appleToken: String,
                    socialNetworks: String,
                    isSocial: Boolean,
                });
                const { id_token, user } = req.body;
                const appleIdTokenClaims = yield apple_signin_auth_1.default.verifyIdToken(id_token, {
                    nonce: "nonce"
                        ? crypto_1.default.createHash("sha256").update("nonce").digest("hex")
                        : undefined,
                });
                // // check if email exists
                let emailExistsApple = yield user_1.default.findOne({
                    appleToken: appleIdTokenClaims.sub,
                });
                if (emailExistsApple) {
                    const nuevoUsuario = emailExistsApple;
                    res.json({ nuevoUsuario, token: appleIdTokenClaims.sub });
                }
                else {
                    saveEmail_1.SaveEmailWilbby(user.email, user.name.firstName, user.name.lastName);
                    const nuevoUsuario = new user_1.default({
                        name: user.name.firstName,
                        lastName: user.name.lastName,
                        email: user.email,
                        password: appleIdTokenClaims.sub,
                        isSocial: true,
                        appleToken: appleIdTokenClaims.sub,
                        socialNetworks: "Apple",
                    });
                    nuevoUsuario.id = nuevoUsuario._id;
                    nuevoUsuario.save((error) => __awaiter(this, void 0, void 0, function* () {
                        if (error) {
                            return res.json(error);
                        }
                        else {
                            yield stripe.customers.create({
                                name: user.name.firstName,
                                email: user.email,
                                description: "Clientes de Wilbby",
                            }, function (err, customer) {
                                user_1.default.findOneAndUpdate({ _id: nuevoUsuario._id }, {
                                    $set: {
                                        StripeID: customer.id,
                                    },
                                }, (err) => {
                                    if (err) {
                                        console.log(err);
                                    }
                                });
                            });
                            return res.json({ nuevoUsuario, token: appleIdTokenClaims.sub });
                        }
                    }));
                }
            });
        });
        this.router.post("/api/v1/auth/google", passport.authenticate("google-token", { session: false }), function (req, res, next) {
            if (!req.user) {
                return res.send(401);
            }
            req.auth = {
                id: req.user.id,
            };
            next();
        }, generateToken, sendToken);
        this.router.post("/api/v1/auth/social/mobile", function (req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                require("mongoose").model("user").schema.add({
                    appleToken: String,
                    socialNetworks: String,
                    isSocial: Boolean,
                });
                let data = req.body;
                const tokens = data.token;
                // // check if email exists
                let emailExistsApple = yield user_1.default.findOne({
                    appleToken: data.token,
                });
                if (emailExistsApple) {
                    const nuevoUsuario = emailExistsApple;
                    res.json({ nuevoUsuario, token: tokens });
                }
                else {
                    mailJet_1.SaveEmail(data.email, data.firstName);
                    SendEmail_1.welcomeEmail(data.email, data.firstName);
                    // // check if email exists
                    let emailExists = yield user_1.default.findOne({
                        email: data.email,
                    });
                    if (emailExists) {
                        bcrypt.genSalt(10, (err, salt) => {
                            if (err)
                                console.log(err);
                            bcrypt.hash(data.token, salt, (err, hash) => {
                                if (err)
                                    console.log(err);
                                user_1.default.findOneAndUpdate({ email: data.email }, { password: hash }, (err, updated) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                    let nuevoUsuario = updated;
                                    res.json({ nuevoUsuario, token: tokens });
                                });
                            });
                        });
                    }
                    else {
                        const nuevoUsuarios = new user_1.default({
                            name: data.firstName,
                            lastName: data.lastName,
                            email: data.email,
                            password: data.token,
                            isSocial: true,
                            city: data.city,
                            appleToken: data.token,
                            socialNetworks: data.provide,
                        });
                        nuevoUsuarios.id = nuevoUsuarios._id;
                        nuevoUsuarios.save((error) => __awaiter(this, void 0, void 0, function* () {
                            if (error) {
                                return res.json(error);
                            }
                            else {
                                yield stripe.customers.create({
                                    name: data.firstName,
                                    email: data.email,
                                    description: "Clientes de Wilbby",
                                }, function (err, customer) {
                                    user_1.default.findOneAndUpdate({ _id: nuevoUsuarios._id }, {
                                        $set: {
                                            StripeID: customer.id,
                                        },
                                    }, (err, customers) => {
                                        if (err) {
                                            console.log(err);
                                        }
                                    });
                                });
                                const nuevoUsuario = nuevoUsuarios;
                                return res.json({ nuevoUsuario, token: tokens });
                            }
                        }));
                    }
                }
            });
        });
    }
}
const socialRouter = new SocialRouter();
socialRouter.routes();
exports.default = socialRouter.router;
