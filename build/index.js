"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const helmet_1 = __importDefault(require("helmet"));
const mongoose_1 = __importDefault(require("mongoose"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const apollo_server_express_1 = require("apollo-server-express");
const path_1 = __importDefault(require("path"));
const body_parser_1 = __importDefault(require("body-parser"));
const index_router_1 = __importDefault(require("./routers/index.router"));
const Apis_1 = __importDefault(require("./LoginSocial/Apis"));
const Recovery_1 = __importDefault(require("./RecoveryPassword/Recovery"));
const Paypal_1 = __importDefault(require("./Paypal"));
const ConfirmNumber_1 = __importDefault(require("./Twilio/ConfirmNumber"));
const userRouter_1 = __importDefault(require("./routers/userRouter"));
const stripe_1 = __importDefault(require("./stripe/stripe"));
const category_1 = __importDefault(require("./MercadonaApis/category"));
const saveProduct_1 = __importDefault(require("./MercadonaApis/saveProduct"));
const OneSignal_1 = __importDefault(require("./OneSignal"));
const constact_router_1 = __importDefault(require("./routers/constact.router"));
const apis_1 = __importDefault(require("./webHook/apis"));
const apiEmails_1 = __importDefault(require("./Emails/apiEmails"));
const nemu_api_1 = __importDefault(require("./Api_Stores/nemu_api"));
const Resolvers_1 = require("./GraphQL/Resolvers");
const Schema_1 = require("./GraphQL/Schema");
const consolidate_1 = __importDefault(require("consolidate"));
const paypal_1 = __importDefault(require("./Paypal/paypal"));
const index_1 = __importDefault(require("./whatsApp/index"));
/* const setOrders = async () => {
  menuSchema.updateMany(
    { account: "602fc6967ed2c280b3f5a9e7" },
    {
      $set: {
        storeId: "6050ad74d632f526c761a43d",
      },
    },
    () => {}
  );
};

setOrders(); */
/*
menuSchema
  .deleteMany({ estado: "Pendiente de pago" })
  .then(function () {
    console.log("Data deleted card"); // Success
  })
  .catch(function (error) {
    console.log(error); // Failure
  }); */
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.router();
    }
    config() {
        const MONGO_URI = process.env.MONGO_URI || "nodata";
        mongoose_1.default.set("useFindAndModify", true);
        mongoose_1.default
            .connect(MONGO_URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        })
            .then((db) => console.log("DB is conected"));
        //Settings
        this.app.set("port", process.env.PORT || 4000);
        //Middleware
        this.app.use(cors_1.default());
        this.app.use(morgan_1.default("dev"));
        this.app.use(body_parser_1.default.json({ limit: "50mb" }));
        this.app.use(body_parser_1.default.urlencoded({ limit: "50mb", extended: true }));
        this.app.use(helmet_1.default());
        this.app.use(compression_1.default());
        this.app.use(function (req, res, next) {
            //to allow cross domain requests to send cookie information.
            //@ts-ignore
            res.header("Access-Control-Allow-Credentials", true);
            // origin can not be '*' when crendentials are enabled. so need to set it to the request origin
            res.header("Access-Control-Allow-Origin", req.headers.origin);
            // list of methods that are supported by the server
            res.header("Access-Control-Allow-Methods", "OPTIONS,GET,PUT,POST,DELETE");
            res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, X-XSRF-TOKEN");
            next();
        });
    }
    router() {
        this.app.use(index_router_1.default);
        this.app.use("/api/user", userRouter_1.default);
        this.app.use(Apis_1.default);
        this.app.use(Recovery_1.default);
        this.app.use(ConfirmNumber_1.default);
        this.app.use(Paypal_1.default);
        this.app.use(paypal_1.default);
        this.app.use(category_1.default);
        this.app.use(constact_router_1.default);
        this.app.use(saveProduct_1.default);
        this.app.use(index_1.default);
        this.app.use(apiEmails_1.default);
        this.app.use(apis_1.default);
        this.app.use(nemu_api_1.default);
        this.app.engine("ejs", consolidate_1.default.ejs);
        this.app.set("views", "views");
        this.app.set("view engine", "ejs");
        this.app.use("/assets", express_1.default.static(path_1.default.join(__dirname + "/../uploads")));
        this.app.use(stripe_1.default);
        this.app.use(OneSignal_1.default);
        this.app.use(body_parser_1.default.urlencoded({
            parameterLimit: 100000,
            limit: "50mb",
            extended: true,
        }));
        this.app.use(body_parser_1.default.json({ limit: "50mb", type: "application/json" }));
    }
    start(paht) {
        this.app.listen(this.app.get("port"), () => {
            console.log(`Server on port http://localhost:${this.app.get("port")}${paht}`);
        });
    }
}
const servers = new apollo_server_express_1.ApolloServer({
    typeDefs: Schema_1.typeDefs,
    resolvers: Resolvers_1.resolvers,
    context: ({ req }) => __awaiter(void 0, void 0, void 0, function* () {
        const token = req.headers["authorization"];
        if (token !== "null") {
            try {
                // verificarl el token que viene del cliente
                //@ts-ignore
                const usuarioActual = yield jsonwebtoken_1.default.verify(token, process.env.SECRETO);
                req.usuarioActual = usuarioActual;
                return {
                    usuarioActual,
                };
            }
            catch (err) {
                // console.log('err: ', err);
            }
        }
    }),
});
const server = new Server();
servers.applyMiddleware(server);
server.start(servers.graphqlPath);
