"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mutation = void 0;
const categorias_1 = __importDefault(require("../models/categorias"));
const user_1 = __importDefault(require("../models/user"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const platos_1 = __importDefault(require("../models/nemu/platos"));
const menu_1 = __importDefault(require("../models/nemu/menu"));
const Pago_1 = __importDefault(require("../models/Pago"));
const transacciones_1 = __importDefault(require("../models/transacciones"));
const adress_1 = __importDefault(require("../models/adress"));
const acompanante_1 = __importDefault(require("../models/nemu/acompanante"));
const userAdmin_1 = __importDefault(require("../models/userAdmin"));
const card_1 = __importDefault(require("../models/card"));
const cupones_1 = __importDefault(require("../models/cupones"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const cartAdd_1 = __importDefault(require("../models/cartAdd"));
const riders_1 = __importDefault(require("../models/riders"));
const Status_messages_1 = require("./Status_messages");
const fs_1 = __importDefault(require("fs"));
const mongoose_1 = require("mongoose");
const order_1 = __importDefault(require("../models/order"));
const rating_1 = __importDefault(require("../models/rating"));
const Notification_1 = __importDefault(require("../models/Notification"));
const Opinion_1 = __importDefault(require("../models/Opinion"));
const tipo_1 = __importDefault(require("../models/tipo"));
const highkitchenCategory_1 = __importDefault(require("../models/highkitchenCategory"));
const adressStore_1 = __importDefault(require("../models/adressStore"));
const post_1 = __importDefault(require("../models/post"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const mailJet_1 = require("../mailJet");
const saveEmail_1 = require("../SaveEmailList/saveEmail");
const tipotienda_1 = __importDefault(require("../models/tipotienda"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const quincena_1 = __importDefault(require("../models/quincena"));
const offerts_1 = __importDefault(require("../models/offerts"));
const collections_1 = __importDefault(require("../models/collections"));
const subcollection_1 = __importDefault(require("../models/subcollection"));
const SendEmail_1 = require("../RecoveryPassword/Email/SendEmail");
const cityclose_1 = __importDefault(require("../models/cityclose"));
const trasactionRider_1 = __importDefault(require("../models/trasactionRider"));
const SavedataLogin_1 = require("../funtions/SavedataLogin");
const distance_1 = require("../getDistance/distance");
const dotenv_1 = __importDefault(require("dotenv"));
const LoginEmailSend_1 = require("../RecoveryPassword/Email/LoginEmailSend");
const sendNotificationRider_1 = require("./sendNotificationRider");
const CompraRiders_1 = require("../Holded/CompraRiders");
const addToCart_1 = __importDefault(require("../models/newOrder/addToCart"));
dotenv_1.default.config({ path: "variables.env" });
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
const { ObjectId } = mongoose_1.Types;
const crearToken = (restaurant, secreto, expiresIn) => {
    const { _id } = restaurant;
    return jsonwebtoken_1.default.sign({ _id }, secreto, { expiresIn });
};
exports.Mutation = {
    autenticarRestaurant: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const restaurant = yield restaurant_1.default.findOne({ email });
        if (!restaurant) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.USER_NOT_FOUND,
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, restaurant.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.INCORRECT_PASSWORD,
                data: null,
            };
        }
        return {
            success: true,
            message: "Bienvenido a My Store by Wilbby",
            data: {
                token: crearToken(restaurant, process.env.SECRETO, "20000hr"),
                id: restaurant._id,
            },
        };
    }),
    autenticarUsuario: (root, { email, password, input }) => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield user_1.default.findOne({ email });
        if (!users) {
            return {
                success: false,
                message: "Aún no de te has registrado en Wilbby",
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, users.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: "Contraseña incorrecta",
                data: null,
            };
        }
        else {
            LoginEmailSend_1.LoginEmail(email, input, users);
            SavedataLogin_1.saveDataLogin(users._id, input);
            return {
                success: true,
                message: "Bienvenido a Wilbby",
                data: {
                    token: crearToken(users, process.env.SECRETO, "20000hr"),
                    id: users._id,
                },
            };
        }
    }),
    autenticarRiders: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield riders_1.default.findOne({ email });
        if (!users) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.USER_NOT_FOUND,
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, users.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.INCORRECT_PASSWORD,
                data: null,
            };
        }
        return {
            success: true,
            message: "Bienvenid@ a Wilbby®",
            data: {
                token: crearToken(users, process.env.SECRETO, "5000hr"),
                id: users._id,
            },
        };
    }),
    crearUsuario: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        mailJet_1.SaveEmail(input.email, input.nombre);
        saveEmail_1.SaveEmailWilbby(input.email, input.nombre, input.apellidos);
        SendEmail_1.welcomeEmail(input.email, input.nombre);
        // check if email exists
        const emailExists = yield user_1.default.findOne({ email: input.email });
        if (emailExists) {
            return {
                success: false,
                message: "Ya eres parte de Wilbby",
                data: null,
            };
        }
        const nuevoUsuario = new user_1.default({
            name: input.nombre,
            lastName: input.apellidos,
            email: input.email,
            password: input.password,
            city: input.city,
            termAndConditions: input.termAndConditions,
        });
        nuevoUsuario.id = nuevoUsuario._id;
        return new Promise((resolve, object) => {
            nuevoUsuario.save((error) => __awaiter(void 0, void 0, void 0, function* () {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Usuario agregado con éxito",
                        data: nuevoUsuario,
                    });
                    /*  CratePaypalUser(
                      input.nombre,
                      input.apellidos,
                      input.emai,
                      nuevoUsuario._id
                    ); */
                    yield stripe.customers.create({
                        name: input.nombre,
                        email: input.email,
                        description: "Clientes de Wilbby",
                    }, function (err, customer) {
                        user_1.default.findOneAndUpdate({ _id: nuevoUsuario._id }, {
                            $set: {
                                StripeID: customer.id,
                            },
                        }, (err, customers) => {
                            if (err) {
                            }
                        });
                    });
                }
            }));
        });
    }),
    crearRiders: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        // check if email exists
        const emailExists = yield riders_1.default.findOne({ email: input.email });
        if (emailExists) {
            return {
                success: false,
                message: "Ya eres parte de Wilbby",
                data: null,
            };
        }
        const nuevoUsuario = new riders_1.default({
            name: input.nombre,
            lastName: input.apellidos,
            email: input.email,
            password: input.password,
            termAndConditions: input.termAndConditions,
            telefono: input.telefono,
            isAvalible: input.isAvalible,
            city: input.city,
            contactCode: input.contactCode,
        });
        nuevoUsuario.id = nuevoUsuario._id;
        return new Promise((resolve, object) => {
            nuevoUsuario.save((error) => {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Usuario agregado con éxito",
                        data: nuevoUsuario,
                    });
                }
            });
        });
    }),
    crearAdmin: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        // check if email exists
        const emailExists = yield userAdmin_1.default.findOne({ email: input.email });
        if (emailExists) {
            return {
                success: false,
                message: "Ya eres parte de Wilbby",
                data: null,
            };
        }
        const nuevoUsuario = new userAdmin_1.default({
            name: input.nombre,
            lastName: input.apellidos,
            email: input.email,
            password: input.password,
            termAndConditions: input.termAndConditions,
        });
        nuevoUsuario.id = nuevoUsuario._id;
        return new Promise((resolve, object) => {
            nuevoUsuario.save((error) => {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Usuario agregado con éxito",
                        data: nuevoUsuario,
                    });
                }
            });
        });
    }),
    autenticarAdmin: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield userAdmin_1.default.findOne({ email });
        if (!users) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.USER_NOT_FOUND,
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, users.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.INCORRECT_PASSWORD,
                data: null,
            };
        }
        return {
            success: true,
            message: "Bienvenid@ a Wilbby®",
            data: {
                token: crearToken(users, process.env.SECRETO, "5000hr"),
                id: users._id,
            },
        };
    }),
    singleUpload(parent, { file }) {
        const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
        const ext = matches[1];
        const base64_data = matches[2];
        const buffer = Buffer.from(base64_data, "base64");
        const filename = `${Date.now()}-file.${ext}`;
        const filenameWithPath = `${__dirname}/../../uploads/images/${filename}`;
        return new Promise((resolve, reject) => {
            fs_1.default.writeFile(filenameWithPath, buffer, (error) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve({ filename });
                }
            });
        });
    },
    createCategory: (root, { input }) => {
        const nuevaCategoria = new categorias_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
        });
        return new Promise((resolve, rejects) => {
            nuevaCategoria.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    createTipo: (root, { input }) => {
        const nuevatipo = new tipo_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
        });
        return new Promise((resolve, rejects) => {
            nuevatipo.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    createHighkitchenCategory: (root, { input }) => {
        const highkitchenCategory = new highkitchenCategory_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
        });
        return new Promise((resolve, rejects) => {
            highkitchenCategory.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    createTipoTienda: (root, { input }) => {
        const nuevatipo = new tipotienda_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
        });
        return new Promise((resolve, rejects) => {
            nuevatipo.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    eliminarTipoTienda: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            tipotienda_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    rejects(error);
                else
                    resolve("Eliminado correctamente");
            });
        });
    },
    eliminarTipo: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            tipo_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    rejects(error);
                else
                    resolve("Eliminado correctamente");
            });
        });
    },
    eliminarCategory: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            categorias_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    rejects(error);
                else
                    resolve("Eliminado correctamente");
            });
        });
    },
    actualizarAdmin: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            userAdmin_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, _usuario) => {
                if (error)
                    reject(error);
                else
                    resolve(_usuario);
            });
        });
    }),
    actualizarUsuario: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            user_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, _usuario) => {
                if (error)
                    reject(error);
                else
                    resolve(_usuario);
            });
        });
    }),
    actualizarRiders: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            riders_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, _usuario) => {
                if (error)
                    reject(error);
                else
                    resolve(_usuario);
            });
        });
    }),
    eliminarAdmin: (root, { id }) => {
        return new Promise((resolve, reject) => {
            userAdmin_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Usuario eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    eliminarUsuario: (root, { id }) => {
        return new Promise((resolve, reject) => {
            user_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Usuario eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    eliminarRiders: (root, { id }) => {
        return new Promise((resolve, reject) => {
            riders_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Usuario eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    createRestaurant: (root, { input }) => {
        const nuevoRestaurant = new restaurant_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
            categoryName: input.categoryName,
            categoryID: input.categoryID,
            minime: input.minime,
            shipping: input.shipping,
            extras: input.extras,
            phone: input.phone,
            autoshipping: input.autoshipping,
            alegeno_url: input.alegeno_url,
            email: input.email,
            logo: input.logo,
            inOffert: input.inOffert,
            previous_shipping: input.previous_shipping,
            tipo: input.tipo,
            password: input.password,
            includeCity: input.includeCity,
            type: input.type,
            open: input.open,
            isnew: input.isnew,
            llevar: input.llevar,
            highkitchen: input.highkitchen,
            stimateTime: input.stimateTime,
            slug: input.slug,
            ispartners: input.ispartners,
            schedule: input.schedule,
            channelLinkId: input.channelLinkId,
            collections: input.collections,
            isDeliverectPartner: input.isDeliverectPartner,
            adress: input.adress,
            city: input.city,
            contactCode: input.contactCode,
        });
        return new Promise((resolve, rejects) => {
            nuevoRestaurant.save((error, res) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    eliminarRestaurant: (root, { id }) => {
        return new Promise((resolve, reject) => {
            restaurant_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Restaurante eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    actualizarRestaurant: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            restaurant_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un error con tu solicitud vuelve a intentalo por favor",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Datos actualizado con éxito",
                        success: true,
                    });
            });
        });
    }),
    crearFavorito: (root, { restaurantID, usuarioId }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
                data: null,
            };
        }
        const favorito = new Favorito_1.default({
            usuarioId,
            restaurantID,
        });
        favorito.id = favorito._id;
        return new Promise((resolve, reject) => {
            favorito.save((error) => {
                if (error) {
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                }
                else {
                    resolve({
                        messages: "Farmacia añadido a favorito",
                        success: true,
                    });
                }
            });
        });
    }),
    crearCart: (root, { PlatoId, usuarioId, cantd }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
                data: null,
            };
        }
        const isExist = yield cartAdd_1.default.findOne({ usuarioId, PlatoId });
        if (!isExist) {
            const cart = new cartAdd_1.default({
                usuarioId,
                PlatoId,
                cantd,
            });
            cart.id = cart._id;
            return new Promise((resolve, reject) => {
                cart.save((error) => {
                    if (error) {
                        reject({
                            messages: "Hay un problema con tu solicitud",
                            success: false,
                        });
                    }
                    else {
                        resolve({
                            messages: "Producto añadido al carrito",
                            success: true,
                        });
                    }
                });
            });
        }
        else {
            const input = {
                usuarioId,
                PlatoId,
                cantd,
            };
            return new Promise((resolve, reject) => {
                cartAdd_1.default.findOneAndUpdate({ PlatoId: PlatoId }, input, { new: true }, (error, card) => {
                    if (error)
                        reject(error);
                    else
                        resolve(card);
                });
            });
        }
    }),
    eliminarCart: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            cartAdd_1.default.findOneAndDelete({ PlatoId: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Produto eliminado del carrito",
                        success: true,
                    });
            });
        });
    }),
    eliminarFavorito: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            Favorito_1.default.findOneAndDelete({ restaurantID: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Restaurante eliminado de favorito",
                        success: true,
                    });
            });
        });
    }),
    createPlatos: (root, { input }) => {
        const newPlatos = new platos_1.default({
            title: input.title,
            ingredientes: input.ingredientes,
            price: input.price,
            previous_price: input.previous_price,
            imagen: input.imagen,
            menu: input.menu,
            restaurant: input.restaurant,
            oferta: input.oferta,
            popular: input.popular,
            news: input.news,
            plu: input.plu,
        });
        return new Promise((resolve, rejects) => {
            newPlatos.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    actualizarPlato: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            platos_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error) => {
                if (error)
                    reject({
                        messages: "Algo no va bien intentalo de nuevo",
                        success: true,
                    });
                else
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
            });
        });
    }),
    createMenu: (root, { input }) => {
        const newMenu = new menu_1.default({
            title: input.title,
            subtitle: input.subtitle,
            restaurant: input.restaurant,
        });
        return new Promise((resolve, rejects) => {
            newMenu.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    actualizarMenu: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            menu_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error) => {
                if (error)
                    reject({
                        messages: "Algo no va bien intentalo de nuevo",
                        success: true,
                    });
                else
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
            });
        });
    }),
    createAcompanante: (root, { input }) => {
        const newAcompanante = new acompanante_1.default({
            name: input.name,
            children: input.children,
            plato: input.plato,
            required: input.required,
            restaurant: input.restaurant,
        });
        return new Promise((resolve, rejects) => {
            newAcompanante.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    createItemCard: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const existenPlato = yield card_1.default.findOne({
            platoID: input.platoID,
            userId: input.userId,
        });
        if (existenPlato) {
            return new Promise((resolve, reject) => {
                card_1.default.findOneAndUpdate({ _id: existenPlato._id }, input, { new: true }, (error, card) => {
                    if (error)
                        reject(error);
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                        });
                    }
                });
            });
        }
        else {
            const newCardItem = new card_1.default({
                userId: input.userId,
                restaurant: input.restaurant,
                plato: input.plato,
                platoID: input.platoID,
                total: input.total,
                extra: input.extra,
                complementos: input.complementos ? input.complementos : [],
            });
            return new Promise((resolve, rejects) => {
                newCardItem.save((error) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                        });
                    }
                });
            });
        }
    }),
    actualizarCardItem: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            card_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, card) => {
                if (error)
                    reject(error);
                else
                    resolve(card);
            });
        });
    }),
    eliminarCardItem: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            card_1.default.findOneAndDelete({ platoID: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Producto eliminado de tu carrito",
                        success: true,
                    });
            });
        });
    }),
    crearCupon: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const nuevoCupon = new cupones_1.default({
                clave: input.clave,
                descuento: input.descuento,
                tipo: input.tipo,
            });
            return new Promise((resolve, reject) => {
                nuevoCupon.save((error, cupon) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        return resolve(cupon);
                    }
                });
            });
        }
        catch (error) {
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Hay un problema con su solicitud",
                    data: null,
                });
            });
        }
    }),
    eliminarCupon: (root, { id }) => {
        return new Promise((resolve, reject) => {
            cupones_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                    });
                else
                    resolve({
                        success: true,
                        message: "Miembro eliminado con éxito",
                    });
            });
        });
    },
    crearModificarOrden: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (!usuarioActual) {
                return {
                    success: false,
                    message: "Debes iniciar sesión para continiar",
                    data: null,
                };
            }
            let orden;
            if (input.id) {
                orden = yield order_1.default.findById(ObjectId(input.id)).exec();
                orden.set({ riders: input.riders ? input.riders : null });
            }
            if (!input.restaurant) {
                const restaurants = yield restaurant_1.default
                    .findById(ObjectId(input.restaurant))
                    .exec();
                if (!restaurants) {
                    return new Promise((_resolve, reject) => {
                        return reject({
                            success: false,
                            message: "No existe un restaurante para esta orden",
                            data: null,
                        });
                    });
                }
            }
            let cupon;
            if (!!input.clave) {
                cupon = yield cupones_1.default.findOne({ clave: input.clave }).exec();
            }
            else if (!!input.cupon) {
                cupon = input.cupon;
            }
            let userData;
            userData = yield user_1.default.findOne({
                _id: input.userID,
            });
            let AdreesData;
            AdreesData = yield adress_1.default.findOne({
                _id: input.adress,
            });
            let StoreData;
            StoreData = yield restaurant_1.default.findOne({
                _id: input.restaurant,
            });
            let platos;
            platos = yield card_1.default.find({
                userId: input.userID,
                restaurant: input.restaurant,
            });
            if (!orden || !orden._id) {
                orden = new order_1.default({
                    display_id: input.display_id,
                    restaurant: input.restaurant,
                    time: input.time,
                    cupon: cupon && cupon._id ? cupon._id : cupon,
                    nota: input.nota,
                    aceptaTerminos: input.aceptaTerminos,
                    cantidad: input.cantidad || 1,
                    userID: input.userID,
                    platos: platos,
                    cubiertos: input.cubiertos,
                    total: input.total,
                    propina: input.propina,
                    adress: input.adress,
                    city: input.city,
                    isvalored: input.isvalored,
                    riders: input.riders,
                    llevar: input.llevar,
                    subtotal: input.subtotal,
                    complement: input.complement,
                    descuentopromo: input.descuentopromo,
                    tarifa: input.tarifa,
                    envio: input.envio,
                    propinaTotal: input.propinaTotal,
                    schedule: input.schedule,
                    userData: userData,
                    AdreesData: AdreesData,
                    StoreData: StoreData,
                    holdedID: input.holdedID,
                    holdedRidersID: input.holdedRidersID,
                    holdedPartnerID: input.holdedPartnerID,
                    AdreesStoreData: input.AdreesStoreData,
                    reportRiders: input.reportRiders,
                });
            }
            else {
                if (!orden.cupon) {
                    orden.set({ cupon: cupon ? cupon._id : null });
                }
            }
            return new Promise((resolve, reject) => {
                orden.save((error, ordenguardada) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        if (!ordenguardada.descuento && cupon && cupon._id) {
                            ordenguardada.descuento = cupon;
                        }
                        distance_1.setDistanceToOrder(ordenguardada._id);
                        return resolve(ordenguardada);
                    }
                });
            });
        }
        catch (error) {
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Hay un problema con tu solicitud",
                    data: null,
                });
            });
        }
    }),
    crearValoracion: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const nuevoValoracion = new rating_1.default({
                user: input.user,
                comment: input.comment,
                value: input.value,
                restaurant: input.restaurant,
            });
            return new Promise((resolve, reject) => {
                nuevoValoracion.save((error, rating) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        return resolve(rating);
                    }
                });
            });
        }
        catch (error) {
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Hay un problema con su solicitud",
                    data: null,
                });
            });
        }
    }),
    createNotification: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para continiar",
                success: false,
            };
        }
        let newNotification = new Notification_1.default({
            user: input.user,
            usuario: input.usuario,
            restaurant: input.restaurant,
            type: input.type,
            ordenId: input.ordenId,
            riders: input.riders,
        });
        return new Promise((resolve, reject) => {
            newNotification.save((error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Notificacion leída con éxito",
                        success: true,
                    });
            });
        });
    }),
    readNotification: (root, { notificationId }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para continiar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            Notification_1.default.findOneAndUpdate({ _id: ObjectId(notificationId) }, { $set: { read: true } }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Notificacion leída con éxito",
                        success: true,
                    });
            });
        });
    }),
    createOpinion: (root, { input }) => {
        const nuevaOpinion = new Opinion_1.default({
            plato: input.plato,
            comment: input.comment,
            rating: input.rating,
            user: input.user,
        });
        return new Promise((resolve, rejects) => {
            nuevaOpinion.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    OrdenProceed: (root, { orden, estado, progreso, status }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            const message = estado === "Confirmada" ? "Confirmada" : "Rechazada";
            let dataToUpdate = { estado };
            //@ts-ignore
            if (progreso)
                dataToUpdate.progreso = progreso;
            //@ts-ignore
            if (status)
                dataToUpdate.status = status;
            order_1.default.findOneAndUpdate({ _id: orden }, dataToUpdate, (error, order) => __awaiter(void 0, void 0, void 0, function* () {
                if (error) {
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                }
                else {
                    if (order.riders && estado === "Listo para recoger") {
                        const ridersS = yield riders_1.default.findOne({ _id: order.riders });
                        sendNotificationRider_1.sendNotification(ridersS === null || ridersS === void 0 ? void 0 : ridersS.OnesignalID, `${order.StoreData.title} tiene el pedido listo para recoger.`);
                    }
                    resolve({
                        messages: "Orden procesada con éxito",
                        success: true,
                    });
                }
            }));
        });
    }),
    eliminarPlato: (root, { id }) => {
        return new Promise((resolve, reject) => {
            platos_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Plato eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarComplemento: (root, { id }) => {
        return new Promise((resolve, reject) => {
            acompanante_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Complemento eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarMenu: (root, { id }) => {
        return new Promise((resolve, reject) => {
            menu_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Menú eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    crearPago: (root, { input }) => {
        const nuevoPago = new Pago_1.default({
            nombre: input.nombre,
            iban: input.iban,
            restaurantID: input.restaurantID,
        });
        nuevoPago.id = nuevoPago._id;
        return new Promise((resolve, reject) => {
            nuevoPago.save((error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Pago añadido con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarPago: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            Pago_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Pago eliminado con éxito",
                        success: true,
                    });
            });
        });
    }),
    crearDeposito: (root, { input }) => {
        const nuevoDeposito = new transacciones_1.default({
            fecha: new Date(),
            estado: input.estado,
            total: input.total,
            restaurantID: input.restaurantID,
        });
        nuevoDeposito.id = nuevoDeposito._id;
        return new Promise((resolve, reject) => {
            nuevoDeposito.save((error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Deposito añadido con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarDeposito: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            transacciones_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                    });
                else
                    resolve({
                        success: true,
                        messages: "Deposito eliminado con éxito",
                    });
            });
        });
    }),
    createAdress: (root, { input }) => {
        const nuevaadress = new adress_1.default({
            formatted_address: input.formatted_address,
            puertaPiso: input.puertaPiso,
            type: input.type,
            usuario: input.usuario,
            lat: input.lat,
            lgn: input.lgn,
        });
        return new Promise((resolve, rejects) => {
            nuevaadress.save((error, adress) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: adress,
                    });
                }
            });
        });
    },
    createStoreAdress: (root, { input }) => {
        const nuevaadress = new adressStore_1.default({
            calle: input.calle,
            numero: input.numero,
            codigoPostal: input.codigoPostal,
            ciudad: input.ciudad,
            store: input.store,
            lat: input.lat,
            lgn: input.lgn,
        });
        return new Promise((resolve, rejects) => {
            nuevaadress.save((error, adress) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        success: true,
                        message: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        data: adress,
                    });
                }
            });
        });
    },
    actualizarAdress: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            adress_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error, adress) => {
                if (error)
                    reject(error);
                else
                    resolve({
                        messages: "Dirección actualizada con éxito",
                        success: true,
                        data: adress,
                    });
            });
        });
    }),
    eliminarAdress: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            adress_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Dirección eliminado con éxito",
                        success: true,
                    });
            });
        });
    }),
    eliminarAdressStore: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            adressStore_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Dirección eliminado con éxito",
                        success: true,
                    });
            });
        });
    }),
    eliminarPost: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield userAdmin_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: true,
                messages: "No estas autorizado para eliminar un post",
                data: null,
            };
        }
        else {
            return new Promise((resolve, reject) => {
                post_1.default.findOneAndDelete({ _id: id }, (error) => {
                    if (error)
                        reject({
                            message: "Hay un problema con su solicitud",
                            success: false,
                        });
                    else
                        resolve({
                            messages: "Dirección eliminado con éxito",
                            success: true,
                        });
                });
            });
        }
    }),
    actualizarPost: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield userAdmin_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: true,
                messages: "No estas autorizado para actualizar un post",
                data: null,
            };
        }
        else {
            return new Promise((resolve, reject) => {
                post_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error, post) => {
                    if (error)
                        reject(error);
                    else
                        resolve({
                            success: true,
                            message: "Dirección actualizada con éxito",
                            data: post,
                        });
                });
            });
        }
    }),
    createPost: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield userAdmin_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: true,
                message: "No estas autorizado para crear un post",
                data: null,
            };
        }
        else {
            const nuevoPost = new post_1.default({
                title: input.title,
                image: input.image,
                shortDescription: input.shortDescription,
                like: input.like,
                tags: input.tags,
                author: input.author,
                category: input.category,
                readTime: input.readTime,
                content: input.content,
                slug: input.slug,
                country: input.country,
            });
            return new Promise((resolve, rejects) => {
                nuevoPost.save((error, post) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        resolve({
                            success: true,
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            data: post,
                        });
                    }
                });
            });
        }
    }),
    createCustonOrder: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield user_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: true,
                messages: "No estas autorizado para crear un esta orden",
                data: null,
            };
        }
        else {
            const nuevaOrder = new custonorder_1.default({
                display_id: input.display_id,
                riders: input.riders,
                origin: input.origin,
                destination: input.destination,
                schedule: input.schedule,
                distance: input.distance,
                nota: input.nota,
                date: input.date,
                city: input.city,
                userID: input.userID,
                estado: input.estado,
                status: input.status,
                progreso: input.progreso,
                total: input.total,
                product_stimate_price: input.product_stimate_price,
            });
            return new Promise((resolve, rejects) => {
                nuevaOrder.save((error, order) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        resolve({
                            success: true,
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            data: order,
                        });
                    }
                });
            });
        }
    }),
    actualizarOrderProcess: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            custonorder_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error, order) => {
                if (error)
                    reject(error);
                else
                    resolve({
                        success: true,
                        messages: "Dirección actualizada con éxito",
                        data: order,
                    });
            });
        });
    }),
    actualizarCity: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            cityclose_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error) => {
                if (error)
                    reject({
                        messages: "Algo va mal intentalo de Nuevo",
                        success: true,
                    });
                else
                    resolve({
                        messages: "Dirección actualizada con éxito",
                        success: true,
                    });
            });
        });
    }),
    createCity: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaCity = new cityclose_1.default({
            close: input.close,
            city: input.city,
            title: input.title,
            subtitle: input.subtitle,
            imagen: input.imagen,
        });
        return new Promise((resolve, rejects) => {
            nuevaCity.save((error, order) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    createOffert: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaOffert = new offerts_1.default({
            store: input.store,
            slug: input.slug,
            city: input.city,
            apertura: input.apertura,
            cierre: input.cierre,
            imagen: input.imagen,
            open: input.open,
        });
        return new Promise((resolve, rejects) => {
            nuevaOffert.save((error, order) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    eliminarOfferts: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            offerts_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Algo va mal intentalo de Nuevo",
                        success: true,
                    });
                else
                    resolve({
                        messages: "Oferta eliminada con éxito",
                        success: true,
                    });
            });
        });
    }),
    createTransactionRider: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevatransaction = new trasactionRider_1.default({
            rider: input.rider,
            km: input.km,
            base: input.base,
            time: input.time,
            order: input.order,
            extra: input.extra,
            combinado: input.combinado,
            lluvia: input.lluvia,
            total: input.total,
            propina: input.propina,
            iva: input.iva,
        });
        return new Promise((resolve, rejects) => {
            nuevatransaction.save((error, order) => __awaiter(void 0, void 0, void 0, function* () {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    const riders = yield riders_1.default.findOne({ _id: input.rider });
                    CompraRiders_1.RegiterCompraRider(riders, input);
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            }));
        });
    }),
    createQuincena: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaquincena = new quincena_1.default({
            fromDate: input.fromDate,
            toDate: input.toDate,
            numberQuincena: input.numberQuincena,
        });
        return new Promise((resolve, rejects) => {
            nuevaquincena.save((error) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    createCollection: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaCollection = new collections_1.default({
            title: input.title,
            image: input.image,
            store: input.store,
            sorting: input.sorting,
        });
        return new Promise((resolve, rejects) => {
            nuevaCollection.save((error) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    createsubCollection: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevasubCollection = new subcollection_1.default({
            title: input.title,
            collectiontype: input.collectiontype,
            sorting: input.sorting,
        });
        return new Promise((resolve, rejects) => {
            nuevasubCollection.save((error) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    addToCart: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const newCart = new addToCart_1.default({
            userId: input.userId,
            storeId: input.storeId,
            productId: input.productId,
            items: input.items,
            addToCart: input.addToCart,
        });
        const exist = yield addToCart_1.default.findOne({ productId: input.productId });
        if (exist) {
            return new Promise((resolve, rejects) => {
                addToCart_1.default.findOneAndUpdate({ productId: input.productId }, input, { new: true }, (error) => {
                    if (error) {
                        rejects({
                            messages: "Algo va mal intentalo de nuevo",
                            success: true,
                        });
                    }
                    else {
                        resolve({
                            messages: "Producto añadido a la cesta",
                            success: true,
                        });
                    }
                });
            });
        }
        else {
            return new Promise((resolve, rejects) => {
                newCart.save((error) => {
                    if (error) {
                        rejects({
                            messages: "Algo va mal intentalo de nuevo",
                            success: true,
                        });
                    }
                    else {
                        resolve({
                            messages: "Producto añadido a la cesta",
                            success: true,
                        });
                    }
                });
            });
        }
    }),
    deleteCartItem: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            addToCart_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Producto eliminado de la cesta",
                        success: true,
                    });
            });
        });
    }),
};
