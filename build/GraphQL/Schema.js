"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeDefs = void 0;
const apollo_server_express_1 = require("apollo-server-express");
exports.typeDefs = apollo_server_express_1.gql `
  scalar Date
  scalar JSON
  scalar JSONObject

  type CollectionResponse {
    success: Boolean!
    message: String
    data: [Collections]
  }

  type QuincenaResponse {
    success: Boolean!
    message: String
    data: Quincena
  }

  type QuincenaAllResponse {
    success: Boolean!
    message: String
    list: [Quincena]
  }

  type TransactionRiderResponse {
    success: Boolean!
    message: String
    list: [TransacctionRider]
  }

  type CrearUsuarioResponse {
    success: Boolean!
    message: String
    data: Usuario
  }

  type getStatisticsResponse {
    success: Boolean!
    message: String
    data: [Statistics]
  }

  type PagoResponseList {
    messages: String
    success: Boolean!
    data: Pago
  }

  type CustomerAdminResponse {
    success: Boolean
    messages: String
    data: [Usuario]
  }

  type transitionResponselist {
    messages: String
    success: Boolean!
    list: [Transacion]
  }

  type Transacion {
    id: ID
    fecha: String
    estado: String
    total: String
    restaurantID: String
    created_at: String
  }

  type generarResponse {
    messages: String
    success: Boolean
  }

  type RestaurantgenerarResponse {
    messages: String
    success: Boolean
    data: Restaurant
  }

  type AdresscreateResponse {
    messages: String!
    success: Boolean!
    data: Adress
  }

  type AutenticarRestaurantResponse {
    success: Boolean!
    message: String
    data: AutenticarRestaurent
  }

  type AutenticarRestaurent {
    token: String!
    id: String!
  }

  type AutenticarusuarioResponse {
    success: Boolean!
    message: String
    data: AutenticarUsuario
  }

  type AutenticarUsuario {
    token: String!
    id: String!
    verifyPhone: Boolean
  }

  type OrdenListResponse {
    success: Boolean!
    message: String
    list: [Orden]
  }

  type Pago {
    _id: String
    nombre: String
    iban: String
    restaurantID: String
  }

  type AddressStoreResponse {
    success: Boolean!
    message: String
    data: AddresStore
  }

  type AddressStoreForselecResponse {
    success: Boolean!
    message: String
    data: [AddresStore]
  }

  type restaurantResponse {
    success: Boolean!
    message: String
    list: [RestaurantFavorito]
  }

  type ItemCartResponse {
    success: Boolean!
    message: String
    list: [Cart]
  }

  type getNotificationResponse {
    messages: String
    success: Boolean
    notifications: [Notification]
  }

  type CityResponse {
    success: Boolean
    messages: String
    data: City
  }

  type OffertsResponse {
    success: Boolean
    messages: String
    data: [Offerts]
  }

  type Notification {
    _id: String
    user: ID
    usuario: String
    read: Boolean
    ordenId: ID
    riders: String
    Riders: Usuario
    restaurant: String
    type: String
    created_at: String
    Usuarios: Usuario
    Restaurant: Restaurant
    Orden: Orden
  }

  type AcompananteResponse {
    success: Boolean!
    message: String
    list: [Acompanante]
  }

  type Acompanante {
    id: String
    name: String
    children: [Children]
    restaurant: String
    required: Boolean
    plato: String
  }

  type Children {
    id: Int
    name: String
    price: String
    recomended: Boolean
    cant: Int
    plu: String
  }

  type menuResponse {
    success: Boolean!
    message: String
    list: [Menu]
  }

  type menuResponseMarca {
    success: Boolean!
    message: String
    list: Menu
  }

  type platoResponse {
    success: Boolean!
    message: String
    list: [Platos]
  }

  type OrderListResponse {
    success: Boolean
    message: String
    list: [Orden]
  }

  type OrderListResponseID {
    success: Boolean
    message: String
    list: Orden
  }

  type RiderAdminResponse {
    success: Boolean!
    messages: String
    data: [Usuario]
  }

  type AdressResponse {
    success: Boolean
    message: String
    data: [Adress]
  }

  type ValoracionResponse {
    messages: String
    success: Boolean
    data: [Valoracion]
  }

  type ProductoSearchResponse {
    messages: String
    success: Boolean
    data: [Platos]
  }

  type NewProductoSearchResponse {
    messages: String
    success: Boolean
    data: [Products]
  }

  type OpinionResponse {
    messages: String
    success: Boolean
    data: [Opinion]
  }

  type PostResponse {
    success: Boolean
    message: String
    data: [Post]
  }

  type SinglePostResponse {
    success: Boolean
    message: String
    data: Post
  }

  type CustonOrderResponse {
    success: Boolean
    messages: String
    data: CustonOrder
  }

  type CustonOrderResponseQuery {
    success: Boolean
    messages: String
    data: [CustonOrder]
  }

  type Cupon {
    id: ID
    clave: String
    descuento: Float
    tipo: String
  }

  type Menu {
    _id: String
    title: String
    subtitle: String
    restaurant: String
  }

  type PlatoOrder {
    userId: String
    platoID: String
    restaurant: String
    complementos: [Complementos]
    plato: Platos
  }

  type Statistics {
    name: String
    Ene: Int
    Feb: Int
    Mar: Int
    Abr: Int
    May: Int
    Jun: Int
    Jul: Int
    Aug: Int
    Sep: Int
    Oct: Int
    Nov: Int
    Dic: Int
  }

  type Adress {
    id: String
    formatted_address: String
    puertaPiso: String
    usuario: String
    type: String
    lat: String
    lgn: String
  }

  type ReportRiders {
    km: String
    base: String
    time: String
    order: String
    extra: String
    combinado: String
    lluvia: String
    total: String
    propina: String
    iva: String
  }

  type Orden {
    id: ID
    display_id: Int
    cupon: ID
    nota: String
    adresStore: String
    aceptaTerminos: Boolean
    restaurant: String
    restaurants: Restaurant
    time: String
    cantidad: Int
    userID: ID
    riders: String
    adress: String
    AdreesStoreData: AddresStoreNew
    city: String
    Riders: Usuario
    cubiertos: Boolean
    Adress: Adress
    AdresStore: AddresStore
    usuario: Usuario
    descuento: Cupon
    propina: Boolean
    platos: [PlatoOrder]
    estado: String
    status: String
    progreso: String
    created_at: Date
    total: String
    isvalored: Boolean
    llevar: Boolean
    subtotal: String
    complement: String
    descuentopromo: String
    tarifa: String
    envio: String
    propinaTotal: String
    schedule: Boolean
    invoiceUrl: String
    holdedID: String
    holdedRidersID: String
    holdedPartnerID: String
    reportRiders: ReportRiders
  }

  type Cart {
    id: String
    userId: String
    platoID: String
    restaurant: String
    complementos: [Complementos]
    Restaurant: Restaurant
    plato: Platos
    total: String
    extra: String
    UserId: Usuario
  }

  type Platos {
    _id: String
    title: String
    ingredientes: String
    price: String
    previous_price: String
    imagen: String
    restaurant: String
    menu: String
    oferta: Boolean
    popular: Boolean
    anadidoCart: Boolean
    cant: Int
    cantd: Int
    opiniones: [Opinion]
    news: Boolean
    requireOption: Boolean
    out_of_stock: Boolean
    plu: String
    deliverectID: String
    SubProductos: [Acompanante]
    cartItems: Cart
  }

  type RestaurantFavorito {
    _id: String
    restaurantID: String
    usuarioId: String
    restaurant: Restaurant
  }

  type Opinion {
    id: String
    plato: String
    comment: String
    rating: Int
    created_at: Date
    user: String
    Usuario: Usuario
  }

  type Valoracion {
    id: String
    user: String
    comment: String
    value: Int
    created_at: Date
    restaurant: String
    Usuario: Usuario
  }

  type Days {
    day: String
    isOpen: Boolean
    openHour1: Int
    openHour2: Int
    openMinute1: Int
    openMinute2: Int
    closetHour1: Int
    closetHour2: Int
    closetMinute1: Int
    closetMinute2: Int
  }

  type Schedule {
    Monday: Days
    Tuesday: Days
    Wednesday: Days
    Thursday: Days
    Friday: Days
    Saturday: Days
    Sunday: Days
  }

  type AddresStore {
    _id: String
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
    store: String
    lat: String
    lgn: String
  }

  type AddresStoreNew {
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
    lat: String
    lgn: String
  }

  type Restaurant {
    _id: String
    title: String
    image: String
    description: String
    type: String
    anadidoFavorito: Boolean
    categoryName: String
    categoryID: String
    minime: Int
    shipping: String
    extras: String
    stimateTime: String
    slug: String
    highkitchen: Boolean
    previous_shipping: String
    alegeno_url: String
    Valoracion: [Valoracion]
    autoshipping: Boolean
    inOffert: Boolean
    ispartners: Boolean
    phone: String
    email: String
    llevar: Boolean
    logo: String
    password: String
    schedule: Schedule
    includeCity: [String]
    isnew: Boolean
    isDeliverectPartner: Boolean
    tipo: String
    open: Boolean
    OnesignalID: String
    channelLinkId: String
    collections: Boolean
    Menus: [Menu]
    CategoryNew: [CategoryNew]
    adress: AddresStoreNew
    city: String
    contactCode: String
    apertura: Int
    cierre: Int
    aperturaMin: Int
    cierreMin: Int
    diaslaborales: [String]
    rating: String
    menu: String
  }

  type Usuario {
    _id: String
    name: String
    lastName: String
    email: String
    city: String
    avatar: String
    telefono: String
    created_at: Date
    updated_at: Date
    termAndConditions: Boolean
    verifyPhone: Boolean
    isAvalible: Boolean
    StripeID: String
    PaypalID: String
    OnesignalID: String
    socialNetworks: String
    isSocial: Boolean
    rating: [Int]
    contactCode: String
  }

  type UsuarioResponse {
    messages: String!
    success: Boolean!
    data: Usuario
  }

  type RestaurantResponse {
    messages: String!
    success: Boolean!
    data: [Restaurant]
  }

  type RestaurantIDResponse {
    messages: String!
    success: Boolean!
    data: Restaurant
  }

  type CategoryResponse {
    messages: String!
    success: Boolean!
    data: [Category]
  }

  type Category {
    _id: String
    title: String
    image: String
    description: String
  }

  type File {
    filename: String
    mimetype: String
    encoding: String
  }

  type Complementos {
    cant: Int
    id: Int
    mid: String
    name: String
    price: String
  }

  type Card {
    id: String
    userId: String
    restaurant: String
    plato: String
    complementos: [Complementos]
  }

  type Post {
    id: String
    title: String
    image: String
    shortDescription: String
    like: Int
    tags: [String]
    author: String
    Author: Usuario
    category: String
    readTime: String
    content: String
    created_at: Date
    slug: String
    country: String
  }

  type address_custon {
    address_name: String
    address_number: String
    postcode: String
    type: String
    lat: String
    lgn: String
  }

  type CustonOrder {
    id: ID
    display_id: Int
    riders: String
    Riders: Usuario
    usuario: Usuario
    origin: address_custon
    destination: address_custon
    schedule: Boolean
    distance: String
    nota: String
    date: String
    city: String
    userID: ID
    estado: String
    status: String
    progreso: String
    created_at: Date
    total: String
    product_stimate_price: String
    reportRiders: ReportRiders
  }

  type City {
    id: ID
    close: Boolean
    city: String
    title: String
    subtitle: String
    imagen: String
  }

  type Offerts {
    id: String
    imagen: String
    city: String
    store: String
    slug: String
    apertura: Int
    cierre: Int
    open: Boolean
  }

  type TransacctionRider {
    id: String
    rider: String
    km: String
    base: String
    time: String
    order: String
    extra: String
    combinado: String
    lluvia: String
    total: String
    created_at: Date
    propina: String
    iva: String
  }

  type Quincena {
    _id: ID
    fromDate: String
    toDate: String
    numberQuincena: Int
  }

  type subCollection {
    _id: ID
    title: String
    collectiontype: String
    sorting: Int
  }

  type Collections {
    _id: ID
    title: String
    image: String
    subCollectionItems: [subCollection]
    store: String
    sorting: Int
  }

  type NewMenuResponse {
    success: Boolean!
    message: String
    list: [Menus]
  }

  type Bundles {
    _id: ID
    name: String
    description: String
    account: String
    location: String
    productType: String
    plu: String
    price: Int
    sortOrder: Int
    deliveryTax: Int
    takeawayTax: Int
    multiply: Int
    posProductId: String
    posProductCategoryId: [String]
    subProducts: [String]
    productTags: [String]
    posCategoryIds: [String]
    imageUrl: String
    max: Int
    min: Int
    recomended: Boolean
    quantity: Int
    capacityUsages: [String]
    parentId: String
    visible: Boolean
    snoozed: Boolean
    subProductSortOrder: [String]
    internalId: String
    Products: [Products]
    subItems: JSON
  }

  type ModifierGroups {
    _id: ID
    name: String
    description: String
    account: String
    location: String
    productType: String
    plu: String
    price: Int
    sortOrder: Int
    deliveryTax: Int
    takeawayTax: Int
    multiply: Int
    posProductId: String
    posProductCategoryId: [String]
    subProducts: [String]
    productTags: [String]
    posCategoryIds: [String]
    imageUrl: String
    max: Int
    min: Int
    recomended: Boolean
    quantity: Int
    capacityUsages: [String]
    parentId: String
    visible: Boolean
    snoozed: Boolean
    subProductSortOrder: [String]
    internalId: String
    Modifiers: [Modifiers]
    subItems: JSON
  }

  type Modifiers {
    _id: ID
    name: String
    description: String
    account: String
    location: String
    productType: String
    plu: String
    price: Int
    sortOrder: Int
    deliveryTax: Int
    takeawayTax: Int
    multiply: Int
    posProductId: String
    posProductCategoryId: [String]
    subProducts: [String]
    productTags: [String]
    posCategoryIds: [String]
    imageUrl: String
    max: Int
    min: Int
    recomended: Boolean
    quantity: Int
    capacityUsages: [String]
    parentId: String
    visible: Boolean
    snoozed: Boolean
    subProductSortOrder: [String]
    internalId: String
    ModifierGroups: [ModifierGroups]
    subItems: JSON
  }

  type CartItem {
    _id: ID
    userId: String
    storeId: String
    productId: String
    items: JSON
    addToCart: Boolean
  }

  type Products {
    _id: ID
    name: String
    description: String
    account: String
    location: String
    productType: Int
    plu: String
    price: Int
    sortOrder: Int
    deliveryTax: Int
    takeawayTax: Int
    multiply: Int
    posProductId: String
    posProductCategoryId: [String]
    subProducts: [String]
    productTags: [String]
    posCategoryIds: [String]
    imageUrl: String
    max: Int
    min: Int
    capacityUsages: [String]
    parentId: String
    visible: Boolean
    snoozed: Boolean
    subProductSortOrder: [String]
    ModifierGroups: [ModifierGroups]
    recomended: Boolean
    quantity: Int
    Bundles: [Bundles]
    internalId: String
    new: Boolean
    popular: Boolean
    offert: Boolean
    previous_price: Int
    cartItems: CartItem
    storeId: String
  }

  type CategoryNew {
    _id: ID
    name: String
    description: String
    account: String
    posLocationId: String
    posCategoryType: String
    subCategories: [String]
    posCategoryId: String
    imageUrl: String
    products: [String]
    Products: [Products]
    menu: String
    storeId: String
    sortedChannelProductIds: [String]
    subProductSortOrder: [String]
    subProducts: [String]
    level: Int
    availabilities: [String]
    internalId: String
  }

  type Menus {
    _id: ID
    menu: String
    menuId: String
    description: String
    menuImageURL: String
    menuType: Int
    availabilities: [String]
    productTags: [Int]
    currency: Int
    validations: [String]
    nestedModifiers: Boolean
    channelLinkId: String
    storeId: String
    Categories: [CategoryNew]
  }

  type Payment {
    mount: Int
  }

  type OrderProccess {
    status: String
    date: Date
  }

  type NewOrder {
    _id: ID
    channelOrderDisplayId: Int
    orderType: String
    pickupTime: Date
    estimatedPickupTime: Date
    deliveryTime: Date
    courier: ID
    courierData: Usuario
    customerData: Usuario
    customer: ID
    store: ID
    storeData: Restaurant
    deliveryAddressData: Adress
    deliveryAddress: ID
    orderIsAlreadyPaid: Boolean
    payment: Payment
    note: String
    items: [Products]
    numberOfCustomers: Int
    deliveryCost: Int
    serviceCharge: Int
    discountTotal: Int
    IntegerValue: Int
    paymentMethod: String
    cupon: ID
    statusProcess: OrderProccess
    Needcutlery: Boolean
    status: String
    holdedID: String
    holdedRidersID: String
    holdedPartnerID: String
    invoiceUrl: String
    reportRiders: ReportRiders
  }

  input PaymentInput {
    amount: Int
  }

  input OrderProccessInput {
    status: String
    date: Date
  }

  input NewOrderInput {
    _id: ID
    channelOrderDisplayId: Int
    orderType: String
    pickupTime: Date
    estimatedPickupTime: Date
    deliveryTime: Date
    courier: ID
    customer: ID
    store: ID
    deliveryAddress: ID
    orderIsAlreadyPaid: Boolean
    payment: PaymentInput
    note: String
    numberOfCustomers: Int
    deliveryCost: Int
    serviceCharge: Int
    discountTotal: Int
    IntegerValue: Int
    paymentMethod: String
    cupon: ID
    statusProcess: OrderProccessInput
    Needcutlery: Boolean
    status: String
    holdedID: String
    holdedRidersID: String
    holdedPartnerID: String
    invoiceUrl: String
  }

  input NewCartInput {
    userId: String
    storeId: String
    productId: String
    items: JSON
    addToCart: Boolean
  }

  input CategoryInput {
    title: String
    image: String
    description: String
  }

  input DaysInput {
    day: String
    isOpen: Boolean
    openHour1: Int
    openHour2: Int
    openMinute1: Int
    openMinute2: Int
    closetHour1: Int
    closetHour2: Int
    closetMinute1: Int
    closetMinute2: Int
  }

  input ScheduleInput {
    Monday: DaysInput
    Tuesday: DaysInput
    Wednesday: DaysInput
    Thursday: DaysInput
    Friday: DaysInput
    Saturday: DaysInput
    Sunday: DaysInput
  }

  input Addressinput {
    _id: String
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
    store: String
    lat: String
    lgn: String
  }

  input AddressinputNew {
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
    lat: String
    lgn: String
  }

  input RestaurantInput {
    _id: String
    title: String
    image: String
    description: String
    rating: String
    categoryName: String
    categoryID: String
    minime: Int
    shipping: Int
    extras: Int
    slug: String
    menu: String
    stimateTime: String
    highkitchen: Boolean
    inOffert: Boolean
    alegeno_url: String
    phone: String
    autoshipping: Boolean
    ispartners: Boolean
    previous_shipping: String
    email: String
    llevar: Boolean
    logo: String
    tipo: String
    password: String
    type: String
    schedule: ScheduleInput
    apertura: Int
    cierre: Int
    aperturaMin: Int
    cierreMin: Int
    diaslaborales: [String]
    includeCity: [String]
    open: Boolean
    isnew: Boolean
    channelLinkId: String
    collections: Boolean
    isDeliverectPartner: Boolean
    adress: AddressinputNew
    city: String
    contactCode: String
  }

  input ActualizarUsuarioInput {
    _id: String
    name: String
    lastName: String
    city: String
    email: String
    avatar: String
    telefono: String
    isAvalible: Boolean
    PaypalID: String
    rating: [Int]
  }

  input FileInput {
    filename: String
    mimetype: String
    encoding: String
  }

  input CreatePlatoInput {
    id: ID
    title: String
    ingredientes: String
    price: String
    previous_price: String
    restaurant: String
    imagen: String
    menu: String
    oferta: Boolean
    popular: Boolean
    news: Boolean
    out_of_stock: Boolean
    plu: String
  }

  input CreateMenuInput {
    id: String
    title: String
    subtitle: String
    restaurant: String
  }

  input CreateAcompananteInput {
    name: String
    children: [Childrens]
    restaurant: String
    required: Boolean
    plato: String
  }

  input Childrens {
    id: Int
    name: String
    price: String
    recomended: Boolean
    cant: Int
    plu: String
  }

  input PlatosCardInput {
    _id: String
    title: String
    ingredientes: String
    price: String
    imagen: String
    restaurant: String
    menu: String
    oferta: Boolean
    popular: Boolean
    cant: Int
    anadidoCart: Boolean
    plu: String
  }

  input InputComplementos {
    cant: Int
    id: Int
    mid: String
    name: String
    price: String
    plu: String
  }

  input CreateCardInput {
    userId: String
    restaurant: String
    plato: PlatosCardInput
    platoID: String
    total: String
    extra: String
    complementos: [InputComplementos]
  }

  input AtualizarCardInput {
    id: String
    userId: String
    restaurant: String
    platoID: String
    plato: PlatosCardInput
    total: String
    extra: String
    complementos: [InputComplementos]
  }

  input CuponCreationInput {
    clave: String!
    descuento: Int
    tipo: String
  }

  input ValoracionCreationInput {
    user: String
    comment: String
    value: Int
    restaurant: String
  }

  input ReportRidersInput {
    km: String
    base: String
    time: String
    order: String
    extra: String
    combinado: String
    lluvia: String
    total: String
    propina: String
    iva: String
  }

  input OrdenCreationInput {
    id: ID
    display_id: Int
    cupon: ID
    riders: String
    adress: String
    nota: String
    aceptaTerminos: Boolean
    restaurant: String
    time: String
    cantidad: Int
    adresStore: String
    city: String
    clave: String
    userID: ID
    propina: Boolean
    estado: String
    status: String
    progreso: String
    created_at: Date
    total: String
    isvalored: Boolean
    cubiertos: Boolean
    llevar: Boolean
    subtotal: String
    complement: String
    descuentopromo: String
    tarifa: String
    envio: String
    propinaTotal: String
    holdedID: String
    holdedRidersID: String
    holdedPartnerID: String
    schedule: Boolean
    AdreesStoreData: AddressinputNew
    reportRiders: ReportRidersInput
  }

  input DateRangeInput {
    fromDate: String
    toDate: String
  }

  input NotificationInput {
    user: String
    usuario: String
    ordenId: String
    restaurant: String
    type: String
    riders: String
  }

  input OpinionInput {
    plato: String
    comment: String
    rating: Int
    user: String
  }

  input PagoInput {
    nombre: String
    iban: String
    restaurantID: String
  }

  input trasitionInput {
    fecha: String
    estado: String
    total: String
    restaurantID: String
  }

  input UsuarioInput {
    nombre: String
    apellidos: String
    email: String
    password: String
    termAndConditions: Boolean
    telefono: String
    isAvalible: Boolean
    city: String
    contactCode: String
  }

  input adreesUser {
    formatted_address: String
    puertaPiso: String
    usuario: String
    type: String
    lat: String
    lgn: String
  }

  input adreesUserEdit {
    id: String
    formatted_address: String
    puertaPiso: String
    usuario: String
    type: String
    lat: String
    lgn: String
  }

  input PostInput {
    id: String
    title: String
    image: String
    shortDescription: String
    like: Int
    tags: [String]
    author: String
    category: String
    readTime: String
    content: String
    slug: String
    country: String
  }

  input address_custon_order {
    address_name: String
    address_number: String
    postcode: String
    type: String
    lat: String
    lgn: String
  }

  input CustonOrderInput {
    id: ID
    display_id: Int
    riders: String
    origin: address_custon_order
    destination: address_custon_order
    schedule: Boolean
    distance: String
    nota: String
    date: String
    city: String
    userID: ID
    estado: String
    status: String
    progreso: String
    created_at: Date
    total: String
    product_stimate_price: String
    reportRiders: ReportRidersInput
  }

  input CityInput {
    id: ID
    close: Boolean
    city: String
    title: String
    subtitle: String
    imagen: String
  }

  input deviceInfo {
    date: Date
    divice: String
    system: String
    location: String
    IPaddress: String
  }

  input Offert {
    imagen: String
    city: String
    store: String
    slug: String
    apertura: Int
    cierre: Int
    open: Boolean
  }

  input TransacctionRiderInput {
    id: String
    rider: String
    km: String
    base: String
    time: String
    order: String
    extra: String
    combinado: String
    lluvia: String
    total: String
    createdAt: Date
    propina: String
    iva: String
  }

  input QuincenaInput {
    _id: ID
    fromDate: String
    toDate: String
    numberQuincena: Int
  }

  input CollectionInput {
    _id: ID
    title: String
    image: String
    store: String
    sorting: Int
  }

  input subCollectionInput {
    _id: ID
    title: String
    collectiontype: String
    sorting: Int
  }

  type Query {
    getUsuario: UsuarioResponse
    getUsuarioAdmin: UsuarioResponse
    getRiders: UsuarioResponse
    getRiderForAdmin(city: String): RiderAdminResponse
    getRiderForAdminAll: RiderAdminResponse
    getCategory: CategoryResponse
    getTipo: CategoryResponse
    getHighkitchenCategory: CategoryResponse
    getTipoTienda: CategoryResponse
    getRestaurant(city: String): RestaurantResponse
    getStoreInOffert(city: String): RestaurantResponse
    getRestaurantSearch(
      city: String
      price: Int
      category: String
      search: String
      llevar: Boolean
      tipo: String
    ): RestaurantResponse
    getProductoSearch(
      farmacy: String
      search: String
      page: Int
      limit: Int
      category: String
    ): ProductoSearchResponse
    getRestaurantForCategory(
      city: String
      category: String
      tipo: String
      llevar: Boolean
    ): RestaurantResponse
    getRestaurantHighkitchen(
      city: String
      category: String
      llevar: Boolean
    ): RestaurantResponse
    getRestaurantForID(id: ID): RestaurantIDResponse
    getRestaurantForSlugWeb(slug: String): RestaurantIDResponse
    getRestaurantForDetails(slug: String, city: String): RestaurantIDResponse
    getRestaurantForIDWeb(id: ID, city: String): RestaurantIDResponse
    getRestaurantFavorito(id: ID): restaurantResponse
    getMenu(id: ID): menuResponse
    getMenumarca(id: ID): menuResponseMarca
    getPlato(id: ID, page: Int, limit: Int): platoResponse
    getPlatoCollection(id: ID): platoResponse
    getAcompanante(id: ID): AcompananteResponse
    getItemCart(id: ID, PlatoID: ID): ItemCartResponse
    getItemCarts(id: ID): ItemCartResponse
    getMyItemCart(id: ID, restaurant: ID): ItemCartResponse
    getCupon(clave: String): Cupon
    getCuponAll: Cupon
    getPosts(country: String, page: Int): PostResponse
    getPostsForTags(country: String, page: Int, tag: String): PostResponse
    getPost(country: String): SinglePostResponse
    getPostbyId(slug: String, country: String): SinglePostResponse

    getOrderByAdmin(
      id: Int
      city: String
      dateRange: DateRangeInput
    ): OrderListResponse

    getOrderByAdminAll(
      id: Int
      city: String
      dateRange: DateRangeInput
      page: Int
    ): OrderListResponse

    getOrderByUsuario(
      usuarios: ID
      dateRange: DateRangeInput
    ): OrderListResponse

    getOrderByUsuarioAll(
      usuarios: ID
      dateRange: DateRangeInput
    ): OrderListResponse

    getOrderByRiders(id: ID): OrderListResponse
    getOrderByRidersAll(id: ID, page: Int): OrderListResponse

    getOrderByRestaurantID(id: ID): OrderListResponseID
    getValoraciones(restaurant: ID): ValoracionResponse
    getNotifications(Id: ID): getNotificationResponse
    getNotificationsRiders(riders: ID): getNotificationResponse
    getOpinion(id: ID): OpinionResponse
    getPago(id: ID): PagoResponseList
    getTransaction(id: ID): transitionResponselist
    getStatistics(id: ID): getStatisticsResponse
    obtenerUsuario: Usuario
    obtenerAdmin: Usuario
    getAdress(id: ID): AdressResponse
    getAdressStore(id: ID, city: String): AddressStoreResponse
    getAdressStoreforSelect(id: ID): AddressStoreForselecResponse
    getCustomOrder(id: ID): CustonOrderResponseQuery
    getCustomOrderRider(id: ID): CustonOrderResponseQuery
    getCustomOrderAdmin: CustonOrderResponseQuery
    getOrderByRestaurantNew(
      id: ID
      dateRange: DateRangeInput
    ): OrderListResponse

    getOrderByRestaurant(
      id: ID
      dateRange: DateRangeInput
      page: Int
      orderID: Int
    ): OrderListResponse
    getCity(city: String): CityResponse
    getOfferts(city: String): OffertsResponse
    getCustomers(page: Int, limit: Int, email: String): CustomerAdminResponse
    getAllCustomers(email: String): CustomerAdminResponse
    getRiderTransaction(
      id: ID
      dateRange: DateRangeInput
      page: Int
    ): TransactionRiderResponse
    getQuincena: QuincenaResponse
    getQuincenaAll: QuincenaAllResponse
    getCollection(store: String): CollectionResponse

    getNewMenu(id: String): NewMenuResponse
    getNewProductoSearch(
      store: String
      search: String
      page: Int
      limit: Int
      category: String
    ): NewProductoSearchResponse
  }

  type Mutation {
    createOffert(input: Offert): generarResponse
    eliminarOfferts(id: ID): generarResponse
    createCategory(input: CategoryInput): generarResponse
    createTipo(input: CategoryInput): generarResponse
    createHighkitchenCategory(input: CategoryInput): generarResponse
    eliminarTipo(id: ID): generarResponse
    createTipoTienda(input: CategoryInput): generarResponse
    eliminarTipoTienda(id: ID): generarResponse
    createRestaurant(input: RestaurantInput): RestaurantgenerarResponse
    eliminarRestaurant: generarResponse
    eliminarCategory(id: ID): generarResponse
    actualizarRestaurant(input: RestaurantInput): generarResponse
    actualizarUsuario(input: ActualizarUsuarioInput): Usuario
    actualizarAdmin(input: ActualizarUsuarioInput): Usuario
    actualizarRiders(input: ActualizarUsuarioInput): Usuario
    singleUpload(file: Upload): File
    eliminarUsuario(id: ID!): generarResponse
    eliminarAdmin(id: ID!): generarResponse
    eliminarRiders(id: ID!): generarResponse
    crearFavorito(restaurantID: ID, usuarioId: ID): generarResponse
    crearCart(PlatoId: ID, usuarioId: ID, cantd: Int): generarResponse
    eliminarCart(id: ID): generarResponse
    eliminarFavorito(id: ID): generarResponse
    createPlatos(input: CreatePlatoInput): generarResponse
    actualizarPlato(input: CreatePlatoInput): generarResponse
    createMenu(input: CreateMenuInput): generarResponse
    actualizarMenu(input: CreateMenuInput): generarResponse
    createAcompanante(input: CreateAcompananteInput): generarResponse
    createItemCard(input: CreateCardInput): generarResponse
    actualizarCardItem(input: AtualizarCardInput): Card
    eliminarCardItem(id: ID): generarResponse
    crearCupon(input: CuponCreationInput): Cupon
    eliminarCupon(id: ID): generarResponse
    crearModificarOrden(input: OrdenCreationInput): Orden
    crearValoracion(input: ValoracionCreationInput): Valoracion
    readNotification(notificationId: ID): generarResponse
    createNotification(input: NotificationInput): generarResponse
    createOpinion(input: OpinionInput): generarResponse
    autenticarRestaurant(
      email: String
      password: String
    ): AutenticarRestaurantResponse

    autenticarUsuario(
      email: String
      password: String
      input: deviceInfo
    ): AutenticarusuarioResponse

    autenticarRiders(
      email: String
      password: String
    ): AutenticarRestaurantResponse

    autenticarAdmin(
      email: String
      password: String
    ): AutenticarRestaurantResponse

    OrdenProceed(
      orden: ID
      estado: String
      progreso: String
      status: String
    ): generarResponse
    eliminarPlato(id: ID): generarResponse
    eliminarComplemento(id: ID): generarResponse
    eliminarMenu(id: ID): generarResponse
    crearPago(input: PagoInput): generarResponse
    eliminarPago(id: ID): generarResponse
    crearDeposito(input: trasitionInput): generarResponse
    eliminarDeposito(id: ID): generarResponse
    crearUsuario(input: UsuarioInput): CrearUsuarioResponse
    crearRiders(input: UsuarioInput): CrearUsuarioResponse
    crearAdmin(input: UsuarioInput): CrearUsuarioResponse
    createAdress(input: adreesUser): AdresscreateResponse
    actualizarAdress(input: adreesUserEdit): AdresscreateResponse
    eliminarAdress(id: ID): generarResponse
    eliminarAdressStore(id: ID): generarResponse
    createStoreAdress(input: Addressinput): AddressStoreResponse
    eliminarPost(id: ID): generarResponse
    createPost(input: PostInput): generarResponse
    actualizarPost(input: PostInput): SinglePostResponse
    createCustonOrder(input: CustonOrderInput): CustonOrderResponse
    actualizarOrderProcess(input: CustonOrderInput): CustonOrderResponse
    createCity(input: CityInput): generarResponse
    actualizarCity(input: CityInput): generarResponse
    createTransactionRider(input: TransacctionRiderInput): generarResponse
    createQuincena(input: QuincenaInput): generarResponse
    createCollection(input: CollectionInput): generarResponse
    createsubCollection(input: subCollectionInput): generarResponse
    addToCart(input: NewCartInput): generarResponse
    deleteCartItem(id: ID): generarResponse
  }
`;
