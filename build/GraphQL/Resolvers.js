"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolvers = void 0;
const query_1 = require("./query");
const mutation_1 = require("./mutation");
const user_1 = __importDefault(require("../models/user"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const cartAdd_1 = __importDefault(require("../models/cartAdd"));
const card_1 = __importDefault(require("../models/card"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const rating_1 = __importDefault(require("../models/rating"));
const order_1 = __importDefault(require("../models/order"));
const Opinion_1 = __importDefault(require("../models/Opinion"));
const adress_1 = __importDefault(require("../models/adress"));
const adressStore_1 = __importDefault(require("../models/adressStore"));
const riders_1 = __importDefault(require("../models/riders"));
const acompanante_1 = __importDefault(require("../models/nemu/acompanante"));
const userAdmin_1 = __importDefault(require("../models/userAdmin"));
const subcollection_1 = __importDefault(require("../models/subcollection"));
const menu_1 = __importDefault(require("../models/nemu/menu"));
const category_1 = __importDefault(require("../models/newMenu/category"));
const bundles_1 = __importDefault(require("../models/newMenu/bundles"));
const addToCart_1 = __importDefault(require("../models/newOrder/addToCart"));
const modifierGroups_1 = __importDefault(require("../models/newMenu/modifierGroups"));
const modifiers_1 = __importDefault(require("../models/newMenu/modifiers"));
const products_1 = __importDefault(require("../models/newMenu/products"));
exports.resolvers = {
    Query: query_1.Query,
    Mutation: mutation_1.Mutation,
    Menus: {
        Categories(parent, args) {
            return new Promise((resolve, rejects) => {
                category_1.default.find({ menu: parent.menuId }, (error, category) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!category)
                            resolve([]);
                        else {
                            resolve(category);
                        }
                    }
                });
            });
        },
    },
    CategoryNew: {
        Products(parent, args) {
            return new Promise((resolve, rejects) => {
                products_1.default.find({
                    _id: parent.products.map((p) => {
                        return p;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!product)
                            resolve([]);
                        else {
                            resolve(product);
                        }
                    }
                });
            });
        },
    },
    Bundles: {
        Products(parent, args) {
            return new Promise((resolve, rejects) => {
                products_1.default.find({
                    _id: parent.subProducts.map((p) => {
                        return p;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!product)
                            resolve([]);
                        else {
                            resolve(product);
                        }
                    }
                });
            });
        },
    },
    Modifiers: {
        ModifierGroups(parent, args) {
            return new Promise((resolve, rejects) => {
                modifierGroups_1.default.find({
                    _id: parent.subProducts.map((p) => {
                        return p;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!product)
                            resolve([]);
                        else {
                            resolve(product);
                        }
                    }
                });
            });
        },
    },
    Products: {
        Bundles(parent, args) {
            return new Promise((resolve, rejects) => {
                bundles_1.default.find({
                    _id: parent.subProducts.map((p) => {
                        return p;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!product)
                            resolve([]);
                        else {
                            resolve(product);
                        }
                    }
                });
            });
        },
        ModifierGroups(parent, args) {
            return new Promise((resolve, rejects) => {
                modifierGroups_1.default.find({
                    _id: parent.subProducts.map((id) => {
                        return id;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!product)
                            resolve([]);
                        else {
                            resolve(product);
                        }
                    }
                });
            });
        },
        cartItems(parent, args, { usuarioActual }) {
            return new Promise((resolve, rejects) => {
                addToCart_1.default.findOne({
                    userId: usuarioActual._id,
                    productId: parent._id,
                }, (error, cart) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!cart)
                            resolve({});
                        else {
                            resolve(cart);
                        }
                    }
                });
            });
        },
    },
    ModifierGroups: {
        Modifiers(parent, args) {
            return new Promise((resolve, rejects) => {
                modifiers_1.default.find({
                    _id: parent.subProducts.map((p) => {
                        return p;
                    }),
                }, (error, product) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!product)
                            resolve([]);
                        else {
                            resolve(product);
                        }
                    }
                });
            });
        },
    },
    Collections: {
        subCollectionItems(parent, args) {
            return new Promise((resolve, rejects) => {
                subcollection_1.default.find({ collectiontype: parent._id }, (error, collection) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        if (!collection)
                            resolve(false);
                        else {
                            const collections = collection;
                            collections.sort((a, b) => 
                            //@ts-ignore
                            a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                            resolve(collections);
                        }
                    }
                });
            });
        },
    },
    CustonOrder: {
        Riders(parent, args) {
            return new Promise((resolve, rejects) => {
                riders_1.default.findOne({ _id: parent.riders }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
        usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.userID }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Post: {
        Author(parent, args) {
            return new Promise((resolve, rejects) => {
                userAdmin_1.default.findOne({ _id: parent.author }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Platos: {
        SubProductos(parent, args) {
            return __awaiter(this, void 0, void 0, function* () {
                const dataID = yield restaurant_1.default.findOne({ _id: parent.restaurant });
                const queryID = (dataID === null || dataID === void 0 ? void 0 : dataID.isDeliverectPartner) ? parent.deliverectID
                    : parent._id;
                return new Promise((resolve, rejects) => {
                    acompanante_1.default.find({ plato: queryID }, (error, option) => {
                        if (error)
                            rejects(error);
                        else {
                            if (!option)
                                resolve(false);
                            else {
                                resolve(option);
                            }
                        }
                    });
                });
            });
        },
        cartItems(parent, args) {
            return new Promise((resolve, rejects) => {
                card_1.default.findOne({ platoID: parent._id }, (error, cart) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!cart)
                            resolve(false);
                        else {
                            resolve(cart);
                        }
                    }
                });
            });
        },
        opiniones(parent, args) {
            return new Promise((resolve, rejects) => {
                Opinion_1.default.find({ plato: parent._id }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
        requireOption(parent, args) {
            return new Promise((resolve, rejects) => {
                acompanante_1.default.findOne({ plato: parent._id }, (error, option) => {
                    if (error)
                        rejects(error);
                    else {
                        if (option)
                            resolve(true);
                        else {
                            resolve(false);
                        }
                    }
                });
            });
        },
        anadidoCart(parent, args, { usuarioActual }) {
            return new Promise((resolve, reject) => {
                if (!usuarioActual)
                    resolve(false);
                user_1.default.findOne({ _id: usuarioActual._id }, (error, usuario) => {
                    if (error)
                        reject(error);
                    else {
                        if (!usuario)
                            resolve(false);
                        else {
                            cartAdd_1.default.findOne({ usuarioId: usuario._id, PlatoId: parent._id }, (error, favorito) => {
                                if (error)
                                    reject(error);
                                else {
                                    if (!favorito)
                                        resolve(false);
                                    else
                                        resolve(true);
                                }
                            });
                        }
                    }
                });
            });
        },
        cantd(parent, args) {
            return new Promise((resolve, rejects) => {
                cartAdd_1.default.findOne({ PlatoId: parent._id }, (error, plato) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!plato)
                            resolve(1);
                        else {
                            resolve(plato.cantd);
                        }
                    }
                });
            });
        },
    },
    Opinion: {
        Usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.user }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Notification: {
        Riders(parent, args) {
            return new Promise((resolve, rejects) => {
                riders_1.default.findOne({ _id: parent.riders }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
        Usuarios(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.usuario }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
        Restaurant(parent, args) {
            return new Promise((resolve, rejects) => {
                restaurant_1.default.findOne({ _id: parent.restaurant }, (error, restaurant) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!restaurant)
                            resolve(false);
                        else {
                            resolve(restaurant);
                        }
                    }
                });
            });
        },
        Orden(parent, args) {
            return new Promise((resolve, rejects) => {
                order_1.default.findOne({ _id: parent.ordenId }, (error, orden) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!orden)
                            resolve(false);
                        else {
                            resolve(orden);
                        }
                    }
                });
            });
        },
    },
    Valoracion: {
        Usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.user }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Orden: {
        AdresStore(parent, args) {
            return new Promise((resolve, rejects) => {
                adressStore_1.default.findOne({ _id: parent.adresStore }, (error, adress) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!adress)
                            resolve(false);
                        else {
                            resolve(adress);
                        }
                    }
                });
            });
        },
        Adress(parent, args) {
            return new Promise((resolve, rejects) => {
                adress_1.default.findOne({ _id: parent.adress }, (error, adress) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!adress)
                            resolve(false);
                        else {
                            resolve(adress);
                        }
                    }
                });
            });
        },
        Riders(parent, args) {
            return new Promise((resolve, rejects) => {
                riders_1.default.findOne({ _id: parent.riders }, (error, riders) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!riders)
                            resolve(false);
                        else {
                            resolve(riders);
                        }
                    }
                });
            });
        },
        restaurants(parent, args) {
            return new Promise((resolve, rejects) => {
                restaurant_1.default.findOne({ _id: parent.restaurant }, (error, restaurant) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!restaurant)
                            resolve(false);
                        else {
                            resolve(restaurant);
                        }
                    }
                });
            });
        },
        usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.userID }, (error, userID) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!userID)
                            resolve(false);
                        else {
                            resolve(userID);
                        }
                    }
                });
            });
        },
    },
    Restaurant: {
        anadidoFavorito(parent, args, { usuarioActual }) {
            return new Promise((resolve, reject) => {
                if (!usuarioActual)
                    resolve(false);
                user_1.default.findOne({ _id: usuarioActual._id }, (error, usuario) => {
                    if (error)
                        reject(error);
                    else {
                        if (!usuario)
                            resolve(false);
                        else {
                            Favorito_1.default.findOne({ usuarioId: usuario._id, restaurantID: parent._id }, (error, favorito) => {
                                if (error)
                                    reject(error);
                                else {
                                    if (!favorito)
                                        resolve(false);
                                    else
                                        resolve(true);
                                }
                            });
                        }
                    }
                });
            });
        },
        Valoracion(parent, args) {
            return new Promise((resolve, rejects) => {
                rating_1.default.find({ restaurant: parent._id }, (error, valoracion) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!valoracion)
                            resolve(false);
                        else {
                            resolve(valoracion);
                        }
                    }
                });
            });
        },
        Menus(parent, args) {
            return new Promise((resolve, rejects) => {
                menu_1.default.find({ restaurant: parent._id }, (error, menu) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!menu)
                            resolve(false);
                        else {
                            resolve(menu);
                        }
                    }
                });
            });
        },
        CategoryNew(parent, args) {
            return new Promise((resolve, rejects) => {
                category_1.default.find({ storeId: parent._id }, (error, menu) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!menu)
                            resolve(false);
                        else {
                            resolve(menu);
                        }
                    }
                });
            });
        },
    },
    RestaurantFavorito: {
        restaurant(parent) {
            return new Promise((resolve, reject) => {
                restaurant_1.default.findById({ _id: parent.restaurantID }, (error, restaurant) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(restaurant);
                    }
                });
            });
        },
    },
    Cart: {
        Restaurant(parent) {
            return new Promise((resolve, reject) => {
                restaurant_1.default.findById({ _id: parent.restaurant }, (error, restaurant) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(restaurant);
                    }
                });
            });
        },
        UserId(parent) {
            return new Promise((resolve, reject) => {
                user_1.default.findById({ _id: parent.userId }, (error, user) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(user);
                    }
                });
            });
        },
    },
};
