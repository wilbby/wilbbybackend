"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Query = void 0;
const user_1 = __importDefault(require("../models/user"));
const categorias_1 = __importDefault(require("../models/categorias"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const menu_1 = __importDefault(require("../models/nemu/menu"));
const acompanante_1 = __importDefault(require("../models/nemu/acompanante"));
const platos_1 = __importDefault(require("../models/nemu/platos"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const cupones_1 = __importDefault(require("../models/cupones"));
const riders_1 = __importDefault(require("../models/riders"));
const card_1 = __importDefault(require("../models/card"));
const order_1 = __importDefault(require("../models/order"));
const Status_messages_1 = require("./Status_messages");
const rating_1 = __importDefault(require("../models/rating"));
const Notification_1 = __importDefault(require("../models/Notification"));
const Opinion_1 = __importDefault(require("../models/Opinion"));
const Pago_1 = __importDefault(require("../models/Pago"));
const transacciones_1 = __importDefault(require("../models/transacciones"));
const adress_1 = __importDefault(require("../models/adress"));
const adressStore_1 = __importDefault(require("../models/adressStore"));
const tipo_1 = __importDefault(require("../models/tipo"));
const highkitchenCategory_1 = __importDefault(require("../models/highkitchenCategory"));
const tipotienda_1 = __importDefault(require("../models/tipotienda"));
const userAdmin_1 = __importDefault(require("../models/userAdmin"));
const post_1 = __importDefault(require("../models/post"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const cityclose_1 = __importDefault(require("../models/cityclose"));
const offerts_1 = __importDefault(require("../models/offerts"));
const trasactionRider_1 = __importDefault(require("../models/trasactionRider"));
const collections_1 = __importDefault(require("../models/collections"));
const Menu_1 = __importDefault(require("../models/newMenu/Menu"));
const products_1 = __importDefault(require("../models/newMenu/products"));
const dotenv_1 = __importDefault(require("dotenv"));
const quincena_1 = __importDefault(require("../models/quincena"));
dotenv_1.default.config({ path: "variables.env" });
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
exports.Query = {
    getRiderForAdmin: (root, { city }, { usuarioActual }) => {
        const isAdmin = userAdmin_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: false,
                messages: Status_messages_1.STATUS_MESSAGES.NOT_LOGGED_IN,
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                riders_1.default.find({ city: city, isAvalible: true }, (err, res) => {
                    if (err) {
                        rejects({
                            messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    getRiderForAdminAll: (root, {}, { usuarioActual }) => {
        const isAdmin = userAdmin_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: false,
                messages: Status_messages_1.STATUS_MESSAGES.NOT_LOGGED_IN,
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                riders_1.default.find((err, res) => {
                    if (err) {
                        rejects({
                            messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    getUsuario: (root, {}, { usuarioActual }) => {
        if (!usuarioActual) {
            return {
                success: false,
                messages: Status_messages_1.STATUS_MESSAGES.NOT_LOGGED_IN,
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: usuarioActual._id }, (err, res) => {
                    if (err) {
                        rejects({
                            messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    getUsuarioAdmin: (root, {}, { usuarioActual }) => {
        if (!usuarioActual) {
            return {
                success: false,
                messages: Status_messages_1.STATUS_MESSAGES.NOT_LOGGED_IN,
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                userAdmin_1.default.findOne({ _id: usuarioActual._id }, (err, res) => {
                    if (err) {
                        rejects({
                            messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    getRiders: (root, {}, { usuarioActual }) => {
        if (!usuarioActual) {
            return {
                success: false,
                messages: Status_messages_1.STATUS_MESSAGES.NOT_LOGGED_IN,
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                riders_1.default.findOne({ _id: usuarioActual._id }, (err, res) => {
                    if (err) {
                        rejects({
                            messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    obtenerUsuario: (root, args, { usuarioActual }) => {
        if (!usuarioActual) {
            return null;
        }
        const usuario = user_1.default.findOne({ _id: usuarioActual._id });
        return usuario;
    },
    obtenerAdmin: (root, args, { usuarioActual }) => {
        if (!usuarioActual) {
            return null;
        }
        const usuario = userAdmin_1.default.findOne({ _id: usuarioActual._id });
        return usuario;
    },
    getCategory: (root) => {
        return new Promise((resolve, rejects) => {
            categorias_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getTipo: (root) => {
        return new Promise((resolve, rejects) => {
            tipo_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getHighkitchenCategory: (root) => {
        return new Promise((resolve, rejects) => {
            highkitchenCategory_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getTipoTienda: (root) => {
        return new Promise((resolve, rejects) => {
            tipotienda_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantHighkitchen: (root, { city, llevar, category }) => {
        let condition = { includeCity: { $all: [city] }, highkitchen: true };
        //@ts-ignore
        if (llevar)
            condition.llevar = llevar;
        // @ts-ignore
        if (category)
            condition.tipo = category;
        return new Promise((resolve, rejects) => {
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getRestaurant: (root, { city }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.find({ includeCity: { $all: [city] } }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getAdress: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            adress_1.default
                .find({ usuario: id }, (err, res) => {
                if (err) {
                    rejects({
                        success: false,
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        data: {},
                    });
                }
                else {
                    resolve({
                        success: true,
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getAdressStore: (root, { id, city }) => {
        return new Promise((resolve, rejects) => {
            let condition = { store: id, ciudad: city };
            //@ts-ignore
            adressStore_1.default
                .findOne(condition, (err, res) => {
                if (err) {
                    rejects({
                        success: false,
                        message: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        data: {},
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getAdressStoreforSelect: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            let condition = { store: id };
            adressStore_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        success: false,
                        message: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        data: {},
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getRestaurantSearch: (root, { city, price, category, search, llevar, tipo }) => {
        let condition = { includeCity: { $all: [city] } };
        // @ts-ignore
        if (search)
            condition = { $text: { $search: `"\"${search} \""` } };
        // @ts-ignore
        if (price)
            condition.minime = { $lt: price };
        //@ts-ignore
        if (llevar)
            condition.llevar = llevar;
        // @ts-ignore
        if (category)
            condition.categoryID = category;
        // @ts-ignore
        if (tipo)
            condition.tipo = tipo;
        return new Promise((resolve, rejects) => {
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort(search ? null : { $natural: -1 });
        });
    },
    getProductoSearch: (root, { farmacy, search, page, limit, category }) => {
        let condition = {};
        // @ts-ignore
        if (category)
            condition.menu = category;
        // @ts-ignore
        if (search)
            condition = { $text: { $search: `"\"${search} \""` } };
        // @ts-ignore
        if (farmacy)
            condition.restaurant = farmacy;
        return new Promise((resolve, rejects) => {
            platos_1.default.find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    },
    getRestaurantForCategory: (root, { city, category, tipo, llevar }) => {
        return new Promise((resolve, rejects) => {
            let condition = { includeCity: { $all: [city] } };
            //@ts-ignore
            if (llevar)
                condition.llevar = llevar;
            //@ts-ignore
            if (tipo)
                condition.tipo = tipo;
            // @ts-ignore
            if (category)
                condition.categoryID = category;
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getRestaurantForID: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ _id: id }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForIDWeb: (root, { id, city }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ _id: id, includeCity: { $all: [city] } }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForDetails: (root, { slug, city }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ slug: slug, includeCity: { $all: [city] } }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForSlugWeb: (root, { slug }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ slug: slug }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantFavorito: (root, { id }) => {
        return new Promise((resolve, reject) => {
            Favorito_1.default.find({ usuarioId: id }, (error, favourite) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: favourite,
                    });
                }
            });
        });
    },
    getStoreInOffert: (root, { city }) => {
        return new Promise((resolve, rejects) => {
            let condition = { inOffert: true, includeCity: { $all: [city] } };
            restaurant_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    },
    getMenu: (root, { id }) => {
        return new Promise((resolve, reject) => {
            menu_1.default.find({ restaurant: id }, (error, menu) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    const menus = menu;
                    menus.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: menus,
                    });
                }
            });
        });
    },
    getMenumarca: (root, { id }) => {
        return new Promise((resolve, reject) => {
            menu_1.default.findOne({ restaurant: id }, (error, menu) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: menu,
                    });
                }
            });
        });
    },
    getPlato: (root, { id, page, limit }) => {
        return new Promise((resolve, reject) => {
            platos_1.default.find({ menu: id }, (error, plato) => {
                if (!error) {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: plato,
                    });
                }
                else {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
            })
                .skip((page - 1) * limit)
                .limit(limit * 1);
        });
    },
    getAcompanante: (root, { id }) => {
        return new Promise((resolve, reject) => {
            acompanante_1.default.find({ plato: id }, (error, favourite) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: favourite,
                    });
                }
            });
        });
    },
    getItemCart: (root, { id, PlatoID }) => {
        return new Promise((resolve, reject) => {
            card_1.default.find({ userId: id, platoID: PlatoID }, (error, cart) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: cart,
                    });
                }
            });
        });
    },
    getItemCarts: (root, { id }) => {
        return new Promise((resolve, reject) => {
            card_1.default.find({ userId: id }, (error, cart) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: cart,
                    });
                }
            });
        });
    },
    getMyItemCart: (root, { id, restaurant }) => {
        return new Promise((resolve, reject) => {
            card_1.default.find({ userId: id, restaurant: restaurant }, (error, cart) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: cart,
                    });
                }
            });
        });
    },
    getCupon: (root, { clave }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (!usuarioActual) {
                return {
                    success: false,
                    message: "Debe iniciar sesión para continuar",
                    data: null,
                };
            }
            const usuario = yield user_1.default.findOne({
                _id: usuarioActual._id,
            });
            if (!usuario) {
                return {
                    success: false,
                    message: "Debe iniciar sesión para continuar",
                    data: null,
                };
            }
            return new Promise((resolve, reject) => {
                cupones_1.default.findOne({ clave }, (error, cupon) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        return resolve(cupon);
                    }
                });
            });
        }
        catch (error) {
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getCuponAll: (root) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return new Promise((resolve, reject) => {
                cupones_1.default.findOne((error, cupon) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        return resolve(cupon);
                    }
                });
            });
        }
        catch (error) {
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getOrderByUsuario: (root, { usuarios, dateRange }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        const usuario = yield user_1.default.findOne({
            _id: usuarioActual._id,
        });
        if (!usuario) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        return new Promise((resolve, reject) => {
            let condition = {
                userID: usuarios,
                estado: {
                    $in: [
                        "Pagado",
                        "Confirmada",
                        "Listo para recoger",
                        "Preparando para el envío",
                        "Aceptada",
                        "En camino",
                        "Entregada",
                        "Rechazada por el rider",
                    ],
                },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, consulta) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: consulta,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByUsuarioAll: (root, { usuarios, dateRange }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        const usuario = yield user_1.default.findOne({
            _id: usuarioActual._id,
        });
        if (!usuario) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        return new Promise((resolve, reject) => {
            let condition = {
                userID: usuarios,
                estado: { $ne: "Pendiente de pago" },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, consulta) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: consulta,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByAdmin: (root, { city, dateRange, id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        const usuario = yield userAdmin_1.default.findOne({
            _id: usuarioActual._id,
        });
        if (!usuario) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        return new Promise((resolve, reject) => {
            let condition = {
                city: city,
                estado: {
                    $in: [
                        "Pagado",
                        "Confirmada",
                        "Listo para recoger",
                        "Preparando para el envío",
                        "En camino",
                        "Rechazada por el rider",
                    ],
                },
            };
            //@ts-ignore
            if (id)
                condition.display_id = id;
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, consulta) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: consulta,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByAdminAll: (root, { city, dateRange, id, page }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        const usuario = yield userAdmin_1.default.findOne({
            _id: usuarioActual._id,
        });
        if (!usuario) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        return new Promise((resolve, reject) => {
            let condition = {
                city: city,
                estado: {
                    $in: [
                        "Pendiente de pago",
                        "Pagado",
                        "Confirmada",
                        "Listo para recoger",
                        "Preparando para el envío",
                        "En camino",
                        "Entregada",
                        "Rechazado",
                        "Rechazada por la tienda",
                        "Rechazada por el rider",
                        "Devuelto",
                        "Valorada",
                        "Resolución",
                    ],
                },
            };
            //@ts-ignore
            if (id)
                condition.display_id = id;
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, consulta) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: consulta,
                    });
                }
            })
                .limit(10 * 1)
                .skip((page - 1) * 11)
                .sort({ $natural: -1 });
        });
    }),
    getOrderByRiders: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                riders: id,
                estado: {
                    $in: [
                        "Listo para recoger",
                        "Confirmada",
                        "Preparando para el envío",
                        "En camino",
                        "Pagado",
                    ],
                },
            };
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByRidersAll: (root, { id, page }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                riders: id,
                estado: { $ne: "Pendiente de pago" },
            };
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            })
                .limit(11 * 1)
                .skip((page - 1) * 11)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getOrderByRestaurantNew: (root, { id, dateRange }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                restaurant: id,
                estado: {
                    $in: ["Pagado", "Confirmada", "Listo para recoger"],
                },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByRestaurant: (root, { id, dateRange, page, orderID }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                restaurant: id,
                estado: {
                    $in: [
                        "Pagado",
                        "Confirmada",
                        "Listo para recoger",
                        "Preparando para el envío",
                        "En camino",
                        "Entregada",
                        "Rechazado",
                        "Rechazada por la tienda",
                        "Rechazada por el rider",
                        "Devuelto",
                        "Valorada",
                        "Resolución",
                    ],
                },
            };
            // @ts-ignore
            if (orderID)
                condition.display_id = orderID;
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            })
                .limit(20 * 1)
                .skip((page - 1) * 20)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getOrderByRestaurantID: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            order_1.default.findOne({ _id: id }, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            });
        });
    }),
    getValoraciones: (root, { restaurant }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return new Promise((resolve, reject) => {
                rating_1.default.find({ restaurant: restaurant }, (error, valoracion) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        resolve({
                            messages: "Datos Obtenidos con éxito",
                            success: true,
                            data: valoracion,
                        });
                    }
                }).sort({ $natural: -1 });
            });
        }
        catch (error) {
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getNotifications: (root, { Id }) => {
        if (Id) {
            return new Promise((resolve, reject) => {
                Notification_1.default.find({ user: Id, read: false })
                    .populate("users")
                    .populate("restaurants")
                    .populate("orders")
                    .sort({ $natural: -1 })
                    .exec((error, notification) => {
                    if (error) {
                        reject({
                            messages: "Hubo un problema con su solicitud",
                            success: false,
                            notifications: [],
                        });
                    }
                    else {
                        resolve({
                            messages: "success",
                            success: true,
                            notifications: notification,
                        });
                    }
                });
            });
        }
        else {
            return null;
        }
    },
    getNotificationsRiders: (root, { riders }) => {
        let condition = {
            riders: riders,
            read: false,
            type: {
                $nin: [
                    "accept_order",
                    "reject_order_riders",
                    "order_process",
                    "finish_order",
                    "valored_order",
                    "resolution_order",
                ],
            },
        };
        if (riders) {
            return new Promise((resolve, reject) => {
                Notification_1.default.find(condition)
                    .populate("users")
                    .populate("restaurants")
                    .populate("orders")
                    .sort({ $natural: -1 })
                    .exec((error, notification) => {
                    if (error) {
                        reject({
                            messages: "Hubo un problema con su solicitud",
                            success: false,
                            notifications: [],
                        });
                    }
                    else {
                        resolve({
                            messages: "success",
                            success: true,
                            notifications: notification,
                        });
                    }
                });
            });
        }
        else {
            return null;
        }
    },
    getOpinion: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            Opinion_1.default.find({ plato: id }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            }).sort({ $natural: -1 });
        });
    },
    getPago: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            Pago_1.default.findOne({ restaurantID: id }, (error, pagos) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                        data: null,
                    });
                else
                    resolve({
                        messages: "Operacion realizada con éxito",
                        success: true,
                        data: pagos,
                    });
            });
        });
    }),
    getTransaction: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            transacciones_1.default
                .find({ restaurantID: id })
                .sort({ $natural: -1 })
                .exec((error, deposito) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                        list: [],
                    });
                else
                    resolve({
                        messages: "solicitud procesada con éxito",
                        success: true,
                        list: deposito,
                    });
            });
        });
    }),
    getStatistics: (root, { id }) => {
        return new Promise((resolve, reject) => {
            let matchQuery1 = {
                restaurant: id,
                estado: {
                    $in: ["Entregada", "Valorada"],
                },
            };
            let matchQuery2 = {
                restaurant: id,
                estado: "Rechazada",
            };
            order_1.default
                .aggregate([
                { $match: matchQuery1 },
                {
                    $group: {
                        _id: {
                            month: { $substr: ["$created_at", 5, 2] },
                        },
                        paypalAmount: { $first: "$total" },
                        stripeAmount: { $first: "$total" },
                        finishedCount: { $sum: 1 },
                        created_at: { $first: "$created_at" },
                    },
                },
                {
                    $project: {
                        your_year_variable: { $year: "$created_at" },
                        paypalAmount: 1,
                        finishedCount: 1,
                        stripeAmountDivide: { $divide: [Number("$stripeAmount"), 100] },
                    },
                },
                { $match: { your_year_variable: 2021 } },
            ])
                .then((res) => {
                let ordenes = {
                    name: "Pedidos",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                let ganacias = {
                    name: "Total Ventas",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                let devoluciones = {
                    name: "Pedidos rechazados",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                for (let i = 0; i < res.length; i++) {
                    let month = res[i]._id.month;
                    if (month == "01") {
                        ganacias["Ene"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Ene"] = res[i].finishedCount;
                    }
                    else if (month == "02") {
                        ganacias["Feb"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Feb"] = res[i].finishedCount;
                    }
                    else if (month == "03") {
                        ganacias["Mar"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Mar"] = res[i].finishedCount;
                    }
                    else if (month == "04") {
                        ganacias["Abr"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Abr"] = res[i].finishedCount;
                    }
                    else if (month == "05") {
                        ganacias["May"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["May"] = res[i].finishedCount;
                    }
                    else if (month == "06") {
                        ganacias["Jun"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Jun"] = res[i].finishedCount;
                    }
                    else if (month == "07") {
                        ganacias["Jul"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Jul"] = res[i].finishedCount;
                    }
                    else if (month == "08") {
                        ganacias["Aug"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Aug"] = res[i].finishedCount;
                    }
                    else if (month == "09") {
                        ganacias["Sep"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Sep"] = res[i].finishedCount;
                    }
                    else if (month == "10") {
                        ganacias["Oct"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Oct"] = res[i].finishedCount;
                    }
                    else if (month == "11") {
                        ganacias["Nov"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Nov"] = res[i].finishedCount;
                    }
                    else if (month == "12") {
                        ganacias["Dic"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Dic"] = res[i].finishedCount;
                    }
                }
                order_1.default
                    .aggregate([
                    { $match: matchQuery2 },
                    {
                        $group: {
                            _id: {
                                month: { $substr: ["$created_at", 5, 2] },
                                estado: "Rechazada",
                            },
                            returnedCount: { $sum: 1 },
                            created_at: { $first: "$created_at" },
                        },
                    },
                    {
                        $project: {
                            your_year_variable: { $year: "$created_at" },
                            returnedCount: 1,
                        },
                    },
                    { $match: { your_year_variable: 2021 } },
                ])
                    .then((res1) => {
                    for (let i = 0; i < res1.length; i++) {
                        let month = res1[i]._id.month;
                        if (month == "01") {
                            devoluciones["Ene"] = res1[i].returnedCount;
                        }
                        else if (month == "02") {
                            devoluciones["Feb"] = res1[i].returnedCount;
                        }
                        else if (month == "03") {
                            devoluciones["Mar"] = res1[i].returnedCount;
                        }
                        else if (month == "04") {
                            devoluciones["Abr"] = res1[i].returnedCount;
                        }
                        else if (month == "05") {
                            devoluciones["May"] = res1[i].returnedCount;
                        }
                        else if (month == "06") {
                            devoluciones["Jun"] = res1[i].returnedCount;
                        }
                        else if (month == "07") {
                            devoluciones["Jul"] = res1[i].returnedCount;
                        }
                        else if (month == "08") {
                            devoluciones["Aug"] = res1[i].returnedCount;
                        }
                        else if (month == "09") {
                            devoluciones["Sep"] = res1[i].returnedCount;
                        }
                        else if (month == "10") {
                            devoluciones["Oct"] = res1[i].returnedCount;
                        }
                        else if (month == "11") {
                            devoluciones["Nov"] = res1[i].returnedCount;
                        }
                        else if (month == "12") {
                            devoluciones["Dic"] = res1[i].returnedCount;
                        }
                    }
                    resolve({
                        success: true,
                        message: "",
                        data: [ordenes, ganacias, devoluciones],
                    });
                });
            })
                .catch((err) => { });
        });
    },
    getPost: (root, { country }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            post_1.default
                .findOne({ country: country }, (error, post) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        message: "Operacion realizada con éxito",
                        data: post,
                    });
            })
                .sort({ $natural: -1 });
        });
    }),
    getPosts: (root, { country, page }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            post_1.default
                .find({ country: country }, (error, post) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        message: "Operacion realizada con éxito",
                        data: post,
                    });
            })
                .limit(20 * 1)
                .skip((page - 1) * 20)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getPostsForTags: (root, { country, page, tag }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            post_1.default
                .find({ country: country, tags: { $all: [tag] } }, (error, post) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        message: "Operacion realizada con éxito",
                        data: post,
                    });
            })
                .limit(20 * 1)
                .skip((page - 1) * 20)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getPostbyId: (root, { slug, country }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            post_1.default.findOne({ slug: slug, country: country }, (error, post) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        message: "Operacion realizada con éxito",
                        data: post,
                    });
            });
        });
    }),
    getCustomOrder: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                userID: id,
                estado: {
                    $in: [
                        "Pagado",
                        "Asignada",
                        "Aceptada",
                        "En camino",
                        "Entregado",
                        "Rechazada por el rider",
                        "Rechazada",
                        "Valorada",
                    ],
                },
            };
            custonorder_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Operacion realizada con éxito",
                        data: order,
                    });
            })
                .sort({ $natural: -1 });
        });
    }),
    getCustomOrderRider: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                riders: id,
                estado: {
                    $in: [
                        "Pagado",
                        "Asignada",
                        "Aceptada",
                        "En camino",
                        "Entregado",
                        "Valorada",
                    ],
                },
            };
            custonorder_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Operacion realizada con éxito",
                        data: order,
                    });
            })
                .sort({ $natural: -1 });
        });
    }),
    getCustomOrderAdmin: (root, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield userAdmin_1.default.find({ _id: usuarioActual._id });
        if (isAdmin) {
            return new Promise((resolve, reject) => {
                let condition = {
                    estado: { $ne: "Pendiente de pago" },
                };
                custonorder_1.default
                    .find(condition, (error, order) => {
                    if (error)
                        reject({
                            success: false,
                            messages: "Hay un problema con su solicitud",
                            data: null,
                        });
                    else
                        resolve({
                            success: true,
                            messages: "Operacion realizada con éxito",
                            data: order,
                        });
                })
                    .sort({ $natural: -1 });
            });
        }
        else {
            return {
                success: false,
                messages: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getCity: (root, { city }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                city: city,
            };
            cityclose_1.default.findOne(condition, (error, city) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Aun no hemos llegado a esta zona",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Ciudad operativa",
                        data: city,
                    });
            });
        });
    }),
    getOfferts: (root, { city }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                city: city,
            };
            offerts_1.default.find(condition, (error, city) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Aun no tenemos ofertas en esta zona",
                        data: null,
                    });
                else
                    resolve({
                        success: true,
                        messages: "Ofertas obtenidas con éxito",
                        data: city,
                    });
            });
        });
    }),
    getCustomers: (root, { page, limit, email }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        let condition = {};
        // @ts-ignore
        if (email)
            condition.email = email;
        const isAdmin = yield userAdmin_1.default.findById({ _id: usuarioActual._id });
        if (isAdmin) {
            return new Promise((resolve, reject) => {
                user_1.default
                    .find(condition, (error, user) => {
                    if (error)
                        reject({
                            success: false,
                            messages: "Hay un problema con su solicitud",
                            data: null,
                        });
                    else
                        resolve({
                            success: true,
                            messages: "Operacion realizada con éxito",
                            data: user,
                        });
                })
                    .limit(limit * 1)
                    .skip((page - 1) * limit)
                    .sort({ $natural: -1 })
                    .exec();
            });
        }
        else {
            return {
                success: false,
                messages: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getAllCustomers: (root, { email }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        let condition = {};
        // @ts-ignore
        if (email)
            condition.email = email;
        const isAdmin = yield userAdmin_1.default.findById({ _id: usuarioActual._id });
        if (isAdmin) {
            return new Promise((resolve, reject) => {
                user_1.default.find(condition, (error, user) => {
                    if (error)
                        reject({
                            success: false,
                            messages: "Hay un problema con su solicitud",
                            data: null,
                        });
                    else
                        resolve({
                            success: true,
                            messages: "Operacion realizada con éxito",
                            data: user,
                        });
                });
            });
        }
        else {
            return {
                success: false,
                messages: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getRiderTransaction: (root, { id, dateRange, page }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                rider: id,
                created_at: {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                },
            };
            trasactionRider_1.default
                .find(condition, (error, list) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: list,
                    });
                }
            })
                .limit(20 * 1)
                .skip((page - 1) * 20)
                .sort({ $natural: -1 })
                .exec();
        });
    }),
    getQuincena: (root) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            quincena_1.default
                .findOne((error, quincena) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: {},
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        data: quincena,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getQuincenaAll: (root) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            quincena_1.default
                .find((error, quincena) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: quincena,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getCollection: (root, { store }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            collections_1.default.find({ store: store }, (error, collection) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: [],
                    });
                else {
                    const collections = collection;
                    collections.sort((a, b) => a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0);
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        data: collections,
                    });
                }
            });
        });
    }),
    getPlatoCollection: (root, { id }) => {
        return new Promise((resolve, reject) => {
            platos_1.default.find({ menu: id }, (error, plato) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: plato,
                    });
                }
            }).limit(3);
        });
    },
    getNewMenu: (root, { id }) => {
        return new Promise((resolve, reject) => {
            Menu_1.default.find({ storeId: id }, (error, menu) => {
                if (error) {
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: menu,
                    });
                }
            });
        });
    },
    getNewProductoSearch: (root, { store, search, page, limit, category }) => {
        let condition = {};
        // @ts-ignore
        if (category)
            condition.parentId = category;
        // @ts-ignore
        if (search)
            condition = { $text: { $search: `"\"${search} \""` } };
        // @ts-ignore
        if (store)
            condition.storeId = store;
        return new Promise((resolve, rejects) => {
            products_1.default
                .find(condition, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            })
                .limit(limit * 1)
                .skip((page - 1) * limit)
                .exec();
        });
    },
};
