"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginEmail = void 0;
const nodemailer = require("nodemailer");
const LoginEmail_1 = __importDefault(require("./LoginEmail"));
exports.LoginEmail = (email, data, user) => {
    const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        service: "gmail",
        port: 465,
        secure: true,
        auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: process.env.EMAIL_PASSWORD,
        },
    });
    const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email ? email : "info@wilbby.com",
        subject: "Nuevo inicio de sesión en tu cuenta",
        text: "Nuevo inicio de sesión en tu cuenta",
        html: LoginEmail_1.default(data, user),
    };
    transporter.sendMail(mailOptions);
};
