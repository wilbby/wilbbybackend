const nodemailer = require("nodemailer");
import welcomeEmails from "./welcomeEmail";

export const welcomeEmail = (email: string, name: string) => {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    service: "gmail",
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  const mailOptions = {
    from: process.env.EMAIL_ADDRESS,
    to: email ? email : "info@wilbby.com",
    subject: "Bienvenido a Wilbby",
    text: "Con Wilbby lo tienes todo",
    html: welcomeEmails(name),
  };

  transporter.sendMail(mailOptions);
};
