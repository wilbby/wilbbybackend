import { Request, Response, Router } from "express";
import userSchema from "../models/user";

const accountSid = process.env.accountSid;
const authToken = process.env.authToken;
const SERVICESID = process.env.SERVICESID;

const client = require("twilio")(accountSid, authToken, SERVICESID);

class ConfirmNumberRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post("/verify-phone", async (req: Request, res: Response) => {
      const { phone } = req.body;
      const channel = "sms";
      await client.verify
        .services(SERVICESID)
        .verifications.create({ to: `+${phone}`, channel })
        .then((message: any) => {
          res.status(200).json({ success: true, data: message }).end();
        })
        .catch((err: any) => {
          console.log("Err sen message", err);
          res.status(404).json({ success: false, data: err }).end();
        });
    });

    this.router.post("/verify-code", async (req: Request, res: Response) => {
      const { phone, code, id } = req.body;
      client.verify
        .services(SERVICESID)
        .verificationChecks.create({ to: `+${phone}`, code: code })
        .then((verification_check: any) => {
          if (verification_check.status === "approved") {
            userSchema.findOneAndUpdate(
              { _id: id },
              {
                $set: { verifyPhone: true, telefono: phone },
              },
              (err, user) => {
                if (err) {
                  res.status(200).json({ success: false, data: err }).end();
                } else {
                  res
                    .status(200)
                    .json({ success: true, data: verification_check })
                    .end();
                }
              }
            );
          } else {
            res
              .status(200)
              .json({ success: false, data: verification_check })
              .end();
          }
        })
        .catch((err: any) => {
          res.status(200).json({ success: false, data: err }).end();
        });
    });

    //deprecated
    this.router.get("/verify-phone", async (req: Request, res: Response) => {
      const { phone } = req.query;
      const channel = "sms";
      await client.verify
        .services(SERVICESID)
        .verifications.create({ to: `+${phone}`, channel })
        .then((message: any) => {
          res.status(200).send({ success: true, data: message });
        })
        .catch((err: any) => {
          console.log("Err sen message", err);
          res.status(404).send({ success: false, data: err });
        });
    });

    this.router.get("/verify-code", async (req: Request, res: Response) => {
      const { phone, code, id } = req.query;
      client.verify
        .services(SERVICESID)
        .verificationChecks.create({ to: `+${phone}`, code: code })
        .then((verification_check: any) => {
          res
            .status(200)
            .send({ success: true, data: verification_check.status });
          if (verification_check.status === "approved") {
            userSchema.findOneAndUpdate(
              { _id: id },
              {
                $set: { verifyPhone: true, telefono: phone },
              },
              (err, user) => {
                if (err) {
                  console.log(err);
                } else {
                  console.log(user);
                }
              }
            );
          } else {
            res
              .status(400)
              .send({ success: false, data: verification_check.status });
          }
        })
        .catch((err: any) => {
          console.log(err);
          res.status(404).send({ success: false, data: err });
        });
    });

    this.router.get("/send-sms", (req, res) => {
      const { recipient, textmessage } = req.query;
      client.messages
        .create({
          body: textmessage,
          from: "WILBBY",
          to: `+${recipient}`,
        })
        .then((message: any) => console.log("done >>>>>>>", message.sid))
        .catch((err: any) => console.log("Err sen message", err));
    });

    this.router.post("/send-sms-to-client", (req, res) => {
      const { telefono, mensageMovil } = req.body;
      client.messages
        .create({
          body: mensageMovil,
          from: "WILBBY",
          to: `+${telefono}`,
        })
        .then((message: any) =>
          res.json({ data: "Mensaje enviado con éxito", success: true })
        )
        .catch((err: any) =>
          res.json({ data: "Algo va mal intentalo de nuevo", success: false })
        );
    });
  }
}

const confirmNumberRouter = new ConfirmNumberRouter();
confirmNumberRouter.routes();

export default confirmNumberRouter.router;
