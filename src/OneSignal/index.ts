import { Request, Response, Router } from "express";
import userSchema from "../models/user";
import ridersSchema from "../models/riders";
import restaurantSchema from "../models/restaurant";

class OnesignalRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get(
      "/save-userid-notification",
      (req: Request, res: Response) => {
        const { OnesignalID, id } = req.query;
        if (OnesignalID && id) {
          userSchema
            .findOneAndUpdate(
              { _id: id },
              { $set: { OnesignalID: OnesignalID } },
              (err: any, user: any) => {}
            )
            .catch((err: any) => {
              console.log(err);
            });
        }
      }
    );

    this.router.get(
      "/save-riders-notification",
      (req: Request, res: Response) => {
        const { OnesignalID, id } = req.query;
        if (OnesignalID && id) {
          ridersSchema
            .findOneAndUpdate(
              { _id: id },
              { $set: { OnesignalID: OnesignalID } },
              (err: any, user: any) => {}
            )
            .catch((err: any) => {
              console.log(err);
            });
        }
      }
    );

    this.router.get(
      "/save-restaurant-notification",
      async (req: any, res: any, nex) => {
        const { OnesignalID, id } = req.query;
        if (OnesignalID && id) {
          restaurantSchema
            .findOneAndUpdate(
              { _id: id },
              { $set: { OnesignalID: OnesignalID } },
              (err: any, user: any) => {}
            )
            .then(() => {
              console.log({ data: "id save" });
            })
            .catch(() => console.log({ data: "Error save id" }));
        }
        nex();
      }
    );

    this.router.get(
      "/send-push-notification",
      (req: Request, res: Response) => {
        const { IdOnesignal, textmessage } = req.query;
        var sendNotification = function (data: any) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
          };
          var https = require("https");
          var req = https.request(options, function (res: any) {
            res.on("data", function (data: any) {
              console.log("Response:");
              console.log(JSON.parse(data));
            });
          });

          req.on("error", function (e: any) {
            console.log("ERROR:");
            console.log(e);
          });

          req.write(JSON.stringify(data));
          req.end();
        };

        var message = {
          app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
          headings: { en: "Wilbby" },
          android_channel_id: "cfc50b79-a76a-4114-8200-0b53d6d7bd30",
          contents: { en: `${textmessage}` },
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [`${IdOnesignal}`],
        };
        sendNotification(message);
      }
    );

    this.router.get(
      "/send-push-notification-riders",
      (req: Request, res: Response) => {
        const { IdOnesignal, textmessage } = req.query;
        var sendNotification = function (data: any) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
          };
          var https = require("https");
          var req = https.request(options, function (res: any) {
            res.on("data", function (data: any) {
              console.log("Response:");
              console.log(JSON.parse(data));
            });
          });

          req.on("error", function (e: any) {
            console.log("ERROR:");
            console.log(e);
          });

          req.write(JSON.stringify(data));
          req.end();
        };

        var message = {
          app_id: "e21467de-eaf6-4090-9b6c-a311d37c0b5e",
          headings: { en: "Wilbby" },
          android_channel_id: "9b0682ea-6f2d-4228-8802-280df76cb033",
          ios_sound: "despertador_minion.wav",
          contents: { en: `${textmessage}` },
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [`${IdOnesignal}`],
        };
        sendNotification(message);
      }
    );

    this.router.post(
      "/send-push-notification-restaurant",
      (req: Request, res: Response) => {
        const { IdOnesignal, textmessage } = req.body;
        var sendNotification = function (data: any) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
          };
          var https = require("https");
          var req = https.request(options, function (res: any) {
            res.on("data", function (data: any) {
              console.log("Response:");
              console.log(JSON.parse(data));
            });
          });

          req.on("error", function (e: any) {
            console.log("ERROR:");
            console.log(e);
          });
          req.write(JSON.stringify(data));
          req.end();
        };

        var message = {
          app_id: "9a1893e6-8423-43cc-807a-d536a833b8e8",
          headings: { en: "Nuevo pedido Wilbby" },
          android_channel_id: "9009455c-8429-41f2-bb18-bd63a08a9d63",
          ios_sound: "despertador_minion.wav",
          contents: { en: `${textmessage}` },
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [IdOnesignal],
        };
        sendNotification(message);

        res.send({
          data: "Restaurante notificado de tu pedido",
          success: true,
        });
      }
    );

    this.router.post("/send-notification", (req: Request, res: Response) => {
      const { onesignalID, title, messages } = req.body;
      var sendNotification = function (data: any) {
        var headers = {
          "Content-Type": "application/json; charset=utf-8",
        };
        var options = {
          host: "onesignal.com",
          port: 443,
          path: "/api/v1/notifications",
          method: "POST",
          headers: headers,
        };
        var https = require("https");
        var req = https.request(options, function (res: any) {
          res.on("data", function (data: any) {
            console.log(JSON.parse(data));
          });
        });

        req.on("error", function (e: any) {
          console.log(e);
        });

        req.write(JSON.stringify(data));
        req.end();
      };

      var message = {
        app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
        headings: { en: `${title}` },
        contents: { en: `${messages}` },
        android_channel_id: "cfc50b79-a76a-4114-8200-0b53d6d7bd30",
        ios_badgeCount: 1,
        ios_badgeType: "Increase",
        include_player_ids: [`${onesignalID}`],
      };
      sendNotification(message);
      res.json({ data: "Mensaje enviado con éxito", success: true });
    });
  }
}

const onesignalRouter = new OnesignalRouter();
onesignalRouter.routes();

export default onesignalRouter.router;
