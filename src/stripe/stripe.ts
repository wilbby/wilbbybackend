import { Request, Response, Router } from "express";
import dotenv from "dotenv";
import userSchema from "../models/user";
import { chargeToken } from "./changeToken";
import orderSchema from "../models/order";
import cardSchema from "../models/card";
import cartSchema from "../models/cartAdd";
import customorderSchema from "../models/custonorder";
import {
  client_id,
  client_secret,
  audience,
  grant_type,
  oauthURL,
} from "../webHook/config";
import { setOrderToDeliverect } from "../webHook/Deliverect";
import { setOrderToHolded } from "../Holded/Apis";
import { sendNotification } from "./sendNotificationToStore";
var request = require("request");
var stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
dotenv.config({ path: "variables.env" });

var options = {
  method: "POST",
  url: oauthURL,
  headers: {
    "content-type": "application/json",
  },
  body: JSON.stringify({
    client_id: client_id,
    client_secret: client_secret,
    audience: audience,
    grant_type: grant_type,
  }),
};

class StripeRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/create-client", async (req: Request, res: Response) => {
      const { userID, nameclient, email } = req.query;
      await stripe.customers.create(
        {
          name: nameclient,
          email: email,
          description: "Clientes de Wilbby",
        },
        function (err: any, customer: any) {
          userSchema.findOneAndUpdate(
            { _id: userID },
            {
              $set: {
                StripeID: customer.id,
              },
            },
            (err, customers) => {
              if (err) {
                console.log(err);
              }
            }
          );
        }
      );
    });

    this.router.get("/card-create", async (req: Request, res: Response) => {
      const { customers, token } = req.query;
      const paymentMethod = await stripe.paymentMethods.attach(token, {
        customer: customers,
      });
      if (paymentMethod) {
        res.json({ success: true, data: paymentMethod });
      } else {
        res.json({ success: false, data: "Algo salio mas intentalo de nuevo" });
      }
    });

    this.router.get("/get-card", async (req: Request, res: Response) => {
      const { customers } = req.query;
      const paymentMethods = await stripe.paymentMethods.list({
        customer: customers,
        type: "card",
      });
      res.json(paymentMethods);
    });

    this.router.get("/delete-card", async (req: Request, res: Response) => {
      const { cardID, customers } = req.query;
      await stripe.customers.deleteSource(
        customers,
        cardID,
        function (err: any, confirmation: any) {
          res.json(confirmation);
        }
      );
    });

    this.router.get(
      "/payment-existing-card",
      async (req: Request, res: Response) => {
        const {
          card,
          customers,
          amount,
          order,
          cart,
          userID,
          currency,
        } = req.query;

        const total = Number(amount).toFixed(0);

        try {
          await stripe.paymentIntents.create(
            {
              amount: total,
              currency: currency ? currency : "EUR",
              customer: customers,
              payment_method: card,
              setup_future_usage: "off_session",
              confirm: true,
            },
            function (err: any, payment: any) {
              if (err) {
                res.json(err);
              } else {
                res.json(payment);
                if (payment.status === "succeeded") {
                  orderSchema.findOneAndUpdate(
                    { _id: order },
                    {
                      $set: {
                        estado: "Pagado",
                        progreso: "25",
                        status: "active",
                        stripePaymentIntent: payment,
                      },
                    },
                    (err, order) => {
                      if (!err) {
                        if (order?.StoreData.channelLinkId) {
                          request(options, function (error, response) {
                            if (error) throw new Error(error);
                            var resp = JSON.parse(response.body);
                            setOrderToDeliverect(
                              order,
                              resp.access_token,
                              resp.token_type
                            );
                          });
                        } else {
                          sendNotification(
                            order?.StoreData.OnesignalID,
                            "Tienes un nuevo pedido a por todas"
                          );
                        }
                        setOrderToHolded(order);
                      }
                      cardSchema
                        .deleteMany({ restaurant: String(cart) })
                        .then(function () {
                          console.log("Data deleted card"); // Success
                        })
                        .catch(function (error) {
                          console.log(error); // Failure
                        });
                      cartSchema
                        .deleteMany({ usuarioId: String(userID) })
                        .then(function () {
                          console.log("Data deleted cart"); // Success
                        })
                        .catch(function (error) {
                          console.log(error); // Failure
                        });
                    }
                  );
                }
              }
            }
          );
        } catch (err) {
          console.log("Error code is: ", err);
          const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(
            err.raw.payment_intent.id
          );
          console.log("PI retrieved: ", paymentIntentRetrieved.id);
        }
      }
    );

    this.router.post(
      "/payment-auth-new",
      async (req: Request, res: Response) => {
        const { card, customers, amount, currency, order } = req.body;
        const total = amount.toFixed(0);
        try {
          const paymentIntent = await stripe.paymentIntents.create({
            payment_method: card,
            amount: total,
            currency: currency ? currency : "EUR",
            customer: customers,
            confirmation_method: "automatic",
            confirm: true,
          });
          if (paymentIntent.status === "succeeded") {
            orderSchema.findOneAndUpdate(
              { _id: order },
              {
                $set: {
                  stripePaymentIntent: paymentIntent,
                },
              },
              (err, order) => {
                if (!err) {
                  if (order?.StoreData.channelLinkId) {
                    request(options, function (error, response) {
                      if (error) throw new Error(error);
                      var resp = JSON.parse(response.body);
                      setOrderToDeliverect(
                        order,
                        resp.access_token,
                        resp.token_type
                      );
                    });
                  } else {
                    sendNotification(
                      order?.StoreData.OnesignalID,
                      "Tienes un nuevo pedido a por todas"
                    );
                  }
                  setOrderToHolded(order);
                }
              }
            );
            res.status(200).json(paymentIntent).end();
          } else if (paymentIntent.status === "canceled") {
            res.status(200).json(paymentIntent).end();
          } else if (paymentIntent.status === "requires_action") {
            orderSchema.findOneAndUpdate(
              { _id: order },
              {
                $set: {
                  stripePaymentIntent: paymentIntent,
                },
              },
              (err, order) => {
                if (!err) {
                  res.status(200).json(paymentIntent).end();
                }
              }
            );
          } else {
            res.status(200).json(paymentIntent).end();
          }
        } catch (err) {
          console.log("Error code is: ", err);
          const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(
            err.raw.payment_intent.id
          );
          console.log("PI retrieved: ", paymentIntentRetrieved.id);
          res.status(400).json(paymentIntentRetrieved).end();
        }
      }
    );

    this.router.post("/clean-cart", async (req: Request, res: Response) => {
      const { order, cart, userID } = req.body;
      orderSchema.findOneAndUpdate(
        { _id: order },
        {
          $set: {
            estado: "Pagado",
            progreso: "25",
            status: "active",
          },
        },
        (err, order) => {
          if (!err) {
            if (order?.StoreData.channelLinkId) {
              request(options, function (error, response) {
                if (error) throw new Error(error);
                var resp = JSON.parse(response.body);
                setOrderToDeliverect(order, resp.access_token, resp.token_type);
              });
            } else {
              sendNotification(
                order?.StoreData.OnesignalID,
                "Tienes un nuevo pedido a por todas"
              );
            }
            setOrderToHolded(order);
          }
          cardSchema
            .deleteMany({ restaurant: String(cart) })
            .then(function () {
              console.log("Data deleted card"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
          cartSchema
            .deleteMany({ usuarioId: String(userID) })
            .then(function () {
              console.log("Data deleted cart"); // Success
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        }
      );
      res.status(200).json({ success: true }).end();
    });

    this.router.post(
      "/payment-existing-card-custom-order",
      async (req: Request, res: Response) => {
        const { card, customers, amount, order, currency } = req.body;

        const total = Number(amount).toFixed(0);

        try {
          const paymentIntent = await stripe.paymentIntents.create({
            payment_method: card,
            amount: total,
            currency: currency ? currency : "EUR",
            customer: customers,
            confirmation_method: "automatic",
            confirm: true,
          });

          if (paymentIntent.status === "succeeded") {
            res.status(200).json(paymentIntent).end();
          } else if (paymentIntent.status === "canceled") {
            res.status(200).json(paymentIntent).end();
          } else if (paymentIntent.status === "requires_action") {
            customorderSchema.findOneAndUpdate(
              { _id: order },
              {
                $set: {
                  estado: "Pagado",
                  progreso: "25",
                  status: "active",
                  stripePaymentIntent: paymentIntent,
                },
              },
              (err: any, order) => {
                if (err) {
                  res.status(400).json(err).end();
                } else {
                  res.status(200).json(paymentIntent).end();
                }
              }
            );
          } else {
            res.status(400).json(paymentIntent).end();
          }
        } catch (err) {
          console.log("Error code is: ", err);
          const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(
            err.raw.payment_intent.id
          );
          console.log("PI retrieved: ", paymentIntentRetrieved.id);
          res.status(400).json(paymentIntentRetrieved).end();
        }
      }
    );

    this.router.post("/stripe/chargeToken", (req: Request, res: Response) => {
      const { stripeToken, amount, orders, currency } = req.body;
      if (stripeToken && amount != undefined) {
        chargeToken(amount, stripeToken, currency)
          .then(async (response: any) => {
            if (response.status == "succeeded") {
              customorderSchema.findOneAndUpdate(
                { _id: orders },
                {
                  $set: {
                    estado: "Pagado",
                    progreso: "25",
                    status: "active",
                    stripePaymentIntent: response,
                  },
                },
                (err, order) => {
                  res.json(response);
                }
              );
            } else {
              res.send(response);
            }
          })
          .catch((err: any) => {
            res.status(401).send({ success: false, error: err });
          });
      } else {
        res.status(403).send({ success: false, error: "Invalid token" });
      }
    });

    this.router.post(
      "/stripe/chargeToken/order",
      (req: Request, res: Response) => {
        const {
          stripeToken,
          amount,
          orders,
          cart,
          userID,
          currency,
        } = req.body;
        if (stripeToken && amount != undefined) {
          chargeToken(amount, stripeToken, currency)
            .then(async (response: any) => {
              if (response.status == "succeeded") {
                orderSchema.findOneAndUpdate(
                  { _id: orders },
                  {
                    $set: {
                      estado: "Pagado",
                      progreso: "25",
                      status: "active",
                      stripePaymentIntent: response,
                    },
                  },
                  (err, order) => {
                    if (!err) {
                      if (order?.StoreData.channelLinkId) {
                        request(options, function (error, response) {
                          if (error) throw new Error(error);
                          var resp = JSON.parse(response.body);
                          setOrderToDeliverect(
                            order,
                            resp.access_token,
                            resp.token_type
                          );
                        });
                      } else {
                        sendNotification(
                          order?.StoreData.OnesignalID,
                          "Tienes un nuevo pedido a por todas"
                        );
                      }
                      setOrderToHolded(order);
                    }
                    cardSchema
                      .deleteMany({ restaurant: String(cart) })
                      .then(function () {
                        console.log("Data deleted card"); // Success
                      })
                      .catch(function (error) {
                        console.log(error); // Failure
                      });
                    cartSchema
                      .deleteMany({ usuarioId: String(userID) })
                      .then(function () {
                        console.log("Data deleted cart"); // Success
                      })
                      .catch(function (error) {
                        console.log(error); // Failure
                      });
                    res.send(response);
                  }
                );
              } else {
                res.send(response);
              }
            })
            .catch((err: any) => {
              console.log(err);
              res.status(401).send({ success: false, error: err });
            });
        } else {
          res.status(403).send({ success: false, error: "Invalid token" });
        }
      }
    );

    this.router.post("/create-card", async (req: Request, res: Response) => {
      const { customer, paymentMethod } = req.body;
      const paymentMethods = await stripe.paymentMethods.attach(paymentMethod, {
        customer: customer,
      });
      if (paymentMethods) {
        res.json({ success: true, data: paymentMethods });
      } else {
        res.json({ success: false, data: "Algo salio mas intentalo de nuevo" });
      }
    });

    this.router.get("/delete-card-web", async (req: Request, res: Response) => {
      const { cardID } = req.query;
      const done = await stripe.paymentMethods.detach(cardID);
      if (done) {
        res.send(done);
      } else {
        console.log("hay un error");
      }
    });
  }
}

const stripeRouter = new StripeRouter();
stripeRouter.routes();

export default stripeRouter.router;
