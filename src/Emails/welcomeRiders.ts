import emailRiders from "./emailRider";
import riderSchema from "../models/riders";
import restaurantSchema from "../models/restaurant";
import crypto from "crypto";
import nodemailer from "nodemailer";

export const welcomeRiderEmail = (email: any) => {
  const token = crypto.randomBytes(20).toString("hex");
  var newvalues = { $set: { forgotPasswordToken: token } };

  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    service: "gmail",
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  const mailOptions = {
    from: process.env.EMAIL_ADDRESS,
    to: email,
    subject: `ACTIVAR CUENTA WILBBY - Bienvenido a Wilbby para Riders`,
    text: `ACTIVAR CUENTA WILBBY - Bienvenido a Wilbby para Riders`,
    html: emailRiders(token, email),
  };

  transporter.sendMail(mailOptions, (err: any) => {
    if (err) {
      console.log("Error al enviar");
    } else {
      riderSchema.findOneAndUpdate(
        { email: email },
        newvalues,
        {
          //options
          new: true,
          strict: false,
          useFindAndModify: false,
        },
        (err, updated) => {}
      );
    }
  });
};
