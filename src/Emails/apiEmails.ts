import { Request, Response, Router } from "express";
import { welcomeRiderEmail } from "./welcomeRiders";

class EmailRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get(
      "/send-email-welcome-riders",
      (req: Request, res: Response) => {
        const { email } = req.query;
        welcomeRiderEmail(email);

        res
          .status(200)
          .json({ message: "Email enviado con exito", success: true })
          .end();
      }
    );
  }
}

const emailRouter = new EmailRouter();
emailRouter.routes();

export default emailRouter.router;
