import { Request, Response, Router } from "express";
import orderSchema from "../models/order";
import { key } from "../Holded/config";
import request from "request";
import { setOrderToHolded } from "../Holded/Apis";

class IndexRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", (req: Request, res: Response) =>
      res.send("Wilcome to Wilbby App®")
    );

    this.router.get("/update-orden", (req: Request, res: Response) => {
      const { id } = req.query;
      orderSchema.findOneAndUpdate(
        { _id: id },
        {
          $set: {
            isvalored: true,
          },
        },
        (err, order) => {
          // @ts-ignore
          console.log("done update");
        }
      );
    });

    this.router.get(
      "/delete-holded-recipt",
      async (req: Request, res: Response) => {
        const { id, idriders, partnerid } = req.query;
        const url = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${id}`;
        const optiones = {
          method: "DELETE",
          url: url,
          headers: { Accept: "application/json", key: key },
        };
        request(optiones, function (error, responses) {
          if (error) throw new Error(error);
        });

        const urlriedr = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${idriders}`;
        const optione = {
          method: "DELETE",
          url: urlriedr,
          headers: { Accept: "application/json", key: key },
        };
        request(optione, function (error, responses) {
          if (error) throw new Error(error);
        });

        const urlpartner = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${partnerid}`;
        const optionespartner = {
          method: "DELETE",
          url: urlpartner,
          headers: { Accept: "application/json", key: key },
        };
        request(optionespartner, function (error, responses) {
          if (error) throw new Error(error);
        });

        res.json({ data: "Recibo eliminado de Holded", success: true }).end();
      }
    );

    this.router.get("/test-holded", async (req: Request, res: Response) => {
      const { id } = req.query;
      const dat = await orderSchema.findOne({
        _id: id,
      });
      setOrderToHolded(dat);
      res.status(200).end();
    });

    this.router.get(
      "/send-test-notfication-store",
      async (req: Request, res: Response) => {
        const { id } = req.query;
        var sendNotification = function (data: any) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
          };
          var https = require("https");
          var req = https.request(options, function (res: any) {
            res.on("data", function (data: any) {
              console.log("Response:");
              console.log(JSON.parse(data));
            });
          });

          req.on("error", function (e: any) {
            console.log("ERROR:");
            console.log(e);
          });
          req.write(JSON.stringify(data));
          req.end();
        };

        var message = {
          app_id: "9a1893e6-8423-43cc-807a-d536a833b8e8",
          headings: { en: "Nuevo pedido Wilbby" },
          android_channel_id: "9009455c-8429-41f2-bb18-bd63a08a9d63",
          ios_sound: "despertador_minion.wav",
          contents: {
            en: `Estos es una notificacion de prueba para la app de restaurantes`,
          },
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [id],
        };
        sendNotification(message);

        res
          .send({
            data: "Restaurante notificado de tu pedido",
            success: true,
          })
          .end();
      }
    );

    this.router.get(
      "/send-test-notfication-rider",
      async (req: Request, res: Response) => {
        const { id } = req.query;
        var sendNotification = function (data: any) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
          };
          var https = require("https");
          var req = https.request(options, function (res: any) {
            res.on("data", function (data: any) {
              console.log("Response:");
              console.log(JSON.parse(data));
            });
          });

          req.on("error", function (e: any) {
            console.log("ERROR:");
            console.log(e);
          });

          req.write(JSON.stringify(data));
          req.end();
        };

        var message = {
          app_id: "e21467de-eaf6-4090-9b6c-a311d37c0b5e",
          headings: { en: "Wilbby" },
          android_channel_id: "9b0682ea-6f2d-4228-8802-280df76cb033",
          ios_sound: "despertador_minion.wav",
          contents: { en: `Hola tienes un nuevo pedido de Wilbby` },
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [id],
        };
        sendNotification(message);

        res
          .send({
            data: "Rider notificado de tu pedido",
            success: true,
          })
          .end();
      }
    );
  }
}

const indexRouter = new IndexRouter();
indexRouter.routes();

export default indexRouter.router;
