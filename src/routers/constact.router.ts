import { Request, Response, Router } from "express";
import ContactEmail from "../RecoveryPassword/Email/ContactEmail";
import ContactEmailRider from "../RecoveryPassword/Email/ContactRider";
const nodemailer = require("nodemailer");

class ContactRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post(
      "/contact-for-restaurant",
      (req: Request, res: Response) => {
        const { values } = req.body;
        const transporter = nodemailer.createTransport({
          host: "smtp.gmail.com",
          service: "gmail",
          port: 465,
          secure: true,
          auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: process.env.EMAIL_PASSWORD,
          },
        });
        const mailOptions = {
          from: process.env.EMAIL_ADDRESS,
          to: `info@wilbby.com`,
          subject: "Solicitud de para unirse a Wilbby",
          text: "Contacto nuevo establecimiento",
          html: ContactEmail(values),
        };
        transporter.sendMail(mailOptions, (err: any) => {
          if (err) {
            res.json({
              success: false,
              messages: "Algo va mal intentalo de nuevo",
            });
          } else {
            res.json({
              success: true,
              messages:
                "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
            });
          }
        });
      }
    );

    this.router.post("/contact-for-rider", (req: Request, res: Response) => {
      const { values } = req.body;
      const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        service: "gmail",
        port: 465,
        secure: true,
        auth: {
          user: process.env.EMAIL_ADDRESS,
          pass: process.env.EMAIL_PASSWORD,
        },
      });
      const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: `info@wilbby.com`,
        subject: "Solicitud Riders",
        text: "Solicitud para repartir con Wilbby",
        html: ContactEmailRider(values),
      };
      transporter.sendMail(mailOptions, (err: any) => {
        if (err) {
          res.json({
            success: false,
            messages: "Algo va mal intentalo de nuevo",
          });
        } else {
          res.json({
            success: true,
            messages:
              "Solicitud enviada con éxito en 24 o 48 horas te contactaremos",
          });
        }
      });
    });
  }
}

const constctRouter = new ContactRouter();
constctRouter.routes();

export default constctRouter.router;
