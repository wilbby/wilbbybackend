import loginSchema from "../models/DataLoginUser";

export const saveDataLogin = (user: string, data: any) => {
  const nevodataLogin = new loginSchema({
    user: user,
    data: data,
  });
  nevodataLogin.save();
};
