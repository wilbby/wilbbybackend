import orderSchema from "../models/order";
var distance = require("google-distance-matrix");

distance.key("AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA");
distance.units("imperial");

export const setDistanceToOrder = async (OrderID: String) => {
  const order = await orderSchema.findOne({ _id: OrderID });
  if (!order?.llevar) {
    var origins = [`${order?.AdreesData.lat},${order?.AdreesData.lgn}`];
    var destinations = [
      `${order?.StoreData.adress.lat},${order?.StoreData.adress.lgn}`,
    ];

    distance.matrix(origins, destinations, function (err: any, distances: any) {
      if (err) {
        return console.log(err);
      }
      if (!distances) {
        return console.log("no distances");
      }
      if (distances.status === "OK") {
        for (var i = 0; i < origins.length; i++) {
          for (var j = 0; j < destinations.length; j++) {
            var origin = distances.origin_addresses[i];
            var destination = distances.destination_addresses[j];
            if (distances.rows[0].elements[j].status == "OK") {
              var distance = distances.rows[i].elements[j].distance.value;
              const km = distance / 1000;
              const kmPrice = km * 0.4;
              const base = 2.8;
              const time = 0;
              const extras = 0;
              const lluvia = 0;
              const combinado = 0;
              const propina = Number(order?.propinaTotal);
              const total = kmPrice + base + time + extras + lluvia + combinado;
              const iva = (21 / 100) * total;
              const totalFinal = total - iva;

              const ivaformat = iva.toFixed(2);
              const totalformat = totalFinal.toFixed(2);

              const input = {
                km: String(km.toFixed(2)),
                base: String(base),
                time: String(time),
                order: String(order?.display_id),
                extra: String(extras),
                combinado: String(combinado),
                lluvia: String(lluvia),
                total: String(totalformat),
                propina: String(propina),
                iva: String(ivaformat),
              };

              orderSchema.findOneAndUpdate(
                { _id: OrderID },
                {
                  $set: {
                    reportRiders: input,
                  },
                },
                (err, order) => {}
              );
            } else {
              console.log(
                destination + " is not reachable by land from " + origin
              );
            }
          }
        }
      }
    });
  } else {
    null;
  }
};
