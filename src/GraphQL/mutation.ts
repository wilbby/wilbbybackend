import categoriaSchema from "../models/categorias";
import userSchema from "../models/user";
import restaurantSchema from "../models/restaurant";
import PlatoSchema from "../models/nemu/platos";
import MenuSchema from "../models/nemu/menu";
import pagoSchema from "../models/Pago";
import transSchema from "../models/transacciones";
import adressSchema from "../models/adress";
import AcompananteSchema from "../models/nemu/acompanante";
import userAdminSchema from "../models/userAdmin";
import cardSchema from "../models/card";
import Cupones from "../models/cupones";
import favoritoSchema from "../models/Favorito";
import cartSchema from "../models/cartAdd";
import ridersSchema from "../models/riders";
import { STATUS_MESSAGES } from "./Status_messages";
import fs from "fs";
import { Types } from "mongoose";
import orderSchema from "../models/order";
import RatingSchema from "../models/rating";
import NotificationSchema from "../models/Notification";
import OpinionSchema from "../models/Opinion";
import tipoSchema from "../models/tipo";
import highkitchenCategorySchema from "../models/highkitchenCategory";
import adressStoreSchema from "../models/adressStore";
import postSchema from "../models/post";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { SaveEmail } from "../mailJet";
import { SaveEmailWilbby } from "../SaveEmailList/saveEmail";
import tipotiendaSchema from "../models/tipotienda";
import customOrderSchema from "../models/custonorder";
import quincenaSchama from "../models/quincena";
import offertsSchema from "../models/offerts";
import collectionSchema from "../models/collections";
import subCollectionSchema from "../models/subcollection";
import { welcomeEmail } from "../RecoveryPassword/Email/SendEmail";
import citySchema from "../models/cityclose";
import transactionSchema from "../models/trasactionRider";
import { saveDataLogin } from "../funtions/SavedataLogin";
import { setDistanceToOrder } from "../getDistance/distance";
import dotenv from "dotenv";
import { LoginEmail } from "../RecoveryPassword/Email/LoginEmailSend";
import { sendNotification } from "./sendNotificationRider";
import { RegiterCompraRider } from "../Holded/CompraRiders";
import newCartSchema from "../models/newOrder/addToCart";
dotenv.config({ path: "variables.env" });
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

const { ObjectId } = Types;

export interface IRestaurant extends Document {
  email: string;
  password: string;
}

const crearToken = (restaurant: any, secreto: any, expiresIn: any) => {
  const { _id } = restaurant;

  return jwt.sign({ _id }, secreto, { expiresIn });
};

export const Mutation = {
  autenticarRestaurant: async (root: any, { email, password }) => {
    const restaurant = await restaurantSchema.findOne({ email });
    if (!restaurant) {
      return {
        success: false,
        message: STATUS_MESSAGES.USER_NOT_FOUND,
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(
      password,
      restaurant.password
    );
    if (!passwordCorrecto) {
      return {
        success: false,
        message: STATUS_MESSAGES.INCORRECT_PASSWORD,
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenido a My Store by Wilbby",
      data: {
        token: crearToken(restaurant, process.env.SECRETO, "20000hr"),
        id: restaurant._id,
      },
    };
  },

  autenticarUsuario: async (root: any, { email, password, input }) => {
    const users = await userSchema.findOne({ email });
    if (!users) {
      return {
        success: false,
        message: "Aún no de te has registrado en Wilbby",
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, users.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: "Contraseña incorrecta",
        data: null,
      };
    } else {
      LoginEmail(email, input, users);
      saveDataLogin(users._id, input);
      return {
        success: true,
        message: "Bienvenido a Wilbby",
        data: {
          token: crearToken(users, process.env.SECRETO, "20000hr"),
          id: users._id,
        },
      };
    }
  },

  autenticarRiders: async (root: any, { email, password }) => {
    const users = await ridersSchema.findOne({ email });
    if (!users) {
      return {
        success: false,
        message: STATUS_MESSAGES.USER_NOT_FOUND,
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, users.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: STATUS_MESSAGES.INCORRECT_PASSWORD,
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenid@ a Wilbby®",
      data: {
        token: crearToken(users, process.env.SECRETO, "5000hr"),
        id: users._id,
      },
    };
  },

  crearUsuario: async (root: any, { input }) => {
    SaveEmail(input.email, input.nombre);
    SaveEmailWilbby(input.email, input.nombre, input.apellidos);
    welcomeEmail(input.email, input.nombre);
    // check if email exists
    const emailExists = await userSchema.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "Ya eres parte de Wilbby",
        data: null,
      };
    }

    const nuevoUsuario = new userSchema({
      name: input.nombre,
      lastName: input.apellidos,
      email: input.email,
      password: input.password,
      city: input.city,
      termAndConditions: input.termAndConditions,
    });

    nuevoUsuario.id = nuevoUsuario._id;

    return new Promise((resolve, object) => {
      nuevoUsuario.save(async (error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Usuario agregado con éxito",
            data: nuevoUsuario,
          });

          /*  CratePaypalUser(
            input.nombre,
            input.apellidos,
            input.emai,
            nuevoUsuario._id
          ); */

          await stripe.customers.create(
            {
              name: input.nombre,
              email: input.email,
              description: "Clientes de Wilbby",
            },
            function (err: any, customer: any) {
              userSchema.findOneAndUpdate(
                { _id: nuevoUsuario._id },
                {
                  $set: {
                    StripeID: customer.id,
                  },
                },
                (err, customers) => {
                  if (err) {
                  }
                }
              );
            }
          );
        }
      });
    });
  },

  crearRiders: async (root: any, { input }) => {
    // check if email exists
    const emailExists = await ridersSchema.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "Ya eres parte de Wilbby",
        data: null,
      };
    }

    const nuevoUsuario = new ridersSchema({
      name: input.nombre,
      lastName: input.apellidos,
      email: input.email,
      password: input.password,
      termAndConditions: input.termAndConditions,
      telefono: input.telefono,
      isAvalible: input.isAvalible,
      city: input.city,
      contactCode: input.contactCode,
    });

    nuevoUsuario.id = nuevoUsuario._id;

    return new Promise((resolve, object) => {
      nuevoUsuario.save((error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Usuario agregado con éxito",
            data: nuevoUsuario,
          });
        }
      });
    });
  },

  crearAdmin: async (root: any, { input }) => {
    // check if email exists
    const emailExists = await userAdminSchema.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "Ya eres parte de Wilbby",
        data: null,
      };
    }

    const nuevoUsuario = new userAdminSchema({
      name: input.nombre,
      lastName: input.apellidos,
      email: input.email,
      password: input.password,
      termAndConditions: input.termAndConditions,
    });

    nuevoUsuario.id = nuevoUsuario._id;

    return new Promise((resolve, object) => {
      nuevoUsuario.save((error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Usuario agregado con éxito",
            data: nuevoUsuario,
          });
        }
      });
    });
  },

  autenticarAdmin: async (root: any, { email, password }) => {
    const users = await userAdminSchema.findOne({ email });
    if (!users) {
      return {
        success: false,
        message: STATUS_MESSAGES.USER_NOT_FOUND,
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, users.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: STATUS_MESSAGES.INCORRECT_PASSWORD,
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenid@ a Wilbby®",
      data: {
        token: crearToken(users, process.env.SECRETO, "5000hr"),
        id: users._id,
      },
    };
  },

  singleUpload(parent: any, { file }) {
    const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
    const ext = matches[1];
    const base64_data = matches[2];
    const buffer = Buffer.from(base64_data, "base64");

    const filename = `${Date.now()}-file.${ext}`;
    const filenameWithPath = `${__dirname}/../../uploads/images/${filename}`;

    return new Promise((resolve, reject) => {
      fs.writeFile(filenameWithPath, buffer, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve({ filename });
        }
      });
    });
  },

  createCategory: (root: any, { input }) => {
    const nuevaCategoria = new categoriaSchema({
      title: input.title,
      image: input.image,
      description: input.description,
    });
    return new Promise((resolve, rejects) => {
      nuevaCategoria.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  createTipo: (root: any, { input }) => {
    const nuevatipo = new tipoSchema({
      title: input.title,
      image: input.image,
      description: input.description,
    });
    return new Promise((resolve, rejects) => {
      nuevatipo.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  createHighkitchenCategory: (root: any, { input }) => {
    const highkitchenCategory = new highkitchenCategorySchema({
      title: input.title,
      image: input.image,
      description: input.description,
    });
    return new Promise((resolve, rejects) => {
      highkitchenCategory.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  createTipoTienda: (root: any, { input }) => {
    const nuevatipo = new tipotiendaSchema({
      title: input.title,
      image: input.image,
      description: input.description,
    });
    return new Promise((resolve, rejects) => {
      nuevatipo.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  eliminarTipoTienda: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      tipotiendaSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error) rejects(error);
        else resolve("Eliminado correctamente");
      });
    });
  },

  eliminarTipo: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      tipoSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error) rejects(error);
        else resolve("Eliminado correctamente");
      });
    });
  },

  eliminarCategory: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      categoriaSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error) rejects(error);
        else resolve("Eliminado correctamente");
      });
    });
  },

  actualizarAdmin: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      userAdminSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) reject(error);
          else resolve(_usuario);
        }
      );
    });
  },

  actualizarUsuario: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      userSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) reject(error);
          else resolve(_usuario);
        }
      );
    });
  },

  actualizarRiders: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      ridersSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) reject(error);
          else resolve(_usuario);
        }
      );
    });
  },

  eliminarAdmin: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      userAdminSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else {
          resolve({
            messages: "Usuario eliminado con éxito",
            success: true,
          });
        }
      });
    });
  },

  eliminarUsuario: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      userSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else {
          resolve({
            messages: "Usuario eliminado con éxito",
            success: true,
          });
        }
      });
    });
  },

  eliminarRiders: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      ridersSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Usuario eliminado con éxito",
            success: true,
          });
      });
    });
  },

  createRestaurant: (root: any, { input }) => {
    const nuevoRestaurant = new restaurantSchema({
      title: input.title,
      image: input.image,
      description: input.description,
      categoryName: input.categoryName,
      categoryID: input.categoryID,
      minime: input.minime,
      shipping: input.shipping,
      extras: input.extras,
      phone: input.phone,
      autoshipping: input.autoshipping,
      alegeno_url: input.alegeno_url,
      email: input.email,
      logo: input.logo,
      inOffert: input.inOffert,
      previous_shipping: input.previous_shipping,
      tipo: input.tipo,
      password: input.password,
      includeCity: input.includeCity,
      type: input.type,
      open: input.open,
      isnew: input.isnew,
      llevar: input.llevar,
      highkitchen: input.highkitchen,
      stimateTime: input.stimateTime,
      slug: input.slug,
      ispartners: input.ispartners,
      schedule: input.schedule,
      channelLinkId: input.channelLinkId,
      collections: input.collections,
      isDeliverectPartner: input.isDeliverectPartner,
      adress: input.adress,
      city: input.city,
      contactCode: input.contactCode,
    });
    return new Promise((resolve, rejects) => {
      nuevoRestaurant.save((error: any, res: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  eliminarRestaurant: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      restaurantSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Restaurante eliminado con éxito",
            success: true,
          });
      });
    });
  },

  actualizarRestaurant: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      restaurantSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error) => {
          if (error)
            reject({
              messages:
                "Hubo un error con tu solicitud vuelve a intentalo por favor",
              success: false,
            });
          else
            resolve({
              messages: "Datos actualizado con éxito",
              success: true,
            });
        }
      );
    });
  },

  crearFavorito: async (
    root: any,
    { restaurantID, usuarioId },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
        data: null,
      };
    }

    const favorito = new favoritoSchema({
      usuarioId,
      restaurantID,
    });
    favorito.id = favorito._id;
    return new Promise((resolve, reject) => {
      favorito.save((error: any) => {
        if (error) {
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        } else {
          resolve({
            messages: "Farmacia añadido a favorito",
            success: true,
          });
        }
      });
    });
  },

  crearCart: async (
    root: any,
    { PlatoId, usuarioId, cantd },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
        data: null,
      };
    }

    const isExist = await cartSchema.findOne({ usuarioId, PlatoId });

    if (!isExist) {
      const cart = new cartSchema({
        usuarioId,
        PlatoId,
        cantd,
      });
      cart.id = cart._id;
      return new Promise((resolve, reject) => {
        cart.save((error: any) => {
          if (error) {
            reject({
              messages: "Hay un problema con tu solicitud",
              success: false,
            });
          } else {
            resolve({
              messages: "Producto añadido al carrito",
              success: true,
            });
          }
        });
      });
    } else {
      const input = {
        usuarioId,
        PlatoId,
        cantd,
      };
      return new Promise((resolve, reject) => {
        cartSchema.findOneAndUpdate(
          { PlatoId: PlatoId },
          input,
          { new: true },
          (error, card) => {
            if (error) reject(error);
            else resolve(card);
          }
        );
      });
    }
  },

  eliminarCart: async (root: any, { id }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      cartSchema.findOneAndDelete({ PlatoId: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Produto eliminado del carrito",
            success: true,
          });
      });
    });
  },

  eliminarFavorito: async (root: any, { id }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      favoritoSchema.findOneAndDelete({ restaurantID: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Restaurante eliminado de favorito",
            success: true,
          });
      });
    });
  },

  createPlatos: (root: any, { input }) => {
    const newPlatos = new PlatoSchema({
      title: input.title,
      ingredientes: input.ingredientes,
      price: input.price,
      previous_price: input.previous_price,
      imagen: input.imagen,
      menu: input.menu,
      restaurant: input.restaurant,
      oferta: input.oferta,
      popular: input.popular,
      news: input.news,
      plu: input.plu,
    });
    return new Promise((resolve, rejects) => {
      newPlatos.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  actualizarPlato: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      PlatoSchema.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error) => {
          if (error)
            reject({
              messages: "Algo no va bien intentalo de nuevo",
              success: true,
            });
          else
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
            });
        }
      );
    });
  },

  createMenu: (root: any, { input }) => {
    const newMenu = new MenuSchema({
      title: input.title,
      subtitle: input.subtitle,
      restaurant: input.restaurant,
    });
    return new Promise((resolve, rejects) => {
      newMenu.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  actualizarMenu: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      MenuSchema.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error) => {
          if (error)
            reject({
              messages: "Algo no va bien intentalo de nuevo",
              success: true,
            });
          else
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
            });
        }
      );
    });
  },

  createAcompanante: (root: any, { input }) => {
    const newAcompanante = new AcompananteSchema({
      name: input.name,
      children: input.children,
      plato: input.plato,
      required: input.required,
      restaurant: input.restaurant,
    });
    return new Promise((resolve, rejects) => {
      newAcompanante.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  createItemCard: async (root: any, { input }) => {
    const existenPlato = await cardSchema.findOne({
      platoID: input.platoID,
      userId: input.userId,
    });
    if (existenPlato) {
      return new Promise((resolve, reject) => {
        cardSchema.findOneAndUpdate(
          { _id: existenPlato._id },
          input,
          { new: true },
          (error, card) => {
            if (error) reject(error);
            else {
              resolve({
                messages: STATUS_MESSAGES.DATA_SUCCESS,
                success: true,
              });
            }
          }
        );
      });
    } else {
      const newCardItem = new cardSchema({
        userId: input.userId,
        restaurant: input.restaurant,
        plato: input.plato,
        platoID: input.platoID,
        total: input.total,
        extra: input.extra,
        complementos: input.complementos ? input.complementos : [],
      });
      return new Promise((resolve, rejects) => {
        newCardItem.save((error: any) => {
          if (error) {
            rejects(error);
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
            });
          }
        });
      });
    }
  },

  actualizarCardItem: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      cardSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, card) => {
          if (error) reject(error);
          else resolve(card);
        }
      );
    });
  },

  eliminarCardItem: async (root: any, { id }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      cardSchema.findOneAndDelete({ platoID: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Producto eliminado de tu carrito",
            success: true,
          });
      });
    });
  },

  crearCupon: async (root: any, { input }) => {
    try {
      const nuevoCupon = new Cupones({
        clave: input.clave,
        descuento: input.descuento,
        tipo: input.tipo,
      });
      return new Promise((resolve, reject) => {
        nuevoCupon.save((error, cupon) => {
          if (error) {
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con su solicitud",
          data: null,
        });
      });
    }
  },

  eliminarCupon: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      Cupones.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Miembro eliminado con éxito",
          });
      });
    });
  },

  crearModificarOrden: async (root: any, { input }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debes iniciar sesión para continiar",
          data: null,
        };
      }

      let orden: any;
      if (input.id) {
        orden = await orderSchema.findById(ObjectId(input.id)).exec();
        orden.set({ riders: input.riders ? input.riders : null });
      }

      if (!input.restaurant) {
        const restaurants = await restaurantSchema
          .findById(ObjectId(input.restaurant))
          .exec();
        if (!restaurants) {
          return new Promise((_resolve, reject) => {
            return reject({
              success: false,
              message: "No existe un restaurante para esta orden",
              data: null,
            });
          });
        }
      }

      let cupon: any;
      if (!!input.clave) {
        cupon = await Cupones.findOne({ clave: input.clave }).exec();
      } else if (!!input.cupon) {
        cupon = input.cupon;
      }

      let userData: any;
      userData = await userSchema.findOne({
        _id: input.userID,
      });

      let AdreesData: any;
      AdreesData = await adressSchema.findOne({
        _id: input.adress,
      });

      let StoreData: any;
      StoreData = await restaurantSchema.findOne({
        _id: input.restaurant,
      });

      let platos: any;
      platos = await cardSchema.find({
        userId: input.userID,
        restaurant: input.restaurant,
      });

      if (!orden || !orden._id) {
        orden = new orderSchema({
          display_id: input.display_id,
          restaurant: input.restaurant,
          time: input.time,
          cupon: cupon && cupon._id ? cupon._id : cupon,
          nota: input.nota,
          aceptaTerminos: input.aceptaTerminos,
          cantidad: input.cantidad || 1,
          userID: input.userID,
          platos: platos,
          cubiertos: input.cubiertos,
          total: input.total,
          propina: input.propina,
          adress: input.adress,
          city: input.city,
          isvalored: input.isvalored,
          riders: input.riders,
          llevar: input.llevar,
          subtotal: input.subtotal,
          complement: input.complement,
          descuentopromo: input.descuentopromo,
          tarifa: input.tarifa,
          envio: input.envio,
          propinaTotal: input.propinaTotal,
          schedule: input.schedule,
          userData: userData,
          AdreesData: AdreesData,
          StoreData: StoreData,
          holdedID: input.holdedID,
          holdedRidersID: input.holdedRidersID,
          holdedPartnerID: input.holdedPartnerID,
          AdreesStoreData: input.AdreesStoreData,
          reportRiders: input.reportRiders,
        });
      } else {
        if (!orden.cupon) {
          orden.set({ cupon: cupon ? cupon._id : null });
        }
      }

      return new Promise((resolve, reject) => {
        orden.save((error: any, ordenguardada: any) => {
          if (error) {
            return reject(error);
          } else {
            if (!ordenguardada.descuento && cupon && cupon._id) {
              ordenguardada.descuento = cupon;
            }
            setDistanceToOrder(ordenguardada._id);
            return resolve(ordenguardada);
          }
        });
      });
    } catch (error) {
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con tu solicitud",
          data: null,
        });
      });
    }
  },

  crearValoracion: async (root: any, { input }) => {
    try {
      const nuevoValoracion = new RatingSchema({
        user: input.user,
        comment: input.comment,
        value: input.value,
        restaurant: input.restaurant,
      });

      return new Promise((resolve, reject) => {
        nuevoValoracion.save((error, rating) => {
          if (error) {
            return reject(error);
          } else {
            return resolve(rating);
          }
        });
      });
    } catch (error) {
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con su solicitud",
          data: null,
        });
      });
    }
  },

  createNotification: async (root: any, { input }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para continiar",
        success: false,
      };
    }
    let newNotification = new NotificationSchema({
      user: input.user,
      usuario: input.usuario,
      restaurant: input.restaurant,
      type: input.type,
      ordenId: input.ordenId,
      riders: input.riders,
    });
    return new Promise((resolve, reject) => {
      newNotification.save((error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Notificacion leída con éxito",
            success: true,
          });
      });
    });
  },

  readNotification: async (
    root: any,
    { notificationId },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para continiar",
        success: false,
      };
    }

    return new Promise((resolve, reject) => {
      NotificationSchema.findOneAndUpdate(
        { _id: ObjectId(notificationId) },
        { $set: { read: true } },
        (error) => {
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Notificacion leída con éxito",
              success: true,
            });
        }
      );
    });
  },

  createOpinion: (root: any, { input }) => {
    const nuevaOpinion = new OpinionSchema({
      plato: input.plato,
      comment: input.comment,
      rating: input.rating,
      user: input.user,
    });
    return new Promise((resolve, rejects) => {
      nuevaOpinion.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  OrdenProceed: async (root: any, { orden, estado, progreso, status }) => {
    return new Promise((resolve, reject) => {
      const message = estado === "Confirmada" ? "Confirmada" : "Rechazada";
      let dataToUpdate = { estado };
      //@ts-ignore
      if (progreso) dataToUpdate.progreso = progreso;
      //@ts-ignore
      if (status) dataToUpdate.status = status;
      orderSchema.findOneAndUpdate(
        { _id: orden },
        dataToUpdate,
        async (error: any, order: any) => {
          if (error) {
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          } else {
            if (order.riders && estado === "Listo para recoger") {
              const ridersS = await ridersSchema.findOne({ _id: order.riders });
              sendNotification(
                ridersS?.OnesignalID,
                `${order.StoreData.title} tiene el pedido listo para recoger.`
              );
            }

            resolve({
              messages: "Orden procesada con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  eliminarPlato: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      PlatoSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Plato eliminado con éxito",
            success: true,
          });
      });
    });
  },

  eliminarComplemento: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      AcompananteSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Complemento eliminado con éxito",
            success: true,
          });
      });
    });
  },

  eliminarMenu: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      MenuSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Menú eliminado con éxito",
            success: true,
          });
      });
    });
  },

  crearPago: (root: any, { input }) => {
    const nuevoPago = new pagoSchema({
      nombre: input.nombre,
      iban: input.iban,
      restaurantID: input.restaurantID,
    });

    nuevoPago.id = nuevoPago._id;

    return new Promise((resolve, reject) => {
      nuevoPago.save((error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Pago añadido con éxito",
            success: true,
          });
      });
    });
  },

  eliminarPago: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      pagoSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Pago eliminado con éxito",
            success: true,
          });
      });
    });
  },

  crearDeposito: (root: any, { input }) => {
    const nuevoDeposito = new transSchema({
      fecha: new Date(),
      estado: input.estado,
      total: input.total,
      restaurantID: input.restaurantID,
    });
    nuevoDeposito.id = nuevoDeposito._id;
    return new Promise((resolve, reject) => {
      nuevoDeposito.save((error: any) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Deposito añadido con éxito",
            success: true,
          });
      });
    });
  },

  eliminarDeposito: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      transSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            messages: "Hay un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            messages: "Deposito eliminado con éxito",
          });
      });
    });
  },

  createAdress: (root: any, { input }) => {
    const nuevaadress = new adressSchema({
      formatted_address: input.formatted_address,
      puertaPiso: input.puertaPiso,
      type: input.type,
      usuario: input.usuario,
      lat: input.lat,
      lgn: input.lgn,
    });
    return new Promise((resolve, rejects) => {
      nuevaadress.save((error: any, adress: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: adress,
          });
        }
      });
    });
  },

  createStoreAdress: (root: any, { input }) => {
    const nuevaadress = new adressStoreSchema({
      calle: input.calle,
      numero: input.numero,
      codigoPostal: input.codigoPostal,
      ciudad: input.ciudad,
      store: input.store,
      lat: input.lat,
      lgn: input.lgn,
    });
    return new Promise((resolve, rejects) => {
      nuevaadress.save((error: any, adress: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            success: true,
            message: STATUS_MESSAGES.DATA_SUCCESS,
            data: adress,
          });
        }
      });
    });
  },

  actualizarAdress: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      adressSchema.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, adress) => {
          if (error) reject(error);
          else
            resolve({
              messages: "Dirección actualizada con éxito",
              success: true,
              data: adress,
            });
        }
      );
    });
  },

  eliminarAdress: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      adressSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Dirección eliminado con éxito",
            success: true,
          });
      });
    });
  },

  eliminarAdressStore: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      adressStoreSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Dirección eliminado con éxito",
            success: true,
          });
      });
    });
  },

  eliminarPost: async (root: any, { id }, { usuarioActual }) => {
    const isAdmin = await userAdminSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: true,
        messages: "No estas autorizado para eliminar un post",
        data: null,
      };
    } else {
      return new Promise((resolve, reject) => {
        postSchema.findOneAndDelete({ _id: id }, (error) => {
          if (error)
            reject({
              message: "Hay un problema con su solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Dirección eliminado con éxito",
              success: true,
            });
        });
      });
    }
  },

  actualizarPost: async (root: any, { input }, { usuarioActual }) => {
    const isAdmin = await userAdminSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: true,
        messages: "No estas autorizado para actualizar un post",
        data: null,
      };
    } else {
      return new Promise((resolve, reject) => {
        postSchema.findOneAndUpdate(
          { _id: input.id },
          input,
          { new: true },
          (error, post) => {
            if (error) reject(error);
            else
              resolve({
                success: true,
                message: "Dirección actualizada con éxito",
                data: post,
              });
          }
        );
      });
    }
  },

  createPost: async (root: any, { input }, { usuarioActual }) => {
    const isAdmin = await userAdminSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: true,
        message: "No estas autorizado para crear un post",
        data: null,
      };
    } else {
      const nuevoPost = new postSchema({
        title: input.title,
        image: input.image,
        shortDescription: input.shortDescription,
        like: input.like,
        tags: input.tags,
        author: input.author,
        category: input.category,
        readTime: input.readTime,
        content: input.content,
        slug: input.slug,
        country: input.country,
      });
      return new Promise((resolve, rejects) => {
        nuevoPost.save((error: any, post: any) => {
          if (error) {
            rejects(error);
          } else {
            resolve({
              success: true,
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              data: post,
            });
          }
        });
      });
    }
  },

  createCustonOrder: async (root: any, { input }, { usuarioActual }) => {
    const isAdmin = await userSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: true,
        messages: "No estas autorizado para crear un esta orden",
        data: null,
      };
    } else {
      const nuevaOrder = new customOrderSchema({
        display_id: input.display_id,
        riders: input.riders,
        origin: input.origin,
        destination: input.destination,
        schedule: input.schedule,
        distance: input.distance,
        nota: input.nota,
        date: input.date,
        city: input.city,
        userID: input.userID,
        estado: input.estado,
        status: input.status,
        progreso: input.progreso,
        total: input.total,
        product_stimate_price: input.product_stimate_price,
      });
      return new Promise((resolve, rejects) => {
        nuevaOrder.save((error: any, order: any) => {
          if (error) {
            rejects(error);
          } else {
            resolve({
              success: true,
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              data: order,
            });
          }
        });
      });
    }
  },

  actualizarOrderProcess: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      customOrderSchema.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, order) => {
          if (error) reject(error);
          else
            resolve({
              success: true,
              messages: "Dirección actualizada con éxito",
              data: order,
            });
        }
      );
    });
  },

  actualizarCity: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      citySchema.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error) => {
          if (error)
            reject({
              messages: "Algo va mal intentalo de Nuevo",
              success: true,
            });
          else
            resolve({
              messages: "Dirección actualizada con éxito",
              success: true,
            });
        }
      );
    });
  },

  createCity: async (root: any, { input }) => {
    const nuevaCity = new citySchema({
      close: input.close,
      city: input.city,
      title: input.title,
      subtitle: input.subtitle,
      imagen: input.imagen,
    });
    return new Promise((resolve, rejects) => {
      nuevaCity.save((error: any, order: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createOffert: async (root: any, { input }) => {
    const nuevaOffert = new offertsSchema({
      store: input.store,
      slug: input.slug,
      city: input.city,
      apertura: input.apertura,
      cierre: input.cierre,
      imagen: input.imagen,
      open: input.open,
    });
    return new Promise((resolve, rejects) => {
      nuevaOffert.save((error: any, order: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  eliminarOfferts: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      offertsSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Algo va mal intentalo de Nuevo",
            success: true,
          });
        else
          resolve({
            messages: "Oferta eliminada con éxito",
            success: true,
          });
      });
    });
  },

  createTransactionRider: async (root: any, { input }) => {
    const nuevatransaction = new transactionSchema({
      rider: input.rider,
      km: input.km,
      base: input.base,
      time: input.time,
      order: input.order,
      extra: input.extra,
      combinado: input.combinado,
      lluvia: input.lluvia,
      total: input.total,
      propina: input.propina,
      iva: input.iva,
    });
    return new Promise((resolve, rejects) => {
      nuevatransaction.save(async (error: any, order: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          const riders = await ridersSchema.findOne({ _id: input.rider });
          RegiterCompraRider(riders, input);
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createQuincena: async (root: any, { input }) => {
    const nuevaquincena = new quincenaSchama({
      fromDate: input.fromDate,
      toDate: input.toDate,
      numberQuincena: input.numberQuincena,
    });
    return new Promise((resolve, rejects) => {
      nuevaquincena.save((error: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createCollection: async (root: any, { input }) => {
    const nuevaCollection = new collectionSchema({
      title: input.title,
      image: input.image,
      store: input.store,
      sorting: input.sorting,
    });
    return new Promise((resolve, rejects) => {
      nuevaCollection.save((error: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createsubCollection: async (root: any, { input }) => {
    const nuevasubCollection = new subCollectionSchema({
      title: input.title,
      collectiontype: input.collectiontype,
      sorting: input.sorting,
    });
    return new Promise((resolve, rejects) => {
      nuevasubCollection.save((error: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  addToCart: async (root: any, { input }) => {
    const newCart = new newCartSchema({
      userId: input.userId,
      storeId: input.storeId,
      productId: input.productId,
      items: input.items,
      addToCart: input.addToCart,
    });

    const exist = await newCartSchema.findOne({ productId: input.productId });

    if (exist) {
      return new Promise((resolve, rejects) => {
        newCartSchema.findOneAndUpdate(
          { productId: input.productId },
          input,
          { new: true },
          (error: any) => {
            if (error) {
              rejects({
                messages: "Algo va mal intentalo de nuevo",
                success: true,
              });
            } else {
              resolve({
                messages: "Producto añadido a la cesta",
                success: true,
              });
            }
          }
        );
      });
    } else {
      return new Promise((resolve, rejects) => {
        newCart.save((error: any) => {
          if (error) {
            rejects({
              messages: "Algo va mal intentalo de nuevo",
              success: true,
            });
          } else {
            resolve({
              messages: "Producto añadido a la cesta",
              success: true,
            });
          }
        });
      });
    }
  },

  deleteCartItem: async (root: any, { id }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      newCartSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Producto eliminado de la cesta",
            success: true,
          });
      });
    });
  },
};
