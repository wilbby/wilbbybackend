import { Query } from "./query";
import { Mutation } from "./mutation";
import userSchema from "../models/user";
import favoritoSchema from "../models/Favorito";
import cartSchema from "../models/cartAdd";
import cardSchema from "../models/card";
import restaurantSchema from "../models/restaurant";
import RatingSchema from "../models/rating";
import orderSchema from "../models/order";
import OpinionSchema from "../models/Opinion";
import adressSchema from "../models/adress";
import adressStoreSchema from "../models/adressStore";
import ridersSchema from "../models/riders";
import AcompananteSchema from "../models/nemu/acompanante";
import userAdminSchema from "../models/userAdmin";
import subCollection from "../models/subcollection";
import menu from "../models/nemu/menu";

import categorySchema from "../models/newMenu/category";
import bundlesSchema from "../models/newMenu/bundles";
import NewcartSchema from "../models/newOrder/addToCart";
import modifierGroupsSchema from "../models/newMenu/modifierGroups";
import modifiersSchema from "../models/newMenu/modifiers";
import productSchema from "../models/newMenu/products";

export const resolvers = {
  Query,
  Mutation,

  Menus: {
    Categories(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        categorySchema.find({ menu: parent.menuId }, (error, category) => {
          if (error) {
            rejects(error);
          } else {
            if (!category) resolve([]);
            else {
              resolve(category);
            }
          }
        });
      });
    },
  },

  CategoryNew: {
    Products(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        productSchema.find(
          {
            _id: parent.products.map((p: string) => {
              return p;
            }),
          },
          (error, product) => {
            if (error) {
              rejects(error);
            } else {
              if (!product) resolve([]);
              else {
                resolve(product);
              }
            }
          }
        );
      });
    },
  },

  Bundles: {
    Products(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        productSchema.find(
          {
            _id: parent.subProducts.map((p: string) => {
              return p;
            }),
          },
          (error, product) => {
            if (error) {
              rejects(error);
            } else {
              if (!product) resolve([]);
              else {
                resolve(product);
              }
            }
          }
        );
      });
    },
  },

  Modifiers: {
    ModifierGroups(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        modifierGroupsSchema.find(
          {
            _id: parent.subProducts.map((p: string) => {
              return p;
            }),
          },
          (error, product) => {
            if (error) {
              rejects(error);
            } else {
              if (!product) resolve([]);
              else {
                resolve(product);
              }
            }
          }
        );
      });
    },
  },

  Products: {
    Bundles(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        bundlesSchema.find(
          {
            _id: parent.subProducts.map((p: string) => {
              return p;
            }),
          },
          (error, product) => {
            if (error) {
              rejects(error);
            } else {
              if (!product) resolve([]);
              else {
                resolve(product);
              }
            }
          }
        );
      });
    },

    ModifierGroups(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        modifierGroupsSchema.find(
          {
            _id: parent.subProducts.map((id: string) => {
              return id;
            }),
          },
          (error, product) => {
            if (error) {
              rejects(error);
            } else {
              if (!product) resolve([]);
              else {
                resolve(product);
              }
            }
          }
        );
      });
    },

    cartItems(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, rejects) => {
        NewcartSchema.findOne(
          {
            userId: usuarioActual._id,
            productId: parent._id,
          },
          (error, cart) => {
            if (error) {
              rejects(error);
            } else {
              if (!cart) resolve({});
              else {
                resolve(cart);
              }
            }
          }
        );
      });
    },
  },

  ModifierGroups: {
    Modifiers(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        modifiersSchema.find(
          {
            _id: parent.subProducts.map((p: string) => {
              return p;
            }),
          },
          (error, product) => {
            if (error) {
              rejects(error);
            } else {
              if (!product) resolve([]);
              else {
                resolve(product);
              }
            }
          }
        );
      });
    },
  },

  Collections: {
    subCollectionItems(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        subCollection.find(
          { collectiontype: parent._id },
          (error, collection) => {
            if (error) {
              rejects(error);
            } else {
              if (!collection) resolve(false);
              else {
                const collections = collection;
                collections.sort((a, b) =>
                  //@ts-ignore
                  a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
                );
                resolve(collections);
              }
            }
          }
        );
      });
    },
  },

  CustonOrder: {
    Riders(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        ridersSchema.findOne({ _id: parent.riders }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },

    usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.userID }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Post: {
    Author(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userAdminSchema.findOne({ _id: parent.author }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Platos: {
    async SubProductos(parent: any, args: any) {
      const dataID = await restaurantSchema.findOne({ _id: parent.restaurant });

      const queryID = dataID?.isDeliverectPartner
        ? parent.deliverectID
        : parent._id;

      return new Promise((resolve, rejects) => {
        AcompananteSchema.find({ plato: queryID }, (error, option) => {
          if (error) rejects(error);
          else {
            if (!option) resolve(false);
            else {
              resolve(option);
            }
          }
        });
      });
    },

    cartItems(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        cardSchema.findOne({ platoID: parent._id }, (error, cart) => {
          if (error) rejects(error);
          else {
            if (!cart) resolve(false);
            else {
              resolve(cart);
            }
          }
        });
      });
    },

    opiniones(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        OpinionSchema.find({ plato: parent._id }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },

    requireOption(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        AcompananteSchema.findOne({ plato: parent._id }, (error, option) => {
          if (error) rejects(error);
          else {
            if (option) resolve(true);
            else {
              resolve(false);
            }
          }
        });
      });
    },

    anadidoCart(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        userSchema.findOne({ _id: usuarioActual._id }, (error, usuario) => {
          if (error) reject(error);
          else {
            if (!usuario) resolve(false);
            else {
              cartSchema.findOne(
                { usuarioId: usuario._id, PlatoId: parent._id },
                (error, favorito) => {
                  if (error) reject(error);
                  else {
                    if (!favorito) resolve(false);
                    else resolve(true);
                  }
                }
              );
            }
          }
        });
      });
    },
    cantd(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        cartSchema.findOne({ PlatoId: parent._id }, (error, plato) => {
          if (error) rejects(error);
          else {
            if (!plato) resolve(1);
            else {
              resolve(plato.cantd);
            }
          }
        });
      });
    },
  },

  Opinion: {
    Usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.user }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Notification: {
    Riders(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        ridersSchema.findOne({ _id: parent.riders }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },

    Usuarios(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.usuario }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },

    Restaurant(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        restaurantSchema.findOne(
          { _id: parent.restaurant },
          (error, restaurant) => {
            if (error) rejects(error);
            else {
              if (!restaurant) resolve(false);
              else {
                resolve(restaurant);
              }
            }
          }
        );
      });
    },

    Orden(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        orderSchema.findOne({ _id: parent.ordenId }, (error, orden) => {
          if (error) rejects(error);
          else {
            if (!orden) resolve(false);
            else {
              resolve(orden);
            }
          }
        });
      });
    },
  },

  Valoracion: {
    Usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.user }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Orden: {
    AdresStore(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        adressStoreSchema.findOne(
          { _id: parent.adresStore },
          (error, adress) => {
            if (error) rejects(error);
            else {
              if (!adress) resolve(false);
              else {
                resolve(adress);
              }
            }
          }
        );
      });
    },

    Adress(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        adressSchema.findOne({ _id: parent.adress }, (error, adress) => {
          if (error) rejects(error);
          else {
            if (!adress) resolve(false);
            else {
              resolve(adress);
            }
          }
        });
      });
    },

    Riders(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        ridersSchema.findOne({ _id: parent.riders }, (error, riders) => {
          if (error) rejects(error);
          else {
            if (!riders) resolve(false);
            else {
              resolve(riders);
            }
          }
        });
      });
    },

    restaurants(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        restaurantSchema.findOne(
          { _id: parent.restaurant },
          (error, restaurant) => {
            if (error) rejects(error);
            else {
              if (!restaurant) resolve(false);
              else {
                resolve(restaurant);
              }
            }
          }
        );
      });
    },
    usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.userID }, (error, userID) => {
          if (error) rejects(error);
          else {
            if (!userID) resolve(false);
            else {
              resolve(userID);
            }
          }
        });
      });
    },
  },

  Restaurant: {
    anadidoFavorito(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        userSchema.findOne({ _id: usuarioActual._id }, (error, usuario) => {
          if (error) reject(error);
          else {
            if (!usuario) resolve(false);
            else {
              favoritoSchema.findOne(
                { usuarioId: usuario._id, restaurantID: parent._id },
                (error, favorito) => {
                  if (error) reject(error);
                  else {
                    if (!favorito) resolve(false);
                    else resolve(true);
                  }
                }
              );
            }
          }
        });
      });
    },

    Valoracion(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        RatingSchema.find({ restaurant: parent._id }, (error, valoracion) => {
          if (error) rejects(error);
          else {
            if (!valoracion) resolve(false);
            else {
              resolve(valoracion);
            }
          }
        });
      });
    },

    Menus(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        menu.find({ restaurant: parent._id }, (error, menu) => {
          if (error) rejects(error);
          else {
            if (!menu) resolve(false);
            else {
              resolve(menu);
            }
          }
        });
      });
    },

    CategoryNew(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        categorySchema.find({ storeId: parent._id }, (error, menu) => {
          if (error) rejects(error);
          else {
            if (!menu) resolve(false);
            else {
              resolve(menu);
            }
          }
        });
      });
    },
  },

  RestaurantFavorito: {
    restaurant(parent: any) {
      return new Promise((resolve, reject) => {
        restaurantSchema.findById(
          { _id: parent.restaurantID },
          (error, restaurant) => {
            if (error) {
              reject(error);
            } else {
              resolve(restaurant);
            }
          }
        );
      });
    },
  },

  Cart: {
    Restaurant(parent: any) {
      return new Promise((resolve, reject) => {
        restaurantSchema.findById(
          { _id: parent.restaurant },
          (error, restaurant) => {
            if (error) {
              reject(error);
            } else {
              resolve(restaurant);
            }
          }
        );
      });
    },

    UserId(parent: any) {
      return new Promise((resolve, reject) => {
        userSchema.findById({ _id: parent.userId }, (error, user) => {
          if (error) {
            reject(error);
          } else {
            resolve(user);
          }
        });
      });
    },
  },
};
