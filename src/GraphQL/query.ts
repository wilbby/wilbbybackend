import userSchema from "../models/user";
import categoriaSchema from "../models/categorias";
import restaurantSchema from "../models/restaurant";
import MenuSchema from "../models/nemu/menu";
import AcompananteSchema from "../models/nemu/acompanante";
import PlatoSchema from "../models/nemu/platos";
import favoritoSchema from "../models/Favorito";
import Cupones from "../models/cupones";
import ridersSchema from "../models/riders";
import cardSchema from "../models/card";
import orderSchema from "../models/order";
import { STATUS_MESSAGES } from "./Status_messages";
import RatingSchema from "../models/rating";
import NotificationSchema from "../models/Notification";
import OpinionSchema from "../models/Opinion";
import pagoSchema from "../models/Pago";
import transSchema from "../models/transacciones";
import adressSchema from "../models/adress";
import adressStoreSchema from "../models/adressStore";
import tipoSchema from "../models/tipo";
import highkitchenCategorySchema from "../models/highkitchenCategory";
import tipotiendaSchema from "../models/tipotienda";
import userAdminSchema from "../models/userAdmin";
import postSchema from "../models/post";
import customorderSchema from "../models/custonorder";
import citySchema from "../models/cityclose";
import offerts from "../models/offerts";
import riderTransactionScheme from "../models/trasactionRider";
import collectionSchema from "../models/collections";
import subCollectionSchema from "../models/subcollection";

//newProducts
import categorySchema from "../models/newMenu/category";
import bundlesSchema from "../models/newMenu/bundles";
import MenusSchema from "../models/newMenu/Menu";
import modifierGroupsSchema from "../models/newMenu/modifierGroups";
import modifiersSchema from "../models/newMenu/modifiers";
import productSchema from "../models/newMenu/products";

import dotenv from "dotenv";
import quincena from "../models/quincena";
dotenv.config({ path: "variables.env" });
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

export const Query = {
  getRiderForAdmin: (root: any, { city }, { usuarioActual }) => {
    const isAdmin = userAdminSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: false,
        messages: STATUS_MESSAGES.NOT_LOGGED_IN,
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        ridersSchema.find({ city: city, isAvalible: true }, (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  getRiderForAdminAll: (root: any, {}, { usuarioActual }) => {
    const isAdmin = userAdminSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: false,
        messages: STATUS_MESSAGES.NOT_LOGGED_IN,
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        ridersSchema.find((err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  getUsuario: (root: any, {}, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        messages: STATUS_MESSAGES.NOT_LOGGED_IN,
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: usuarioActual._id }, (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  getUsuarioAdmin: (root: any, {}, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        messages: STATUS_MESSAGES.NOT_LOGGED_IN,
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        userAdminSchema.findOne({ _id: usuarioActual._id }, (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  getRiders: (root: any, {}, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        messages: STATUS_MESSAGES.NOT_LOGGED_IN,
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        ridersSchema.findOne({ _id: usuarioActual._id }, (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  obtenerUsuario: (root: any, args: any, { usuarioActual }) => {
    if (!usuarioActual) {
      return null;
    }
    const usuario = userSchema.findOne({ _id: usuarioActual._id });

    return usuario;
  },

  obtenerAdmin: (root: any, args: any, { usuarioActual }) => {
    if (!usuarioActual) {
      return null;
    }
    const usuario = userAdminSchema.findOne({ _id: usuarioActual._id });

    return usuario;
  },

  getCategory: (root: any) => {
    return new Promise((resolve, rejects) => {
      categoriaSchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getTipo: (root: any) => {
    return new Promise((resolve, rejects) => {
      tipoSchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getHighkitchenCategory: (root: any) => {
    return new Promise((resolve, rejects) => {
      highkitchenCategorySchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getTipoTienda: (root: any) => {
    return new Promise((resolve, rejects) => {
      tipotiendaSchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantHighkitchen: (root: any, { city, llevar, category }) => {
    let condition = { includeCity: { $all: [city] }, highkitchen: true };
    //@ts-ignore
    if (llevar) condition.llevar = llevar;
    // @ts-ignore
    if (category) condition.tipo = category;
    return new Promise((resolve, rejects) => {
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getRestaurant: (root: any, { city }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.find(
        { includeCity: { $all: [city] } },
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getAdress: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      adressSchema
        .find({ usuario: id }, (err: any, res: any) => {
          if (err) {
            rejects({
              success: false,
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              data: {},
            });
          } else {
            resolve({
              success: true,
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getAdressStore: (root: any, { id, city }) => {
    return new Promise((resolve, rejects) => {
      let condition = { store: id, ciudad: city };
      //@ts-ignore
      adressStoreSchema
        .findOne(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: {},
            });
          } else {
            resolve({
              success: true,
              message: STATUS_MESSAGES.DATA_SUCCESS,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getAdressStoreforSelect: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      let condition = { store: id };
      adressStoreSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              success: false,
              message: STATUS_MESSAGES.S_WENT_WRONG,
              data: {},
            });
          } else {
            resolve({
              success: true,
              message: STATUS_MESSAGES.DATA_SUCCESS,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getRestaurantSearch: (
    root: any,
    { city, price, category, search, llevar, tipo }
  ) => {
    let condition = { includeCity: { $all: [city] } };
    // @ts-ignore
    if (search) condition = { $text: { $search: `"\"${search} \""` } };
    // @ts-ignore
    if (price) condition.minime = { $lt: price };
    //@ts-ignore
    if (llevar) condition.llevar = llevar;
    // @ts-ignore
    if (category) condition.categoryID = category;
    // @ts-ignore
    if (tipo) condition.tipo = tipo;

    return new Promise((resolve, rejects) => {
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort(search ? null : { $natural: -1 });
    });
  },

  getProductoSearch: (
    root: any,
    { farmacy, search, page, limit, category }
  ) => {
    let condition = {};
    // @ts-ignore
    if (category) condition.menu = category;
    // @ts-ignore
    if (search) condition = { $text: { $search: `"\"${search} \""` } };
    // @ts-ignore
    if (farmacy) condition.restaurant = farmacy;

    return new Promise((resolve, rejects) => {
      PlatoSchema.find(condition, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },

  getRestaurantForCategory: (root: any, { city, category, tipo, llevar }) => {
    return new Promise((resolve, rejects) => {
      let condition = { includeCity: { $all: [city] } };

      //@ts-ignore
      if (llevar) condition.llevar = llevar;
      //@ts-ignore
      if (tipo) condition.tipo = tipo;
      // @ts-ignore
      if (category) condition.categoryID = category;

      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getRestaurantForID: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne({ _id: id }, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: {},
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantForIDWeb: (root: any, { id, city }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne(
        { _id: id, includeCity: { $all: [city] } },
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getRestaurantForDetails: (root: any, { slug, city }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne(
        { slug: slug, includeCity: { $all: [city] } },
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getRestaurantForSlugWeb: (root: any, { slug }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne({ slug: slug }, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: {},
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantFavorito: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      favoritoSchema.find({ usuarioId: id }, (error: any, favourite: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: favourite,
          });
        }
      });
    });
  },

  getStoreInOffert: (root: any, { city }) => {
    return new Promise((resolve, rejects) => {
      let condition = { inOffert: true, includeCity: { $all: [city] } };
      restaurantSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getMenu: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      MenuSchema.find({ restaurant: id }, (error: any, menu: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          const menus = menu;
          menus.sort((a, b) =>
            a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
          );
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: menus,
          });
        }
      });
    });
  },

  getMenumarca: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      MenuSchema.findOne({ restaurant: id }, (error: any, menu: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: menu,
          });
        }
      });
    });
  },

  getPlato: (root: any, { id, page, limit }) => {
    return new Promise((resolve, reject) => {
      PlatoSchema.find({ menu: id }, (error: any, plato: any) => {
        if (!error) {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: plato,
          });
        } else {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        }
      })
        .skip((page - 1) * limit)
        .limit(limit * 1);
    });
  },

  getAcompanante: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      AcompananteSchema.find({ plato: id }, (error: any, favourite: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: favourite,
          });
        }
      });
    });
  },

  getItemCart: (root: any, { id, PlatoID }) => {
    return new Promise((resolve, reject) => {
      cardSchema.find(
        { userId: id, platoID: PlatoID },
        (error: any, cart: any) => {
          if (error) {
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              list: [],
            });
          } else {
            resolve({
              success: true,
              message: "Operación realizada con éxito",
              list: cart,
            });
          }
        }
      );
    });
  },

  getItemCarts: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      cardSchema.find({ userId: id }, (error: any, cart: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: cart,
          });
        }
      });
    });
  },

  getMyItemCart: (root: any, { id, restaurant }) => {
    return new Promise((resolve, reject) => {
      cardSchema.find(
        { userId: id, restaurant: restaurant },
        (error: any, cart: any) => {
          if (error) {
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              list: [],
            });
          } else {
            resolve({
              success: true,
              message: "Operación realizada con éxito",
              list: cart,
            });
          }
        }
      );
    });
  },

  getCupon: async (root: any, { clave }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }
      const usuario = await userSchema.findOne({
        _id: usuarioActual._id,
      });
      if (!usuario) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }
      return new Promise((resolve, reject) => {
        Cupones.findOne({ clave }, (error, cupon) => {
          if (error) {
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getCuponAll: async (root: any) => {
    try {
      return new Promise((resolve, reject) => {
        Cupones.findOne((error: any, cupon: any) => {
          if (error) {
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getOrderByUsuario: async (
    root: any,
    { usuarios, dateRange },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    const usuario = await userSchema.findOne({
      _id: usuarioActual._id,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    return new Promise((resolve, reject) => {
      let condition = {
        userID: usuarios,
        estado: {
          $in: [
            "Pagado",
            "Confirmada",
            "Listo para recoger",
            "Preparando para el envío",
            "Aceptada",
            "En camino",
            "Entregada",
            "Rechazada por el rider",
          ],
        },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, consulta: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: consulta,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByUsuarioAll: async (
    root: any,
    { usuarios, dateRange },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    const usuario = await userSchema.findOne({
      _id: usuarioActual._id,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    return new Promise((resolve, reject) => {
      let condition = {
        userID: usuarios,
        estado: { $ne: "Pendiente de pago" },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, consulta: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: consulta,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByAdmin: async (
    root: any,
    { city, dateRange, id },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    const usuario = await userAdminSchema.findOne({
      _id: usuarioActual._id,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    return new Promise((resolve, reject) => {
      let condition = {
        city: city,
        estado: {
          $in: [
            "Pagado",
            "Confirmada",
            "Listo para recoger",
            "Preparando para el envío",
            "En camino",
            "Rechazada por el rider",
          ],
        },
      };

      //@ts-ignore
      if (id) condition.display_id = id;

      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, consulta: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: consulta,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByAdminAll: async (
    root: any,
    { city, dateRange, id, page },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    const usuario = await userAdminSchema.findOne({
      _id: usuarioActual._id,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    return new Promise((resolve, reject) => {
      let condition = {
        city: city,
        estado: {
          $in: [
            "Pendiente de pago",
            "Pagado",
            "Confirmada",
            "Listo para recoger",
            "Preparando para el envío",
            "En camino",
            "Entregada",
            "Rechazado",
            "Rechazada por la tienda",
            "Rechazada por el rider",
            "Devuelto",
            "Valorada",
            "Resolución",
          ],
        },
      };

      //@ts-ignore
      if (id) condition.display_id = id;
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, consulta: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: consulta,
            });
          }
        })
        .limit(10 * 1)
        .skip((page - 1) * 11)
        .sort({ $natural: -1 });
    });
  },

  getOrderByRiders: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        riders: id,
        estado: {
          $in: [
            "Listo para recoger",
            "Confirmada",
            "Preparando para el envío",
            "En camino",
            "Pagado",
          ],
        },
      };
      orderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByRidersAll: async (root: any, { id, page }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        riders: id,
        estado: { $ne: "Pendiente de pago" },
      };
      orderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: order,
            });
          }
        })
        .limit(11 * 1)
        .skip((page - 1) * 11)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getOrderByRestaurantNew: async (root: any, { id, dateRange }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        restaurant: id,
        estado: {
          $in: ["Pagado", "Confirmada", "Listo para recoger"],
        },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByRestaurant: async (root: any, { id, dateRange, page, orderID }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        restaurant: id,
        estado: {
          $in: [
            "Pagado",
            "Confirmada",
            "Listo para recoger",
            "Preparando para el envío",
            "En camino",
            "Entregada",
            "Rechazado",
            "Rechazada por la tienda",
            "Rechazada por el rider",
            "Devuelto",
            "Valorada",
            "Resolución",
          ],
        },
      };
      // @ts-ignore
      if (orderID) condition.display_id = orderID;

      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: order,
            });
          }
        })
        .limit(20 * 1)
        .skip((page - 1) * 20)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getOrderByRestaurantID: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      orderSchema.findOne({ _id: id }, (error: any, order: any) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            list: [],
          });
        else {
          resolve({
            success: true,
            message: "Consulta extraida con éxito",
            list: order,
          });
        }
      });
    });
  },

  getValoraciones: async (root: any, { restaurant }) => {
    try {
      return new Promise((resolve, reject) => {
        RatingSchema.find(
          { restaurant: restaurant },
          (error: any, valoracion: any) => {
            if (error) {
              return reject(error);
            } else {
              resolve({
                messages: "Datos Obtenidos con éxito",
                success: true,
                data: valoracion,
              });
            }
          }
        ).sort({ $natural: -1 });
      });
    } catch (error) {
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getNotifications: (root: any, { Id }) => {
    if (Id) {
      return new Promise((resolve, reject) => {
        NotificationSchema.find({ user: Id, read: false })
          .populate("users")
          .populate("restaurants")
          .populate("orders")
          .sort({ $natural: -1 })
          .exec((error, notification) => {
            if (error) {
              reject({
                messages: "Hubo un problema con su solicitud",
                success: false,
                notifications: [],
              });
            } else {
              resolve({
                messages: "success",
                success: true,
                notifications: notification,
              });
            }
          });
      });
    } else {
      return null;
    }
  },

  getNotificationsRiders: (root: any, { riders }) => {
    let condition = {
      riders: riders,
      read: false,
      type: {
        $nin: [
          "accept_order",
          "reject_order_riders",
          "order_process",
          "finish_order",
          "valored_order",
          "resolution_order",
        ],
      },
    };

    if (riders) {
      return new Promise((resolve, reject) => {
        NotificationSchema.find(condition)
          .populate("users")
          .populate("restaurants")
          .populate("orders")
          .sort({ $natural: -1 })
          .exec((error, notification) => {
            if (error) {
              reject({
                messages: "Hubo un problema con su solicitud",
                success: false,
                notifications: [],
              });
            } else {
              resolve({
                messages: "success",
                success: true,
                notifications: notification,
              });
            }
          });
      });
    } else {
      return null;
    }
  },

  getOpinion: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      OpinionSchema.find({ plato: id }, (err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      }).sort({ $natural: -1 });
    });
  },

  getPago: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      pagoSchema.findOne({ restaurantID: id }, (error: any, pagos: any) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
            data: null,
          });
        else
          resolve({
            messages: "Operacion realizada con éxito",
            success: true,
            data: pagos,
          });
      });
    });
  },

  getTransaction: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      transSchema
        .find({ restaurantID: id })
        .sort({ $natural: -1 })
        .exec((error, deposito) => {
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
              list: [],
            });
          else
            resolve({
              messages: "solicitud procesada con éxito",
              success: true,
              list: deposito,
            });
        });
    });
  },

  getStatistics: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      let matchQuery1 = {
        restaurant: id,
        estado: {
          $in: ["Entregada", "Valorada"],
        },
      };

      let matchQuery2 = {
        restaurant: id,
        estado: "Rechazada",
      };

      orderSchema
        .aggregate([
          { $match: matchQuery1 },
          {
            $group: {
              _id: {
                month: { $substr: ["$created_at", 5, 2] },
              },
              paypalAmount: { $first: "$total" },
              stripeAmount: { $first: "$total" },
              finishedCount: { $sum: 1 },
              created_at: { $first: "$created_at" },
            },
          },
          {
            $project: {
              your_year_variable: { $year: "$created_at" },
              paypalAmount: 1,
              finishedCount: 1,
              stripeAmountDivide: { $divide: [Number("$stripeAmount"), 100] },
            },
          },
          { $match: { your_year_variable: 2021 } },
        ])
        .then((res) => {
          let ordenes = {
            name: "Pedidos",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };
          let ganacias = {
            name: "Total Ventas",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };
          let devoluciones = {
            name: "Pedidos rechazados",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };

          for (let i = 0; i < res.length; i++) {
            let month = res[i]._id.month;
            if (month == "01") {
              ganacias["Ene"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Ene"] = res[i].finishedCount;
            } else if (month == "02") {
              ganacias["Feb"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Feb"] = res[i].finishedCount;
            } else if (month == "03") {
              ganacias["Mar"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Mar"] = res[i].finishedCount;
            } else if (month == "04") {
              ganacias["Abr"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Abr"] = res[i].finishedCount;
            } else if (month == "05") {
              ganacias["May"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["May"] = res[i].finishedCount;
            } else if (month == "06") {
              ganacias["Jun"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Jun"] = res[i].finishedCount;
            } else if (month == "07") {
              ganacias["Jul"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Jul"] = res[i].finishedCount;
            } else if (month == "08") {
              ganacias["Aug"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Aug"] = res[i].finishedCount;
            } else if (month == "09") {
              ganacias["Sep"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Sep"] = res[i].finishedCount;
            } else if (month == "10") {
              ganacias["Oct"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Oct"] = res[i].finishedCount;
            } else if (month == "11") {
              ganacias["Nov"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Nov"] = res[i].finishedCount;
            } else if (month == "12") {
              ganacias["Dic"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Dic"] = res[i].finishedCount;
            }
          }

          orderSchema
            .aggregate([
              { $match: matchQuery2 },
              {
                $group: {
                  _id: {
                    month: { $substr: ["$created_at", 5, 2] },
                    estado: "Rechazada",
                  },

                  returnedCount: { $sum: 1 },
                  created_at: { $first: "$created_at" },
                },
              },
              {
                $project: {
                  your_year_variable: { $year: "$created_at" },
                  returnedCount: 1,
                },
              },
              { $match: { your_year_variable: 2021 } },
            ])
            .then((res1) => {
              for (let i = 0; i < res1.length; i++) {
                let month = res1[i]._id.month;
                if (month == "01") {
                  devoluciones["Ene"] = res1[i].returnedCount;
                } else if (month == "02") {
                  devoluciones["Feb"] = res1[i].returnedCount;
                } else if (month == "03") {
                  devoluciones["Mar"] = res1[i].returnedCount;
                } else if (month == "04") {
                  devoluciones["Abr"] = res1[i].returnedCount;
                } else if (month == "05") {
                  devoluciones["May"] = res1[i].returnedCount;
                } else if (month == "06") {
                  devoluciones["Jun"] = res1[i].returnedCount;
                } else if (month == "07") {
                  devoluciones["Jul"] = res1[i].returnedCount;
                } else if (month == "08") {
                  devoluciones["Aug"] = res1[i].returnedCount;
                } else if (month == "09") {
                  devoluciones["Sep"] = res1[i].returnedCount;
                } else if (month == "10") {
                  devoluciones["Oct"] = res1[i].returnedCount;
                } else if (month == "11") {
                  devoluciones["Nov"] = res1[i].returnedCount;
                } else if (month == "12") {
                  devoluciones["Dic"] = res1[i].returnedCount;
                }
              }
              resolve({
                success: true,
                message: "",
                data: [ordenes, ganacias, devoluciones],
              });
            });
        })
        .catch((err) => {});
    });
  },

  getPost: async (root: any, { country }) => {
    return new Promise((resolve, reject) => {
      postSchema
        .findOne({ country: country }, (error: any, post: any) => {
          if (error)
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              message: "Operacion realizada con éxito",
              data: post,
            });
        })
        .sort({ $natural: -1 });
    });
  },

  getPosts: async (root: any, { country, page }) => {
    return new Promise((resolve, reject) => {
      postSchema
        .find({ country: country }, (error: any, post: any) => {
          if (error)
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              message: "Operacion realizada con éxito",
              data: post,
            });
        })
        .limit(20 * 1)
        .skip((page - 1) * 20)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getPostsForTags: async (root: any, { country, page, tag }) => {
    return new Promise((resolve, reject) => {
      postSchema
        .find(
          { country: country, tags: { $all: [tag] } },
          (error: any, post: any) => {
            if (error)
              reject({
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
              });
            else
              resolve({
                success: true,
                message: "Operacion realizada con éxito",
                data: post,
              });
          }
        )
        .limit(20 * 1)
        .skip((page - 1) * 20)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getPostbyId: async (root: any, { slug, country }) => {
    return new Promise((resolve, reject) => {
      postSchema.findOne(
        { slug: slug, country: country },
        (error: any, post: any) => {
          if (error)
            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              message: "Operacion realizada con éxito",
              data: post,
            });
        }
      );
    });
  },

  getCustomOrder: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        userID: id,
        estado: {
          $in: [
            "Pagado",
            "Asignada",
            "Aceptada",
            "En camino",
            "Entregado",
            "Rechazada por el rider",
            "Rechazada",
            "Valorada",
          ],
        },
      };

      customorderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              messages: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              messages: "Operacion realizada con éxito",
              data: order,
            });
        })
        .sort({ $natural: -1 });
    });
  },

  getCustomOrderRider: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        riders: id,
        estado: {
          $in: [
            "Pagado",
            "Asignada",
            "Aceptada",
            "En camino",
            "Entregado",
            "Valorada",
          ],
        },
      };

      customorderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              messages: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              messages: "Operacion realizada con éxito",
              data: order,
            });
        })
        .sort({ $natural: -1 });
    });
  },

  getCustomOrderAdmin: async (root: any, { usuarioActual }) => {
    const isAdmin = await userAdminSchema.find({ _id: usuarioActual._id });

    if (isAdmin) {
      return new Promise((resolve, reject) => {
        let condition = {
          estado: { $ne: "Pendiente de pago" },
        };

        customorderSchema
          .find(condition, (error: any, order: any) => {
            if (error)
              reject({
                success: false,
                messages: "Hay un problema con su solicitud",
                data: null,
              });
            else
              resolve({
                success: true,
                messages: "Operacion realizada con éxito",
                data: order,
              });
          })
          .sort({ $natural: -1 });
      });
    } else {
      return {
        success: false,
        messages: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getCity: async (root: any, { city }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        city: city,
      };
      citySchema.findOne(condition, (error: any, city: any) => {
        if (error)
          reject({
            success: false,
            messages: "Aun no hemos llegado a esta zona",
            data: null,
          });
        else
          resolve({
            success: true,
            messages: "Ciudad operativa",
            data: city,
          });
      });
    });
  },

  getOfferts: async (root: any, { city }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        city: city,
      };
      offerts.find(condition, (error: any, city: any) => {
        if (error)
          reject({
            success: false,
            messages: "Aun no tenemos ofertas en esta zona",
            data: null,
          });
        else
          resolve({
            success: true,
            messages: "Ofertas obtenidas con éxito",
            data: city,
          });
      });
    });
  },

  getCustomers: async (
    root: any,
    { page, limit, email },
    { usuarioActual }
  ) => {
    let condition = {};
    // @ts-ignore
    if (email) condition.email = email;

    const isAdmin = await userAdminSchema.findById({ _id: usuarioActual._id });
    if (isAdmin) {
      return new Promise((resolve, reject) => {
        userSchema
          .find(condition, (error: any, user: any) => {
            if (error)
              reject({
                success: false,
                messages: "Hay un problema con su solicitud",
                data: null,
              });
            else
              resolve({
                success: true,
                messages: "Operacion realizada con éxito",
                data: user,
              });
          })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .sort({ $natural: -1 })
          .exec();
      });
    } else {
      return {
        success: false,
        messages: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getAllCustomers: async (root: any, { email }, { usuarioActual }) => {
    let condition = {};
    // @ts-ignore
    if (email) condition.email = email;

    const isAdmin = await userAdminSchema.findById({ _id: usuarioActual._id });
    if (isAdmin) {
      return new Promise((resolve, reject) => {
        userSchema.find(condition, (error: any, user: any) => {
          if (error)
            reject({
              success: false,
              messages: "Hay un problema con su solicitud",
              data: null,
            });
          else
            resolve({
              success: true,
              messages: "Operacion realizada con éxito",
              data: user,
            });
        });
      });
    } else {
      return {
        success: false,
        messages: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getRiderTransaction: async (root: any, { id, dateRange, page }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        rider: id,
        created_at: {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        },
      };
      riderTransactionScheme
        .find(condition, (error: any, list: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: list,
            });
          }
        })
        .limit(20 * 1)
        .skip((page - 1) * 20)
        .sort({ $natural: -1 })
        .exec();
    });
  },

  getQuincena: async (root: any) => {
    return new Promise((resolve, reject) => {
      quincena
        .findOne((error: any, quincena: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              data: {},
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              data: quincena,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getQuincenaAll: async (root: any) => {
    return new Promise((resolve, reject) => {
      quincena
        .find((error: any, quincena: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: quincena,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getCollection: async (root: any, { store }) => {
    return new Promise((resolve, reject) => {
      collectionSchema.find({ store: store }, (error: any, collection: any) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: [],
          });
        else {
          const collections = collection;
          collections.sort((a, b) =>
            a.sorting > b.sorting ? 1 : b.sorting > a.sorting ? -1 : 0
          );
          resolve({
            success: true,
            message: "Consulta extraida con éxito",
            data: collections,
          });
        }
      });
    });
  },

  getPlatoCollection: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      PlatoSchema.find({ menu: id }, (error: any, plato: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: plato,
          });
        }
      }).limit(3);
    });
  },

  getNewMenu: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      MenusSchema.find({ storeId: id }, (error: any, menu: any) => {
        if (error) {
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: menu,
          });
        }
      });
    });
  },

  getNewProductoSearch: (
    root: any,
    { store, search, page, limit, category }
  ) => {
    let condition = {};
    // @ts-ignore
    if (category) condition.parentId = category;
    // @ts-ignore
    if (search) condition = { $text: { $search: `"\"${search} \""` } };
    // @ts-ignore
    if (store) condition.storeId = store;

    return new Promise((resolve, rejects) => {
      productSchema
        .find(condition, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
    });
  },
};
