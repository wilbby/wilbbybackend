export const datasHoldedCompra = (data: any) => {
  const items = data.platos.map((element) => {
    const price = Number(element.plato.price);
    const item = {
      name: element.plato.title,
      desc: element.plato.ingredientes ? element.plato.ingredientes : "",
      units: element.plato.cant,
      subtotal: price,
    };
    return item;
  });

  return {
    applyContactDefaults: false,
    contactCode:
      data.StoreData && data.StoreData.contactCode
        ? data.StoreData.contactCode
        : "B09603440",
    desc: "Compra partner Wilbby",
    date: data.created_at,
    dueDate: data.created_at,
    notes: "Procentaje de tarifa no incluidos",
    items: items,
    invoiceNum: Number(data.display_id),
    currency: "EUR",
  };
};
