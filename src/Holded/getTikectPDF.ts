import request from "request";
import { key } from "./config";
import fs from "fs";
import orderSchema from "../models/order";

export const getInvoicePDF = (
  id: string,
  orderID: string,
  order_display_ID: number
) => {
  const options = {
    method: "GET",
    url: `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${id}/pdf`,
    headers: { Accept: "application/json", key: key },
  };

  request(options, function (error, respon) {
    if (error) throw new Error(error);
    const dat = JSON.parse(respon.body);

    const pdfBlob = "data:application/pdf;base64," + dat.data;
    const matches = pdfBlob.match(/^data:.+\/(.+);base64,(.*)$/);
    //@ts-ignore
    const ext = matches[1];
    //@ts-ignore
    const base64_data = matches[2];
    const buffer = Buffer.from(base64_data, "base64");

    const filename = `${order_display_ID}.${ext}`;
    const filenameWithPath = `${__dirname}/../../uploads/invoice/${filename}`;

    fs.writeFileSync(filenameWithPath, buffer, "binary");

    orderSchema.findOneAndUpdate(
      { _id: orderID },
      {
        $set: {
          invoiceUrl: `https://api.wilbby.com/assets/invoice/${filename}`,
        },
      },
      (err, order) => {}
    );
  });
};
