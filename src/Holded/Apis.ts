import request from "request";
import { key, url, urlCompra } from "./config";
import { datasHolded } from "./data";
import orderSchema from "../models/order";
import { getInvoicePDF } from "./getTikectPDF";
import { datasHoldedCompra } from "./dataCompra";

export const setOrderToHolded = (data: any) => {
  var options = {
    method: "POST",
    url: url,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      key: key,
    },
    body: JSON.stringify(datasHolded(data)),
  };

  var optionsCompra = {
    method: "POST",
    url: urlCompra,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      key: key,
    },
    body: JSON.stringify(datasHoldedCompra(data)),
  };

  request(options, function (error, response) {
    if (error) throw new Error(error);
    const resp = JSON.parse(response.body);
    var optionsPay = {
      method: "POST",
      url: `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${resp.id}/pay`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        key: key,
      },
      body: JSON.stringify({
        date: data.created_at,
        amount: Number(data.total),
      }),
    };

    request(optionsCompra, function (error, response) {
      if (error) throw new Error(error);
      const respo = JSON.parse(response.body);

      orderSchema.findOneAndUpdate(
        { _id: data._id },
        {
          $set: {
            holdedPartnerID: respo.id,
          },
        },
        (err, order) => {}
      );
    });

    request(optionsPay, function (error, responses) {
      if (error) throw new Error(error);

      const url = `https://api.holded.com/api/invoicing/v1/documents/salesreceipt/${resp.id}/send`;

      const optiones = {
        method: "POST",
        url: url,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          key: key,
        },
        body: JSON.stringify({
          emails: data.userData.email,
          subject: `Wilbby - Factura de tu pedido en ${data.StoreData.title}`,
          message: `Hola ${data.userData.name} aquí tiene los detalle y la factura de tu pedido en Wilbby`,
        }),
      };

      request(optiones, function (error, respon) {
        if (error) throw new Error(error);
      });
    });

    orderSchema.findOneAndUpdate(
      { _id: data._id },
      {
        $set: {
          holdedID: resp.id,
        },
      },
      (err, order) => {}
    );
    getInvoicePDF(resp.id, data._id, data.display_id);
  });
};
