import { Request, Response, Router } from "express";
import menuSchema from "../models/subcollection";
import MenuSchema from "../models/nemu/menu";
import PlatoSchema from "../models/nemu/platos";
const fetch = require("node-fetch");
const Bluebird = require("bluebird");

fetch.Promise = Bluebird;

class MercadonaRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  async createProduct(req: Request, res: Response) {
    const id = req.query;
    const cat = req.query;
    await fetch(
      `https://tienda.mercadona.es/api/categories/${id.id}?lang=es&wh=mad`
    )
      .then((res) => res.json())
      .then((datos) => {
        datos.categories.forEach(function (item: any) {
          item.products.forEach(function (product: any) {
            const produto = new PlatoSchema({
              title: product.display_name,
              ingredientes: `${product.packaging} ${product.price_instructions.unit_size} ${product.price_instructions.reference_format} | ${product.price_instructions.bulk_price}€ /${product.price_instructions.reference_format}`,
              price: product.price_instructions.unit_price,
              imagen: product.thumbnail.replace(
                "?fit=crop&h=300&w=300",
                "?fit=crop&h=600&w=600"
              ),
              restaurant: "5fb24df8c292c23bed148076",
              menu: cat.cat,
              oferta: false,
              popular: false,
              news: product.price_instructions.is_new,
              previous_price: null,
            });
            //produto.save();
            res.status(200).end();
          });
        });
      });
  }

  async createCategory(req: Request, res: Response) {
    fetch("https://tienda.mercadona.es/api/categories/?lang=es&wh=mad1")
      .then((res) => res.json())
      .then((datos) => {
        datos.results[25].categories.forEach(function (item: any) {
          const menu = new menuSchema({
            title: item.name,
            collectiontype: "604e4f5073d989bfa541f879",
          });
          //enu.save();
          res.status(200).end();
        });
      });
  }

  routes() {
    this.router.get("/create-product-mercadona", this.createProduct);
    this.router.get("/create-category-mercadona", this.createCategory);
  }
}

const mercadonaRouter = new MercadonaRouter();
mercadonaRouter.routes();

export default mercadonaRouter.router;
