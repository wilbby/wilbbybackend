export const data = [
  {
    title: "Cubo de Nuggets x24",
    ingredientes:
      "¿No querías Nuggets? ¡Pues toma un cubo entero! Te traemos 24 bocados del mejor pollo, perfecto para compartir y mojar en nuestras deliciosas salsas. Elige 3 gratis entre Barbacoa, Miel Mostaza o Sour Cream. ¡Pídelo y moja!",
    price: "7.90 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1575963714913_600x411px_Bodegones_HOMERIA_CUBO_DIC2019.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nuggets vegetales x9",
    ingredientes:
      "Crujientes, dorados, deliciosos. Los nuggets de toda la vida, con todo su sabor, pero hechos con plantas. Disfruta los nuevos nuggets vegetales en 6 o 9 unidades. Ahora sin aromas, conservantes ni colorantes artificiales.",
    price: "4.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1601987990819_Nuggets_Vegetales_x9_dis.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Duo Bacon Cheddar (1 Carne)",
    ingredientes:
      "Es una realidad: La mejor combinación ha llegado. Con nuestra jugosa carne a la parrilla, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "8.99 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516960992",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Duo Bacon Cheddar",
    ingredientes:
      "Es una realidad, la mejor combinación ha llegado. Con nuestra jugosa carne a la parrilla, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "9.49 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516960996",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Duo Bacon Cheddar Tendercrisp®",
    ingredientes:
      "Es una realidad: La mejor combinación ha llegado. Con pollo Tendercrisp®, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "9.20 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961001",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Whopper® Vegetal",
    ingredientes:
      "100% Whopper. 100% Vegetal. Si eres cero de carne y mucho de plantas, te va a flipar el Whopper® Vegetal. El Whopper® de toda la vida, pero hecho a base de plantas. Eso sí, igualmente a la parrilla. ¿Sabrás diferenciarlo?",
    price: "7.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961060",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú King Selection - Trufada con Setas (Doble)",
    ingredientes:
      "Te presentamos la nueva King Selection, 300g de Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más ...",
    price: "11.99 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961042",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú King Selection - Trufada con Setas (1 carne)",
    ingredientes:
      "Te presentamos la nueva King Selection, Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más adictiva...",
    price: "9.99 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605703853979_Menu-King-Selection-1C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Bites con Queso Camembert x8",
    ingredientes:
      "Crujientes por fuera y cremosos por dentro, la elección perfecta para cualquier momento del día. Si te mola el queso Camembert, 8 unidades tienes que coger.",
    price: "4.70 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605616133890_Camembert_bites_x8.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf83449ca61521f5f3a63a",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "King Selection - Trufada con Setas (Doble)",
    ingredientes:
      "Te presentamos la nueva King Selection, 300g de Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más ...",
    price: "10.10 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605614223678_King-Selection-2C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf842c9ca61521f5f3a63b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú King Selection - Trufada con Setas (Doble)",
    ingredientes:
      "Te presentamos la nueva King Selection, 300g de Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más ...",
    price: "11.99 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961042",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf842c9ca61521f5f3a63b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú King Selection - Trufada con Setas (1 carne)",
    ingredientes:
      "Te presentamos la nueva King Selection, Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más adictiva...",
    price: "9.99 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605703853979_Menu-King-Selection-1C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf842c9ca61521f5f3a63b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "King Selection - Trufada con Setas (1 carne)",
    ingredientes:
      "Te presentamos la nueva King Selection, Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más adictiva...",
    price: "8.10 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605614240150_King-Selection-1C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf842c9ca61521f5f3a63b",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Menú The King Bacon",
    ingredientes:
      "Bacon Lover, esta es tu hamburguesa. Dos deliciosas carnes a la parrilla acompañadas de crujientes lonchas de bacon con queso, tomate y mayonesa de bacon. ¡No te podrás resistir!",
    price: "9.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564392248145_M_The_King_Bacon_2C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú The King Huevo",
    ingredientes:
      "Coronamos la nueva hamburguesa con un huevo. Disfruta de la increíble The KING HUEVO, en su versión doble, con carne a la parrilla, mayonesa, tomate, bacon, cebolla crujiente, ketchup y queso americano. Todo ello coronado por un delicioso huevo frito...",
    price: "9.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564392307184_M_The_King_Huevo_2C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Doble Cheese Bacon XXL®",
    ingredientes:
      "Haz doble tu hamburguesa de queso, añádele bacon y ahora aumenta su tamaño… lo sabemos, impresiona. Carne a la parrilla como nos gusta en BURGER KING, pepinillos, kétchup y mostaza comparten escenario para mostrarte esta obra de arte.",
    price: "9.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564392591421_M_Doble_Cheese_Bacon_XXL.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Big King® XXL",
    ingredientes:
      "Porque siempre se puede pedir más, creemos que esto te dejará satisfecho. Una hamburguesa que da la talla… XXL. Imagina 2 hamburguesas a la parrilla con doble de queso fundido sobre ellas, cebolla en rodajas, lechuga, pepinillos y salsa Big King. No ...",
    price: "9.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564392616596_M_Big_King_XXL.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Doble Whopper®",
    ingredientes:
      "Pensando en los que quieren repetir y no se atreven nace el DOBLE WHOPPER. El tomate, la lechuga y la cebolla fresca son imprescindibles, el pepinillo un componente esencial, la mayonesa y el kétchup los acompañantes perfectos y el doble de carne a l...",
    price: "8.60 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516960986",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Big King®",
    ingredientes:
      "Doble contraste y doble sabor, queso fundido sobre doble de carne jugosa a la parrilla, lechuga, pepinillos y cebolla, bañados en exquisita salsa Big King entre dos panes de sésamo crujiente, ¿se puede pedir más?",
    price: "7.30 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564392639599_M_Big_King.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú The King Bacon (1 Carne)",
    ingredientes:
      "Bacon Lover, esta es tu hamburguesa. Dos deliciosas carnes a la parrilla acompañadas de crujientes lonchas de bacon con queso, tomate y mayonesa de bacon. ¡No te podrás resistir!.",
    price: "8.70 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564392276153_M_The_King_Bacon_1C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú The King Huevo (1 Carne)",
    ingredientes:
      "¡El KING ha llegado! Disfruta de la increíble The KING Steakhouse, en su versión simple o doble, con carne a la parrilla, mayonesa, tomate, bacon, cebolla crujiente, salsa barbacoa y queso americano. Todo ello coronado por un delicioso huevo frito en...",
    price: "8.70 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961011",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Doble Cheese Burger BBQ",
    ingredientes:
      "Es la mezcla perfecta. Doble de carne para los más hambrientos Y doble de queso para los más detallistas. Y con una deliciosa salsa barbacoa. La Doble Cheeseburger BBQ lo tiene todo.",
    price: "7.30 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1596129840432_Menu_Doble_Chees_Burger_BBQ_HD.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Doble Cheese Salad",
    ingredientes:
      "¡Dos veces buena!.Nuestra clásica carne a la parrilla acompañada de fresca lechuga, jugoso tomate, suave queso Cheddar y deliciosa mayonesa. ¡Qué más se puede pedir! Pues doble de carne. Así, doble de buena.",
    price: "7.30 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564393820753_M_Doble_Cheese_Salad.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Duo Bacon Cheddar (1 Carne)",
    ingredientes:
      "Es una realidad: La mejor combinación ha llegado. Con nuestra jugosa carne a la parrilla, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "8.99 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516960992",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Steakhouse",
    ingredientes:
      "Pan TENDERCRISP, mayonesa, cebolla crujiente, lechuga, tomate, salsa BBQ, bacon, queso Cheddar, carne de vacuno más complemento y bebida.",
    price: "8.60 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564392442513_M_Steakhouse.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Duo Bacon Cheddar",
    ingredientes:
      "Es una realidad, la mejor combinación ha llegado. Con nuestra jugosa carne a la parrilla, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "9.49 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516960996",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Whopper® Vegetal",
    ingredientes:
      "100% Whopper. 100% Vegetal. Si eres cero de carne y mucho de plantas, te va a flipar el Whopper® Vegetal. El Whopper® de toda la vida, pero hecho a base de plantas. Eso sí, igualmente a la parrilla. ¿Sabrás diferenciarlo?",
    price: "7.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961060",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Whopper®",
    ingredientes:
      "El Whopper® siempre será nuestro número uno. Jugosa carne de vacuno de estupenda calidad a la parrilla, tomate y lechuga fresca traídos de la huerta murciana, suave cebolla y sabroso pepinillo acompañado de mayonesa y kétchup. No olvides el esponjoso...",
    price: "7.50 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1575551499656_Menu_Whopper_TA.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú King Selection - Trufada con Setas (Doble)",
    ingredientes:
      "Te presentamos la nueva King Selection, 300g de Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más ...",
    price: "11.99 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961042",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú King Selection - Trufada con Setas (1 carne)",
    ingredientes:
      "Te presentamos la nueva King Selection, Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más adictiva...",
    price: "9.99 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605703853979_Menu-King-Selection-1C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf849e9ca61521f5f3a63c",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Menú Chicken Tendercrisp®",
    ingredientes:
      "100% pechuga de pollo empanada como sólo Burger King sabe hacerlo. Lechuga fresca recién traída de la huerta, tomates cortados en el momento y una capa de mayonesa para sumarle aún más sabor dentro de pan crujiente, repetirás seguro…",
    price: "8.49 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564390276214_M_Tendercrisp.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf84f89ca61521f5f3a63d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menu Long Chicken®",
    ingredientes:
      "Uno de los más demandados en nuestros restaurantes, ¿Por qué será?. Pan de semilla alargado que alberga pollo crujiente empanado ligeramente especiado, lechuga y mayonesa. ¡Todo un clásico!",
    price: "7.50 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564391124618_M_Long_Chicken.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf84f89ca61521f5f3a63d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Chicken® Wrap",
    ingredientes:
      "¡La opción más enrollada! Sabrosa tortilla rellena de crujiente de pollo, queso cheddar, tomate y lechuga fresca con un toque de mayonesa.",
    price: "7.45 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516960952",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf84f89ca61521f5f3a63d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Doble Crispy Chicken®",
    ingredientes:
      "Crujiente por fuera, tierno por dentro. El mejor pollo doble con un empanado crujiente y ligeramente picante, tomates recién cortados, lechuga fresca y mayonesa en un pan de semillas recién tostado. Una auténtica obra maestra.",
    price: "7.80 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1595397512372_Menu_Doble_Crispy_Chicken.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf84f89ca61521f5f3a63d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Crispy Chicken®",
    ingredientes:
      "Crujiente por fuera, tierno por dentro. El mejor pollo con un empanado crujiente y ligeramente picante, tomates recién cortados, lechuga fresca y mayonesa en un pan de semillas recién tostado. Una auténtica obra maestra.",
    price: "7.20 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516960958",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf84f89ca61521f5f3a63d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Chicken® Nuggets (x9)",
    ingredientes:
      "Deliciosos, crujientes y dorados Chicken Nuggets… dipéalos en su sabrosa salsa",
    price: "7.00 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564391658997_M_King_Nuggets_x9.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf84f89ca61521f5f3a63d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Duo Bacon Cheddar Tendercrisp®",
    ingredientes:
      "Es una realidad: La mejor combinación ha llegado. Con pollo Tendercrisp®, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "9.20 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961001",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf84f89ca61521f5f3a63d",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "King Jr™",
    ingredientes:
      "King Jr Meal te trae la fórmula perfecta. Un menú completo y diversión sin fin con la sorpresa que esconde en su interior. La mejor opción para cubrir las necesidades tanto de padres como de los más pequeños.",
    price: "4.50",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961067",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85519ca61521f5f3a63e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Patatas Clásicas",
    ingredientes:
      "Las famosas patatas fritas de las que tanto has oído hablar, si estás son, las mejores, las más crujientes, las que tienen más sabor, si las pruebas entenderás el por qué de su fama. Disponibles en varios tamaños.",
    price: "2.75 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558456",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "King Aros de Cebolla® x10",
    ingredientes:
      "Puedes pedirlos como entrante o guarnición, para compartir o para ti solo, son perfectos para todo, son redondos, están sabrosos, crujientes y dorados, solos o acompañados de una de tus salsas favoritas.",
    price: "2.75 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558406",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Alitas de pollo x8",
    ingredientes:
      "Estas alitas de pollo se han ganado su lugar en la carta de BURGER KING. Con un toque picante, este entrante es perfecto para los hambrientos amantes del pollo.",
    price: "4.70 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558403",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Patatas Supreme",
    ingredientes:
      "Ahora puedes acompañar tus menús con las deliciosas Patatas Supreme. Pruébalas además con la nueva salsa Sour Cream.",
    price: "2.75 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1515751854847_Supreme_Sour.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Delicias de Pollo x8",
    ingredientes:
      "100% pollo y 100% ternura. Lo que pide tu cuerpo: auténtica pechuga de pollo que te alimente. Y lo que pide tu mente: el placer de degustar algo muy tierno. Una mezcla perfecta. ¡Una delicia total!",
    price: "4.70 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1536575072916_Delcicias_Pollo_x8.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Delicias de Pollo x4",
    ingredientes:
      "100% pollo y 100% ternura. Lo que pide tu cuerpo: auténtica pechuga de pollo que te alimente. Y lo que pide tu mente: el placer de degustar algo muy tierno. Una mezcla perfecta. ¡Una delicia total!",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1536574866590_Delcicias_Pollo_x4.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Cubo Chili Cheese Bites",
    ingredientes:
      "Un poquito de todo para todos. Porque un día te apetece una cosa y al otro, otra, te traemos el cubo perfecto para compartir: 6 Chicken Nuggets, 6 Alitas de Pollo y 6 Chili Cheese Bites. Y ahora con 3 salsas gratis a elegir entre Barbacoa, Miel Mosta...",
    price: "7.90 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961165",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Chili Cheese Bites x9",
    ingredientes: "Nuevos Chili Cheese Bites con salsa cheddar y jalapeños.",
    price: "4.00 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558416",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Cubo King Aros",
    ingredientes:
      "Un poquito de todo para todos. Porque un día te apetece una cosa y al otro, otra, te traemos el cubo perfecto para compartir: 6 Chicken Nuggets, 6 Alitas de Pollo y 6 King Aros de Cebolla. Y ahora con 3 salsas gratis a elegir entre Barbacoa, Miel Mos...",
    price: "7.90 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961167",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Alitas de pollo x4",
    ingredientes:
      "Estas alitas de pollo se han ganado su lugar en la carta de BURGER KING. Con un toque picante, este entrante es perfecto para los hambrientos amantes del pollo. El pack es de 4 alitas.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/Alitas_x4.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "King Aros de Cebolla® x7",
    ingredientes:
      "Puedes pedirlos como entrante o guarnición, para compartir o para ti solo, son perfectos para todo, son redondos, están sabrosos, crujientes y dorados, solos o acompañados de una de tus salsas favoritas.",
    price: "2.39 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1579087704134_Aros_de_cebolla.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "King Aros de Cebolla® x13",
    ingredientes:
      "Puedes pedirlos como entrante o guarnición, para compartir o para ti solo, son perfectos para todo, son redondos, están sabrosos, crujientes y dorados, solos o acompañados de una de tus salsas favoritas.",
    price: "3.00 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1579087666620_Aros_de_cebolla.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Cubo de Nuggets x24",
    ingredientes:
      "¿No querías Nuggets? ¡Pues toma un cubo entero! Te traemos 24 bocados del mejor pollo, perfecto para compartir y mojar en nuestras deliciosas salsas. Elige 3 gratis entre Barbacoa, Miel Mostaza o Sour Cream. ¡Pídelo y moja!",
    price: "7.90 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1575963714913_600x411px_Bodegones_HOMERIA_CUBO_DIC2019.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Chicken Nuggets x9",
    ingredientes:
      "Nuevos deliciosos, crujientes, dorados… dipealos en su sabrosa salsa. disponibles en 6, 9 ó 20 uds.",
    price: "4.00 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/King_nuggets_x9.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Chicken Nuggets x6",
    ingredientes:
      "Nuevos deliciosos, crujientes, dorados… dipealos en su sabrosa salsa. disponibles en 6, 9 ó 20 uds.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/King_nuggets_x6.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Chili Cheese Bites x6",
    ingredientes: "Nuevos Chili Cheese Bites con salsa cheddar y jalapeños.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1431932432553_Chili_cheese_bites.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nuggets vegetales x9",
    ingredientes:
      "Crujientes, dorados, deliciosos. Los nuggets de toda la vida, con todo su sabor, pero hechos con plantas. Disfruta los nuevos nuggets vegetales en 6 o 9 unidades. Ahora sin aromas, conservantes ni colorantes artificiales.",
    price: "4.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1601987990819_Nuggets_Vegetales_x9_dis.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nuggets vegetales x6",
    ingredientes:
      "Crujientes, dorados, deliciosos. Los nuggets de toda la vida, con todo su sabor, pero hechos con plantas. Disfruta los nuevos nuggets vegetales en 6 o 9 unidades. Ahora sin aromas, conservantes ni colorantes artificiales.",
    price: "3.10 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1601988011141_Nuggets_Vegetales_x6_dis_(2).png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Bites con Queso Camembert x5",
    ingredientes:
      "Crujientes por fuera y cremosos por dentro, la elección perfecta para cualquier momento del día. Si te mola el queso Camembert, ya sabes lo que tienes que hacer.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605616176779_Camembert_bites_x5.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Bites con Queso Camembert x8",
    ingredientes:
      "Crujientes por fuera y cremosos por dentro, la elección perfecta para cualquier momento del día. Si te mola el queso Camembert, 8 unidades tienes que coger.",
    price: "4.70 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605616133890_Camembert_bites_x8.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf85ce9ca61521f5f3a63f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Steakhouse",
    ingredientes:
      "Pan TENDERCRISP®, mayonesa, cebolla crujiente, lechuga, tomate, salsa BBQ, bacon, queso Cheddar, carne de vacuno más complemento y bebida.",
    price: "6.20 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1469541066",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Whopper® Vegetal",
    ingredientes:
      "100% Whopper. 100% Vegetal. Si eres cero de carne y mucho de plantas, te va a flipar el Whopper® Vegetal. El Whopper® de toda la vida, pero hecho a base de plantas. Eso sí, igualmente a la parrilla. ¿Sabrás diferenciarlo?",
    price: "5.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558396",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Doble Cheese Salad",
    ingredientes:
      "¡Dos veces buena!.Nuestra clásica carne a la parrilla acompañada de fresca lechuga, jugoso tomate, suave queso Cheddar y deliciosa mayonesa. ¡Qué más se puede pedir! Pues doble de carne. Así, doble de buena.",
    price: "4.60 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1469541038",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Whopper®",
    ingredientes:
      "El Whopper® siempre será nuestro número uno. Jugosa carne de vacuno de estupenda calidad a la parrilla, tomate y lechuga fresca traídos de la huerta murciana, suave cebolla y sabroso pepinillo acompañado de mayonesa y kétchup. No olvides el esponjoso...",
    price: "4.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558393",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "The King Bacon",
    ingredientes:
      "Bacon Lover, esta es tu hamburguesa. Dos deliciosas carnes a la parrilla acompañadas de crujientes lonchas de bacon con queso, tomate y mayonesa de bacon. ¡No te podrás resistir!",
    price: "6.99 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576060202289_The_King_Bacon_2Carnes.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "The King Huevo",
    ingredientes:
      "¡El KING ha llegado! Disfruta de la increíble The KING Steakhouse, en su versión simple o doble, con carne a la parrilla, mayonesa, tomate, bacon, cebolla crujiente, salsa barbacoa y queso americano. Todo ello coronado por un delicioso huevo frito en...",
    price: "6.99 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576062725426_The_King_Huevo_2Carnes.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Doble Cheese Bacon XXL®",
    ingredientes:
      "Haz doble tu hamburguesa de queso, añádele bacon y ahora aumenta su tamaño… lo sabemos, impresiona. Carne a la parrilla como nos gusta en BURGER KING, pepinillos, kétchup y mostaza comparten escenario para mostrarte esta obra de arte.",
    price: "6.70 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576063004187_Doble_Cheese_Bacon_XXL.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Big King® XXL",
    ingredientes:
      "Porque siempre se puede pedir más, creemos que esto te dejará satisfecho. Una hamburguesa que da la talla… XXL. Imagina 2 hamburguesas a la parrilla con triple de queso fundido sobre ellas, cebolla en rodajas, lechuga, pepinillos y salsa Big King®. N...",
    price: "6.70 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576058369410_Big_king_XXL.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Doble Whopper®",
    ingredientes:
      "Pensando en los que quieren repetir y no se atreven nace el DOBLE WHOPPER. El tomate, la lechuga y la cebolla fresca son imprescindibles, el pepinillo un componente esencial, la mayonesa y el kétchup los acompañantes perfectos y el doble de carne a l...",
    price: "6.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576058105070_Doble_Whopper.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Big King®",
    ingredientes:
      "Doble contraste y doble sabor, queso fundido sobre doble de carne jugosa a la parrilla, lechuga, pepinillos y cebolla, bañados en exquisita salsa Big King entre dos panes de sésamo crujiente, ¿se puede pedir más?",
    price: "4.60 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576065647373_Big_king.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "The King Bacon (1 Carne)",
    ingredientes:
      "Bacon Lover, esta es tu hamburguesa. Dos deliciosas carnes a la parrilla acompañadas de crujientes lonchas de bacon con queso, tomate y mayonesa de bacon. ¡No te podrás resistir!",
    price: "6.40 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576066125875_The_King_Bacon_1Carnes.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "The King Huevo (1 Carne)",
    ingredientes:
      "¡El KING ha llegado! Disfruta de la increíble The KING Steakhouse, en su versión simple o doble, con carne a la parrilla, mayonesa, tomate, bacon, cebolla crujiente, salsa barbacoa y queso americano. Todo ello coronado por un delicioso huevo frito en...",
    price: "6.40 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576066573346_The_King_Huevo_1Carne.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Doble Cheese Burger BBQ",
    ingredientes:
      "Es la mezcla perfecta. Doble de carne para los más hambrientos Y doble de queso para los más detallistas. Y con una deliciosa salsa barbacoa. La Doble Cheeseburger BBQ lo tiene todo.",
    price: "4.60 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1596184148208_Doble_Cheeseburger_BBQ_new_(1).png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Chicken Tendercrisp®",
    ingredientes:
      "100% pechuga de pollo empanada como sólo Burger King sabe hacerlo. Lechuga fresca recién traída de la huerta, tomates cortados en el momento y una capa de mayonesa para sumarle aún más sabor dentro de pan crujiente, repetirás seguro…",
    price: "6.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/Tendercrisp.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Long Chicken®",
    ingredientes:
      "Uno de los más demandados en nuestros restaurantes, ¿Por qué será?. Pan de semilla alargado que alberga pollo crujiente empanado ligeramente especiado, lechuga y mayonesa. ¡Todo un clásico!",
    price: "4.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1579087236693_Long_chicken.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Doble Crispy Chicken®",
    ingredientes:
      "Crujiente por fuera, tierno por dentro. El mejor pollo doble con un empanado crujiente y ligeramente picante, tomates recién cortados, lechuga fresca y mayonesa en un pan de semillas recién tostado. Una auténtica obra maestra.",
    price: "4.80 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1595348155593_Doble-Crispy-Chicken.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Crispy Chicken®",
    ingredientes:
      "Crujiente por fuera, tierno por dentro. El mejor pollo con un empanado crujiente y ligeramente picante, tomates recién cortados, lechuga fresca y mayonesa en un pan de semillas recién tostado. Una auténtica obra maestra.",
    price: "4.60 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/Crispy_chcken.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Chicken® Wrap",
    ingredientes:
      "¡La opción más enrollada! Sabrosa tortilla rellena de crujiente de pollo, queso cheddar, tomate y lechuga fresca con un toque de mayonesa.",
    price: "4.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1576054370367_Chicken_Wrap.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Duo Bacon Cheddar (1 Carne)",
    ingredientes:
      "Es una realidad: La mejor combinación ha llegado. Con nuestra jugosa carne a la parrilla, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "6.60 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1583758149127_DUO_CHEDDAR_BACON_1C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Duo Bacon Cheddar",
    ingredientes:
      "Es una realidad: La mejor combinación ha llegado. Con nuestra jugosa carne a la parrilla, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "7.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1583479556037_DUO_CHEDDAR_BACON_2C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Duo Bacon Cheddar Tendercrisp",
    ingredientes:
      "Es una realidad: La mejor combinación ha llegado. Con pollo Tendercrisp®, doble de queso cheddar y doble de bacon. Acompañada de tomate fresco, salsa cheddar y cebolla crujiente, ¡hasta el pan lleva bacon! Es el gocheo hecho hamburguesa.",
    price: "6.80 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1601360064392_DUO_CHEDDAR_BACON_TENDERCRISP.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "King Selection - Trufada con Setas (Doble)",
    ingredientes:
      "Te presentamos la nueva King Selection, 300g de Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más ...",
    price: "10.10 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605614223678_King-Selection-2C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "King Selection - Trufada con Setas (1 carne)",
    ingredientes:
      "Te presentamos la nueva King Selection, Carne 100% Angus a la parrilla combinada con queso y mayonesa de trufa y, por si fuera poco, setas en salsa de trufa. Acompañado de tomate fresco y lechuga Batavia sobre pan brioche. La hamburguesa más adictiva...",
    price: "8.10 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605614240150_King-Selection-1C.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86239ca61521f5f3a640",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Tarta Oreo®",
    ingredientes:
      "Con OREO® todo está más bueno, incluidas las tartas. Hazle hueco al postre y disfruta de la tarta más top.",
    price: "2.50 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558484",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf867d9ca61521f5f3a641",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Gofre caliente con sirope",
    ingredientes:
      "El postre más goloso de BURGER KING, un delicioso gofre caliente con sirope a elegir. ¿Podrás resistirte?​.",
    price: "2.50 ",
    imagen: "https://gps.burgerkingencasa.es/images/products/GLOVO/Gofre.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf867d9ca61521f5f3a641",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Danonino® Petitdino Fresa",
    ingredientes: "Danonino® Petitdino Fresa",
    price: "1.00 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1473145933905_Danonino_petitdino_fresa.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf867d9ca61521f5f3a641",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Danonino® Petitdino Chocolate",
    ingredientes: "Danonino® Petitdino Chocolate",
    price: "1.00 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1473060392149_Danonino_petitdino_chocolate.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf867d9ca61521f5f3a641",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ben & Jerry,s Cookie Dough",
    ingredientes:
      "Es imposible no dejar espacio para el postre cuando tienes el helado de masa para galletas de Ben & Jerry,s esperándote. Con trozos de masa de galleta y pepitas de chocolate envueltos en delicioso helado de vainilla, serás teletransportado directamen...",
    price: "7.00 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558467",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf867d9ca61521f5f3a641",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ben & Jerry,s Strawberry Cheesecake",
    ingredientes:
      "¿Qué mejor combinación para un helado que uno de los postres más clásicos?. Tu menú tendrá otro sabor cuando pruebes el helado de tarta de queso con fresas de Ben & Jerry,s. ¡Te derretirás por este helado!. (465 ml)",
    price: "7.00 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558469",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf867d9ca61521f5f3a641",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ben & Jerry,s Chocolate Fudge",
    ingredientes:
      "Si eres amante del chocolate, este postre te volverá loco. Ben & Jerry,s Chocolate Fudge Brownie es la elección perfecta para terminar una gran comida. Solo imagina una base de helado de chocolate con trozos de brownie, ¿te resistirás?. (465 ml)",
    price: "7.00 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1597842100359_Chocolate_fudge_brownie.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf867d9ca61521f5f3a641",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Compal Essencial",
    ingredientes: "Compal Essencial",
    price: "1.75 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1473060602417_Essencial_manzana.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf867d9ca61521f5f3a641",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Sour cream",
    ingredientes: "Sour  cream",
    price: "0.60 zł",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1476771663859_Sour_Cream.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86de9ca61521f5f3a642",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Salsa mayonesa",
    ingredientes: "Salsa mahonesa",
    price: "0.60 zł",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1526548938439_HEINZ_SALSA_MAYONNAISE.jpg",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86de9ca61521f5f3a642",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Salsa miel y mostaza",
    ingredientes: "Salsa miel y mostaza",
    price: "0.60 zł",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/miel_y_mostaza.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86de9ca61521f5f3a642",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Salsa barbacoa",
    ingredientes: "Salsa barbacoa",
    price: "0.60 zł",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1526548859864_HEINZ_SALSA_BARBECUE.jpg",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86de9ca61521f5f3a642",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Salsa queso",
    ingredientes: "Salsa queso",
    price: "0.60 zł",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1526548909756_QUESO_DIP.jpg",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf86de9ca61521f5f3a642",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Nuggets vegetales x9",
    ingredientes:
      "Crujientes, dorados, deliciosos. Los nuggets de toda la vida, con todo su sabor, pero hechos con plantas. Disfruta los nuevos nuggets vegetales en 6 o 9 unidades. Ahora sin aromas, conservantes ni colorantes artificiales.",
    price: "4.20 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1601987990819_Nuggets_Vegetales_x9_dis.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87399ca61521f5f3a643",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nuggets vegetales x6",
    ingredientes:
      "Crujientes, dorados, deliciosos. Los nuggets de toda la vida, con todo su sabor, pero hechos con plantas. Disfruta los nuevos nuggets vegetales en 6 o 9 unidades. Ahora sin aromas, conservantes ni colorantes artificiales.",
    price: "3.10 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1601988011141_Nuggets_Vegetales_x6_dis_(2).png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87399ca61521f5f3a643",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Whopper® Vegetal",
    ingredientes:
      "100% Whopper. 100% Vegetal. Si eres cero de carne y mucho de plantas, te va a flipar el Whopper® Vegetal. El Whopper® de toda la vida, pero hecho a base de plantas. Eso sí, igualmente a la parrilla. ¿Sabrás diferenciarlo?",
    price: "5.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558396",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87399ca61521f5f3a643",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Whopper® Vegetal",
    ingredientes:
      "100% Whopper. 100% Vegetal. Si eres cero de carne y mucho de plantas, te va a flipar el Whopper® Vegetal. El Whopper® de toda la vida, pero hecho a base de plantas. Eso sí, igualmente a la parrilla. ¿Sabrás diferenciarlo?",
    price: "7.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1516961060",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87399ca61521f5f3a643",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Coca-Cola Zero®",
    ingredientes:
      "Coca-Cola Zero Azúcar botella 500ml. Envase 100% reciclable.",
    price: "2.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558506",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Coca-Cola®",
    ingredientes:
      "Coca-Cola Sabor Original botella 500ml. Envase 100% reciclable.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1564389399904_Coca_Cola.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Coca-Cola Zero Zero®",
    ingredientes: "Coca-Cola Zero Azúcar Zero Cafeína botella 500ml.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1594237179984_Nuevo_Pet_Coca_Cola_Zero_Zero.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Fanta® Naranja",
    ingredientes: "Fanta Naranja botella 500ml. Envase 100% reciclable.",
    price: "2.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558516",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Fanta® Limón",
    ingredientes: "Fanta Limón botella 500ml. Envase 100% reciclable.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1595918374828_Fanta_Limon_PET_New.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nestea®",
    ingredientes:
      "Nestea Té Negro Limón botella 500ml. Envase 100% reciclable.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1573028001518_nestea.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sprite®",
    ingredientes: "Sprite botella 500ml. Envase 100% reciclable.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1595926622854_Sprite_PET_New.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Trina®",
    ingredientes: "Trina®",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1582115533215_1565091336509_Trina_LoCal.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Agua mineral",
    ingredientes: "Agua mineral",
    price: "1.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1473146317131_agua.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Cerveza sin alcohol",
    ingredientes: "Cerveza sin alcohol",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1562599807914_Cerveza_sin_alcohol.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Cerveza",
    ingredientes: "Cerveza Grande",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1465453882086_Cerveza.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Caprisun Tropical",
    ingredientes: "Capri-Sun Fruit Crush Tropical bolsita 200 ml",
    price: "1.20 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/1448558490",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Aquarius Limón 500ml",
    ingredientes: "Aquarius Limón botella 500ml.",
    price: "2.95 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1606720505146_Nuevo_Pet_Aquarius_limon.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Monster Energy 500 ml",
    ingredientes: "Monster Energy Original lata 500ml.",
    price: "3.50 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1605864301832_Moster_Energy_500ml.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87a79ca61521f5f3a644",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Cheeseburger Sin Gluten",
    ingredientes:
      "¡Por fin ha llegado la Cheeseburger libre de Gluten! Disfruta de nuestro rico pan sin gluten, con la deliciosa carne Whooper y 2 lonchas de queso americano.",
    price: "3.80 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1587629214409_Hamburguesa_con_Queso_sin_gluten_Logo_New.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87fd9ca61521f5f3a645",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Burger Sin Gluten",
    ingredientes:
      "¡Por fin ha llegado la Burger libre de Gluten! Disfruta de nuestro rico pan sin gluten, con la deliciosa carne Whooper.",
    price: "3.80 ",
    imagen:
      "https://gps.burgerkingencasa.es/images/products/GLOVO/1587629174355_Hamburguesa_sin_gluten_Logo_New.png",
    restaurant: "5fdf827d9ca61521f5f3a638",
    menu: "5fdf87fd9ca61521f5f3a645",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
];
