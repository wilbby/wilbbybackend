export const data = [
  {
    title: "Bayetas microfibra multiusos  31 x 32 cm. 3 uds.",
    ingredientes:
      "Bayetas microfibra multiusos PRODUCTO ALCAMPO 31 x 32 cm 3 uds.",
    price: "2 грн.",
    imagen: "https://www.alcampo.es/media/he8/hbd/9633118257182.jpg",
    restaurant: "5fbbbf7978e59bc06c66178a",
    menu: "5fbd50054d2a7d0a69b10a39",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Bolsas de basura 30 l.  20 uds.",
    ingredientes:
      "Bolsas de basura, lila con autocierre y aroma lavanda capacidad 30 litros PRODUCTO ALCAMPO 20 uds.",
    price: "1 грн.",
    imagen: "https://www.alcampo.es/media/h75/h6e/9152859504670.jpg",
    restaurant: "5fbbbf7978e59bc06c66178a",
    menu: "5fbd50054d2a7d0a69b10a39",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Estropajo fibra verde con esponja 3 uds.",
    ingredientes: "Estropajo con fibra verde PRODUCTO ALCAMPO 3 uds.",
    price: "1 грн.",
    imagen: "https://www.alcampo.es/media/h54/h98/9633126875166.jpg",
    restaurant: "5fbbbf7978e59bc06c66178a",
    menu: "5fbd50054d2a7d0a69b10a39",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Fregona microfibra",
    ingredientes:
      "Fregona de microfibra bicolor suave y absorbente PRODUCTO ALCAMPO 1 ud.",
    price: "1 грн.",
    imagen: "https://www.alcampo.es/media/h2c/h71/9260130762782.jpg",
    restaurant: "5fbbbf7978e59bc06c66178a",
    menu: "5fbd50054d2a7d0a69b10a39",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Guantes de talla grande  10 uds.",
    ingredientes:
      "Guantes de látex finos talla grande 8-8 1/2 PRODUCTO ALCAMPO 10 uds.",
    price: "1 грн.",
    imagen: "https://www.alcampo.es/media/h22/h26/8876005163038.jpg",
    restaurant: "5fbbbf7978e59bc06c66178a",
    menu: "5fbd50054d2a7d0a69b10a39",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
];
