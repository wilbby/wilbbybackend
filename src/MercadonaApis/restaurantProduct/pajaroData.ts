export const data = [
  {
    title: "Oferta 1",
    ingredientes: "5 döner kebab+5 latas de bebida+2 patatas",
    price: "25.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b883fd52769208751a5",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Oferta 2",
    ingredientes: "5 dürüm+5 latas de bebida+2 patatas",
    price: "32.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b883fd52769208751a5",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Hamburguesa",
    ingredientes: "Hamburguesa",
    price: "7.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Döner Kebab",
    ingredientes: "Döner kebab",
    price: "7.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Perrito",
    ingredientes: "Perrito",
    price: "7.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Alitas",
    ingredientes: "Alitas",
    price: "7.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Pizza",
    ingredientes: "Pizza lahmacun",
    price: "8.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Dürüm",
    ingredientes: "Dürüm+Patatas fritas+Bebida",
    price: "8.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Dürum Sólo Carne",
    ingredientes: "Dürum+Patatas fritas+Bebida",
    price: "9.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Dürum Mixto",
    ingredientes: "Dürum+Patatas fritas+Bebida",
    price: "9.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Familiar Kebab",
    ingredientes: "4 döner kebab+2 patatas fritas+Bebida (2 lt.)",
    price: "18.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Familiar Combi",
    ingredientes: "2 döner kebab+2 dürüm+2 patatas fritas+Bebida (2 lt.)",
    price: "20.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Familiar Dürüm",
    ingredientes: "4 dürüm+2 patatas fritas+Bebida (2 lt.)",
    price: "21.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Familiar Dürum Sólo Carne",
    ingredientes: "4 dürüm+2 patatas fritas+Bebida (2lt.)",
    price: "22.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Menú Familiar Dürum Mixto",
    ingredientes: "4 dürüm+2 patatas fritas+Bebida (2 lt.)",
    price: "22.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097b993fd52769208751a6",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Hamburguesa de Pollo",
    ingredientes:
      "Hamburguesa de pollo, pan de hamburguesa, tomate, lechuga  y queso",
    price: "4.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097ba13fd52769208751a7",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Hamburguesa Doble",
    ingredientes: "",
    price: "5.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097ba13fd52769208751a7",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Perrito",
    ingredientes:
      "Salchicha de ternera, pan hot dog, lechuga, cebolla, tomate y nuestras salsas especiales",
    price: "3.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097ba73fd52769208751a8",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Plato",
    ingredientes:
      "Plato de carne de ternera o pollo, patatas fritas, lechuga, tomate, cebolla y salsas",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bae3fd52769208751a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Plato de Faláfel con Arroz",
    ingredientes:
      "Plato de arroz con puré de garbanzos fritos, lechuga, tomate, cebolla y nuestras sabrosas salsas",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bae3fd52769208751a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Plato de Carne con Arroz",
    ingredientes:
      "Plato de carne de pollo o ternera con arroz y nuestras sabrosas salsas",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bae3fd52769208751a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Plato de Mixto",
    ingredientes:
      "Plato de carne de pollo y ternera, patatas fritas, lechuga, aceitunas, tomate, cebolla y sabrosas salsas",
    price: "6.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bae3fd52769208751a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Plato Solo Carne",
    ingredientes: "",
    price: "6.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bae3fd52769208751a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Plato Extra de Carne",
    ingredientes: "",
    price: "6.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bae3fd52769208751a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Plato Extra de Queso",
    ingredientes: "",
    price: "6.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bae3fd52769208751a9",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Döner Kebab",
    ingredientes:
      "Carne con pan crujiente turco, lechuga, tomate, cebolla y nestras sabrosas salsas",
    price: "4.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bb53fd52769208751aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Kebab Vegetal",
    ingredientes:
      "Pan crujiente con queso de oveja, acompañada de lechuga, tomate y nestras salsas especiales",
    price: "4.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bb53fd52769208751aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Kebab de Faláfel",
    ingredientes:
      "Puré de garbanzos frito con lechuga, tomate, cebolla y nuestras sabrosas salsas",
    price: "4.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bb53fd52769208751aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Kebab Mixto",
    ingredientes:
      "Carne de pollo y ternera con pan crujiente turco, lechuga, tomate, cebolla y nuestras sabrosas salsas",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bb53fd52769208751aa",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Lahmacun Vegetal",
    ingredientes:
      "Pizza turca enrollada de fresca lechuga, queso, tomate, cebolla y nuestras sabrosas salsas",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bbc3fd52769208751ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Lahmacun",
    ingredientes:
      "Pizza turca enrollada con carne de ternera o pollo, lechuga, tomate, cebolla y  salsas",
    price: "5.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bbc3fd52769208751ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Lahmacun Mixto",
    ingredientes:
      "Pizza turca enrollada de fresca lechuga, pollo y ternera, tomate, cebolla y nuestras sabrosas salsas",
    price: "6.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bbc3fd52769208751ab",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Dürüm",
    ingredientes:
      "Carne de ternera o pollo con lechuga, tomate, cebolla y nuestras sabrosas salsas",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bc33fd52769208751ac",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Dürüm Mixto",
    ingredientes:
      "Carne de pollo y ternera con lechuga, tomate, cebolla y nuestras sabrosas salsas",
    price: "6.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bc33fd52769208751ac",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Dürüm de solo Carne",
    ingredientes: "Solo carne de ternera o pollo y nuestras sabrosas salsas",
    price: "6.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bc33fd52769208751ac",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Patatas",
    ingredientes: "Ketchup y mayonesa",
    price: "2.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Calamares",
    ingredientes: "Calamares fritos",
    price: "3.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Alitas",
    ingredientes: "Alitas de pollo fritas, 6 unidades",
    price: "3.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Aros de Cebolla",
    ingredientes: "",
    price: "3.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Tapa",
    ingredientes: "Patatas fritas, carne y salsas",
    price: "3.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Arroz",
    ingredientes: "Arroz, carne y salsas",
    price: "3.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Tapa Doble",
    ingredientes: "",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Arroz Doble",
    ingredientes: "",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Calamares Doble",
    ingredientes: "",
    price: "5.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bca3fd52769208751ad",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Coca Cola (330 ml.)",
    ingredientes: "",
    price: "1.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pepsi (330 ml.)",
    ingredientes: "",
    price: "1.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Fanta Limón (330 ml.)",
    ingredientes: "",
    price: "1.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Nestea (330 ml.)",
    ingredientes: "",
    price: "1.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Agua Mineral (500 ml.)",
    ingredientes: "",
    price: "1.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Sprite (330 ml.)",
    ingredientes: "",
    price: "1.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Aquarius Limón (330 ml.)",
    ingredientes: "",
    price: "1.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Cerveza Sin Alcohol (330 ml.)",
    ingredientes: "",
    price: "1.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Red Bull (330 ml.)",
    ingredientes: "",
    price: "2.00 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Coca Cola (2 lt.)",
    ingredientes: "",
    price: "2.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Fanta Naranja (2 lt.)",
    ingredientes: "",
    price: "2.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pepsi (2 lt.)",
    ingredientes: "",
    price: "2.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Aquarius Naranja (1.5 lt.)",
    ingredientes: "",
    price: "2.50 ",
    imagen: null,
    restaurant: "5ff1d23d85c1c068931a385c",
    menu: "60097bd23fd52769208751ae",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
];
