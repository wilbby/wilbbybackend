export const data = [
  {
    title: "Triple box",
    ingredientes: "Tus 2 pizzas medianas favoritas y 4 entrantes todo",
    price: "34.50",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/kyovfpjw96jjpghgksof",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f0cdb2f817c5ce3304e",
    previous_price: null,
    oferta: false,
    popular: true,
    news: false,
  },
  {
    title: "Hut menú para 1",
    ingredientes: "Pizza Individual + 2 panes de ajo + bebida (50 cl.)",
    price: "9.45",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/qgwthyavpgffo9wcqal7",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f0cdb2f817c5ce3304e",
    previous_price: null,
    oferta: false,
    popular: true,
    news: false,
  },
  {
    title: "Hut menú para 2",
    ingredientes:
      "1 Pizza mediana + 4 panes de ajo o patatas + 2 bebidas (50 cl.)",
    price: "17.50",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dqejjekfvhhqrzsx7bx7",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f0cdb2f817c5ce3304e",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "2 pizzas medianas",
    ingredientes: "",
    price: "23.90",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/os7ompk1dilyf7hau7vt",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f2fdb2f817c5ce3304f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "3 pizzas medianas",
    ingredientes: "",
    price: "30.00",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dqtc0jyckhpglkfpzldv",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f2fdb2f817c5ce3304f",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Pizza meat lover's",
    ingredientes:
      "Salsa de tomate, carne, pepperoni, jamón york, bacón y extra de queso 100% mozzarella",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/aydaxcpb7mxohikx3txq",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza cheese lover's",
    ingredientes:
      "Salsa de tomate, queso 100% mozzarella, exclusiva mezcla de 4 quesos, mozzarella, queso azul, cheddar y emmental",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/kqf0ekczved54m6t452o",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza veggie lover's",
    ingredientes:
      "Queso 100% mozzarella, cebolla roja, maíz, pimiento, champiñón y tomate en dados",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/d9yhaua7vylbpt0v0oqu",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza barbacoa",
    ingredientes:
      "Salsa Barbacoa, queso 100% mozzarella, carne, cebolla roja y bacon",
    price: "14.95 ",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/lfuo4lbsplio7g8kpfye",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza supreme",
    ingredientes:
      "Queso 100% mozzarella, carne, pepperoni, cebolla roja, pimiento y champiñones",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/vbilyb0o7zufsmm4q45s",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza pollo a la parrilla",
    ingredientes:
      "Doble de pollo a la parrilla, queso 100% mozzarella, champiñón, tomate en dados y maíz",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/yaeskueishxfnq1bqvr4",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza carbonara",
    ingredientes: "Salsa carbonara, jamón york, champiñones y cebolla roja",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/omv4rh8dwhl8xill4smg",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza hawaiana",
    ingredientes:
      "Doble de jamón, doble de piña y extra de queso 100% mozzarella",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/acp3dgoifxry0kpw8qko",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza kebab",
    ingredientes:
      "Salsa de tomate, queso 100% mozzarella, tomate natural en dados, cebolla roja, pollo kebab y salsa especial",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/xippzgruzygtmwm8e1hq",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza americana",
    ingredientes:
      "Salsa de tomate, queso 100% mozzarella, pepperoni, bacón, cebolla roja con extra de pepperoni",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dkmav42gontc2zhifdpd",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza queso de cabra",
    ingredientes:
      "Salsa cremosa, cebolla caramelizada y queso de cabra al horno",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/zyz6lciqssxcyy005uo3",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza marinera",
    ingredientes:
      "Salsa de tomate, queso 100% mozzarella, atún, aceitunas negras y anchoas",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dhprvnfqxozxh3ykmbfn",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza pepperoni lover's",
    ingredientes:
      "Salsa de tomate, doble de queso 100% mozzarella, doble de pepperoni",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/jeiwuh7ozk5uaywr30qb",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pizza Margarita",
    ingredientes: "Salsa de tomate y queso 100% mozzarella",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/rbfrgobl2fnse3k0dbdy",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f49db2f817c5ce33050",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Pizza al gusto",
    ingredientes: "",
    price: "14.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/xpfh79cu2xd69aqciz7x",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f60db2f817c5ce33051",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Ensalada huerta",
    ingredientes:
      "Disfruta sus frescos ingredientes, lechuga, cebolla roja, tomate en dados, maíz, atún, aceitunas negras, servida con aceite, vinagre y sal",
    price: "5.50",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/hn7q7g67z6b61uvzcxdf",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f74db2f817c5ce33052",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Ensalada César",
    ingredientes:
      "Disfruta sus frescos ingredientes, lechuga, pechuga marinada, templada al horno, crutones, queso rallado y salsa César",
    price: "5.50",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/rjvrpr9ziiroh8truuay",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f74db2f817c5ce33052",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Pan de ajo (4 uds.)",
    ingredientes: "Crujiente pan tostado al horno con ajo y perejil",
    price: "2.40",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/cuv6bjegchhy2xp4tbto",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pan de ajo (6 uds.)",
    ingredientes: "Crujiente pan tostado al horno con ajo y perejil",
    price: "3.00",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/tl9jbbzapnvbvygjoj0u",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Combo bebidas",
    ingredientes: "2 bebidas (50 cl.)",
    price: "3.50",
    imagen: null,
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pan de ajo súper supremo (4 uds.)",
    ingredientes:
      "Pan de ajo con queso 100% mozzarella con jamón york o 4 quesos",
    price: "3.70",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/xelmvcic3b8pca3lu1zl",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pan de ajo supremo (6 uds.)",
    ingredientes: "Crujiente pan de ajo con queso 100% mozzarella",
    price: "3.70",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/c4y2riviqjms63kqievp",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Combo pan de ajo supremo (4 uds.) + bebida (50 cl.)",
    ingredientes:
      "Crujiente pan de ajo con queso 100% mozzarella + bebida (50 ml.)",
    price: "3.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/xjogabueylp0umvh4sgr",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Combo patatas grill + bebida (50 cl.)",
    ingredientes: "Deliciosas patatas gajo",
    price: "3.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/hzdwdi9cuyjaq64vs8ba",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Delicias de pollo (5 uds.)",
    ingredientes:
      "Tiernas tiras de auténtica pechuga de pollo rebozada, acompañadas con salsa",
    price: "4.00",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/slnc9e6qa9wcok9sx3kb",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Alitas de pollo (4 uds.)",
    ingredientes: "4 alitas de pollo completas y marinadas, con salsa a elegir",
    price: "4.00",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/nkc2ytqlzfjlzg0khd2w",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Aros de cebolla (5 uds.)",
    ingredientes: "Aros de cebolla con un crujiente rebozado",
    price: "4.00",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/gioxhdsnbzvumikf3r8o",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Fingers de queso (5 uds.)",
    ingredientes:
      "Barritas rellenas de deliciosa mozzarella fundida, acompañadas de una salsa",
    price: "4.00",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/j6rfd9qums0f8dbq6mj2",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Jalapeños (5 uds.)",
    ingredientes:
      "5 pimientos jalapeños ligeramente picantes rellenos de cremoso queso cheddar",
    price: "4.00",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/fbtbtg5jlghkhgybys71",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pan de ajo súper supremo (6 uds.)",
    ingredientes:
      "Pan de ajo con queso 100% mozzarella con jamón york o 4 quesos",
    price: "4.40",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dpujr431kbrcxfct6qzr",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Chicken wings (10 uds.)",
    ingredientes:
      "Alitas y muslos de pollo rebozados y mezclados con tu salsa favorita.",
    price: "7.50",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/o5jjaykyf2a6pjtlbwzt",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8f8edb2f817c5ce33053",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Cookie gigante de chocolate",
    ingredientes:
      "Descubre tu nuevo postre favorito, nuestra súper cookie, una galleta grande cortada en porciones con deliciosas pepitas de chocolate",
    price: "4.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dtv715yirmiz7mg84krt",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8facdb2f817c5ce33054",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Helado Ben & Jerry's (100 ml.)",
    ingredientes: "",
    price: "3.25",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/ilonlfnyjnvirmtvvy1f",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8facdb2f817c5ce33054",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Helado Magnum (440 ml.)",
    ingredientes: "",
    price: "7.50",
    imagen: null,
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8facdb2f817c5ce33054",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Helado Ben & Jerry's (465 ml.)",
    ingredientes: "",
    price: "8.30",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/qgwrpsn60nhrioffign3",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8facdb2f817c5ce33054",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },

  {
    title: "Agua (500 ml.)",
    ingredientes: "",
    price: "1.40",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/mcwuhygi8auqsxaehuyp",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8fc2db2f817c5ce33055",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Lipton (500 ml.)",
    ingredientes: "",
    price: "1.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/vsbkivsio6wm71ksmyoz",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8fc2db2f817c5ce33055",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pepsi (500ml.)",
    ingredientes: "",
    price: "1.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/tofoghqhwyhnpsvpuohh",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8fc2db2f817c5ce33055",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pepsi light (500ml.)",
    ingredientes: "",
    price: "1.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/avpp8m5gpyyh174oolb7",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8fc2db2f817c5ce33055",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Pepsi max (500ml.)",
    ingredientes: "",
    price: "1.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/iq7rzomvg205bdp6lpkp",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8fc2db2f817c5ce33055",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Seven Up (500ml.)",
    ingredientes: "",
    price: "1.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/vvvpkh5zcitbdwaclgln",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8fc2db2f817c5ce33055",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Kas naranja (500ml.)",
    ingredientes: "",
    price: "1.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/dsxnzsdki3hdhxjehbsp",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8fc2db2f817c5ce33055",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
  {
    title: "Kas limón (500ml.)",
    ingredientes: "",
    price: "1.95",
    imagen:
      "https://res.cloudinary.com/glovoapp/w_340,h_170,c_fit,f_auto,q_auto/Products/ytgvaomjfx7q35oehnjl",
    restaurant: "5fb7bc786f268193a45374ee",
    menu: "5fba8fc2db2f817c5ce33055",
    previous_price: null,
    oferta: false,
    popular: false,
    news: false,
  },
];
