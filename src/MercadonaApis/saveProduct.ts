import { Request, Response, Router } from "express";
import PlatoSchema from "../models/nemu/platos";
import { data } from "./restaurantProduct/isla";

class ProductSaveRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  async createProduct(req: Request, res: Response) {
    data.forEach((element) => {
      const produto = new PlatoSchema(element);
      produto
        .save()
        .then(() => {
          res.json({ data: "Product save" });
        })
        .catch(() => res.json({ data: "Error save Product" }));
    });
  }

  routes() {
    this.router.get("/create-products", this.createProduct);
  }
}

const productSaveRouter = new ProductSaveRouter();
productSaveRouter.routes();

export default productSaveRouter.router;
