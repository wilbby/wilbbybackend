import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const customorderSchema = new mongoose.Schema(
  {
    display_id: {
      type: Number,
    },
    userID: {
      type: String,
    },

    origin: {
      type: mongoose.Schema.Types.Mixed,
    },

    destination: {
      type: mongoose.Schema.Types.Mixed,
    },

    city: {
      type: String,
    },

    riders: {
      type: String,
    },

    pagoPaypal: { type: mongoose.Schema.Types.Mixed },

    stripePaymentIntent: { type: mongoose.Schema.Types.Mixed },

    estado: {
      type: String,
      enum: [
        "Pendiente de pago",
        "Pagado",
        "Asignada",
        "Preparando para el envío",
        "En camino",
        "Entregado",
        "Rechazada",
        "Devuelto",
        "Valorada",
      ],
      default: "Pendiente de pago",
    },

    progreso: {
      type: String,
      enum: ["0", "25", "50", "75", "100"],
      default: "25",
    },
    status: {
      type: String,
      enum: ["success", "exception", "normal", "active"],
      default: "active",
    },

    date: {
      type: String,
    },

    nota: {
      type: String,
    },

    total: {
      type: String,
    },

    schedule: {
      type: Boolean,
    },

    distance: {
      type: String,
    },

    product_stimate_price: {
      type: String,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IOrder extends Document {
  time: string;
  created_at: Date;
}

export default mongoose.model<IOrder>("customorder", customorderSchema);
