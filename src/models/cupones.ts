import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const cuponesSchema = new mongoose.Schema({
  clave: { type: String, unique: true },
  descuento: Number,
  tipo: {
    type: String,
    enum: ["porcentaje", "dinero"],
    default: "porcentaje",
  },
});

export interface ICupon extends Document {
  clave: string;
  tipo: String;
  descuento: string;
}

export default mongoose.model<ICupon>("cupones", cuponesSchema);
