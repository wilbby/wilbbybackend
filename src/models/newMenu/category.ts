import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const categorySchema = new mongoose.Schema(
  {
    name: { type: String },
    description: { type: String },
    nameTranslations: { type: mongoose.Schema.Types.Mixed },
    descriptionTranslations: { type: mongoose.Schema.Types.Mixed },
    account: { type: String },
    posLocationId: { type: String },
    posCategoryType: { type: String },
    subCategories: { type: [String] },
    posCategoryId: { type: String },
    imageUrl: { type: String },
    products: { type: [String] },
    menu: { type: String },
    sortedChannelProductIds: { type: [String] },
    subProductSortOrder: { type: [String] },
    subProducts: { type: [String] },
    level: { type: Number },
    availabilities: { type: [String] },
    internalId: { type: String },
    storeId: { type: String },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface ICategoryProduct extends Document {
  name: string;
  description: string;
  nameTranslations: any;
  descriptionTranslations: any;
  account: string;
  posLocationId: string;
  posCategoryType: string;
  subCategories: string[];
  posCategoryId: string;
  imageUrl: string;
  products: string[];
  menu: string;
  sortedChannelProductIds: string[];
  subProductSortOrder: string[];
  subProducts: string[];
  level: number;
  availabilities: string[];
  internalId: string;
  storeId: string;
}

export default mongoose.model<ICategoryProduct>(
  "categoryProduct",
  categorySchema
);
