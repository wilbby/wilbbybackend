import mongoose, { Document, Mongoose } from "mongoose";

mongoose.Promise = global.Promise;

const MenuSchema = new mongoose.Schema(
  {
    menu: { type: String },
    menuId: { type: String },
    description: { type: String },
    menuImageURL: { type: String },
    menuType: { type: Number },
    availabilities: { type: [String] },
    snoozedProducts: { type: mongoose.Schema.Types.Mixed },
    productTags: { type: [Number] },
    currency: { type: Number },
    validations: { type: [String] },
    nestedModifiers: { type: Boolean },
    channelLinkId: { type: String },
    storeId: { type: String },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface INewMenu extends Document {
  menu: string;
  menuId: string;
  description: string;
  menuImageURL: string;
  menuType: { type: Number };
  availabilities: string[];
  snoozedProducts: any;
  productTags: number[];
  currency: number;
  validations: string[];
  nestedModifiers: boolean;
  channelLinkId: string;
  storeId: string;
}

export default mongoose.model<INewMenu>("newmenu", MenuSchema);
