import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const modifierGroupsSchema = new mongoose.Schema(
  {
    name: { type: String },
    description: { type: String },
    nameTranslations: { type: mongoose.Schema.Types.Mixed },
    descriptionTranslations: { type: mongoose.Schema.Types.Mixed },
    account: { type: String },
    location: { type: String },
    productType: { type: Number },
    plu: { type: String },
    priceLevels: { type: mongoose.Schema.Types.Mixed },
    sortOrder: { type: Number },
    deliveryTax: { type: Number },
    takeawayTax: { type: Number },
    multiply: { type: Number },
    posProductId: { type: String },
    posProductCategoryId: { type: [String] },
    subProducts: { type: [String] },
    productTags: { type: [String] },
    posCategoryIds: { type: [String] },
    imageUrl: { type: String },
    max: { type: Number },
    min: { type: Number },
    multiMax: { type: Number },
    capacityUsages: { type: [String] },
    parentId: { type: String },
    snoozed: { type: Boolean },
    subProductSortOrder: { type: [String] },
    internalId: { type: String },
    recomended: { type: Boolean, default: false },
    quantity: { type: Number, default: 1 },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IMenu extends Document {
  name: string;
  description: string;
  nameTranslations: any;
  descriptionTranslations: any;
  account: string;
  location: string;
  productType: number;
  plu: string;
  priceLevels: any;
  sortOrder: number;
  deliveryTax: number;
  takeawayTax: number;
  multiply: number;
  posProductId: string;
  posProductCategoryId: string[];
  subProducts: string[];
  productTags: string[];
  posCategoryIds: string[];
  imageUrl: string;
  max: number;
  min: number;
  multiMax: number;
  capacityUsages: string[];
  parentId: string[];
  snoozed: boolean;
  subProductSortOrder: string[];
  internalId: string;
  recomended: boolean;
  quantity: number;
}

export default mongoose.model<IMenu>("modifierGroups", modifierGroupsSchema);
