import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const EmailsListSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
  },
  name: {
    type: String,
  },
  lastName: {
    type: String,
  },
});

export interface IEmailData extends Document {
  name: string;
  email: String;
  lastName: string;
}

export default mongoose.model<IEmailData>("emailContactList", EmailsListSchema);
