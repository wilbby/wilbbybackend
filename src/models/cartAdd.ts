import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const cartSchema = new mongoose.Schema(
  {
    usuarioId: String,
    PlatoId: String,
    cantd: { type: Number, default: 1 },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface ICart extends Document {
  usuarioId: string;
  restaurantID: String;
  cantd: number;
}

export default mongoose.model<ICart>("cart", cartSchema);
