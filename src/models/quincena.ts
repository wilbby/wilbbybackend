import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const quincenaSchema = new mongoose.Schema({
  fromDate: {
    type: String,
    required: true,
  },
  toDate: {
    type: String,
    required: true,
  },
  numberQuincena: {
    type: Number,
    required: true,
  },
});

export interface ITipo extends Document {
  fromDate: string;
  toDate: String;
  numberQuincena: number;
}

export default mongoose.model<ITipo>("quincena", quincenaSchema);
