import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const adressSchema = new mongoose.Schema({
  formatted_address: {
    type: String,
    required: true,
  },

  puertaPiso: {
    type: String,
  },

  type: {
    type: String,
  },

  usuario: {
    type: String,
  },

  lat: {
    type: String,
  },

  lgn: {
    type: String,
  },
});

export interface IAdress extends Document {
  calle: string;
  cp: String;
  usuario: string;
}

export default mongoose.model<IAdress>("adress", adressSchema);
