import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const orderSchema = new mongoose.Schema(
  {
    display_id: {
      type: Number,
    },
    restaurant: {
      type: String,
    },

    userID: {
      type: String,
    },

    userData: {
      type: mongoose.Schema.Types.Mixed,
    },

    AdreesStoreData: {
      type: mongoose.Schema.Types.Mixed,
    },

    AdreesData: {
      type: mongoose.Schema.Types.Mixed,
    },

    StoreData: {
      type: mongoose.Schema.Types.Mixed,
    },

    adress: {
      type: String,
    },

    adresStore: {
      type: String,
    },

    city: {
      type: String,
    },

    riders: {
      type: String,
    },

    aceptaTerminos: {
      type: Boolean,
    },

    propina: {
      type: Boolean,
    },

    cupon: { type: mongoose.Schema.Types.ObjectId, ref: "cupones" },

    cantidad: { type: Number, default: 1 },

    pagoPaypal: { type: mongoose.Schema.Types.Mixed },

    stripePaymentIntent: { type: mongoose.Schema.Types.Mixed },

    estado: {
      type: String,
      enum: [
        "Pendiente de pago",
        "Pagado",
        "Confirmada",
        "Listo para recoger",
        "Preparando para el envío",
        "En camino",
        "Entregada",
        "Rechazado",
        "Rechazada por la tienda",
        "Rechazada por el rider",
        "Devuelto",
        "Valorada",
        "Resolución",
      ],
      default: "Pendiente de pago",
    },

    progreso: {
      type: String,
      enum: ["0", "25", "50", "75", "100"],
      default: "25",
    },
    status: {
      type: String,
      enum: ["success", "exception", "normal", "active"],
      default: "active",
    },

    platos: {
      type: [mongoose.Schema.Types.Mixed],
    },
    time: {
      type: String,
    },

    nota: {
      type: String,
    },

    total: {
      type: String,
    },

    isvalored: {
      type: Boolean,
      default: false,
    },

    cubiertos: {
      type: Boolean,
      default: false,
    },

    llevar: {
      type: Boolean,
      default: false,
    },
    subtotal: {
      type: String,
    },
    complement: {
      type: String,
    },
    descuentopromo: {
      type: String,
    },
    tarifa: {
      type: String,
    },
    envio: {
      type: String,
    },
    propinaTotal: {
      type: String,
    },
    schedule: {
      type: Boolean,
    },
    holdedID: {
      type: String,
    },

    holdedRidersID: {
      type: String,
    },

    holdedPartnerID: {
      type: String,
    },

    invoiceUrl: {
      type: String,
    },

    reportRiders: {
      type: mongoose.Schema.Types.Mixed,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

orderSchema.virtual("descuento", {
  ref: "cupones",
  localField: "cupon",
  foreignField: "_id",
  justOne: true,
});

export interface IOrder extends Document {
  restaurant: string;
  platos: String;
  time: string;
  created_at: Date;
  StoreData: any;
  riders: string;
  userData: any;
  llevar: boolean;
  AdreesData: any;
  display_id: number;
  propinaTotal: string;
}

export default mongoose.model<IOrder>("order", orderSchema);
