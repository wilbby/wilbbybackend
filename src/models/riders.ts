import mongoose, { Document } from "mongoose";
import bcrypt from "bcryptjs";

mongoose.Promise = global.Promise;

const ridersSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      text: true,
    },
    lastName: {
      type: String,
      required: true,
      text: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },

    city: {
      type: String,
    },

    avatar: {
      type: String,
      default: "defaultAvatar.png",
    },

    rating: {
      type: [Number],
    },

    password: {
      type: String,
      required: true,
      unique: true,
    },

    termAndConditions: {
      type: Boolean,
      required: true,
      default: false,
    },

    isSocial: {
      type: Boolean,
    },

    verifyPhone: {
      type: Boolean,
      default: true,
    },

    isAvalible: {
      type: Boolean,
      default: false,
    },

    telefono: {
      type: String,
    },

    StripeID: {
      type: String,
    },

    OnesignalID: {
      type: String,
    },

    contactCode: {
      type: String,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IRider extends Document {
  name: string;
  lastName: String;
  email: string;
  password: string;
  verifyPhone: boolean;
  OnesignalID: any;
}

// hashear los password antes de guardar
ridersSchema.pre<IRider>("save", function (next) {
  // Si el password no esta hasheado...
  if (!this.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  });
});

export default mongoose.model<IRider>("rider", ridersSchema);
