import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const transactionSchema = new mongoose.Schema(
  {
    rider: {
      type: String,
      required: true,
    },
    km: {
      type: String,
      required: true,
    },
    base: {
      type: String,
    },

    time: {
      type: String,
    },
    order: {
      type: String,
    },
    extra: {
      type: String,
    },

    combinado: {
      type: String,
    },

    lluvia: {
      type: String,
    },

    total: {
      type: String,
    },

    propina: {
      type: String,
    },

    iva: {
      type: String,
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface ITipo extends Document {
  rider: string;
  order: String;
  total: string;
}

export default mongoose.model<ITipo>("transactionRider", transactionSchema);
