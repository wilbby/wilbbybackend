import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const paypalSchema = new mongoose.Schema({
  nonce: {
    type: String,
  },
  payerId: {
    type: String,
  },
  email: {
    type: String,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  phone: {
    type: String,
  },
  userID: {
    type: String,
  },
});

export interface IPaypal extends Document {
  nonce: string;
  email: String;
  payerId: string;
}

export default mongoose.model<IPaypal>("paypal", paypalSchema);
