import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const collectionSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },

  store: {
    type: String,
    required: true,
  },
  sorting: {
    type: Number,
    default: 0,
  },
});

export interface ICategory extends Document {
  title: string;
  image: String;
  description: string;
}

export default mongoose.model<ICategory>("collections", collectionSchema);
