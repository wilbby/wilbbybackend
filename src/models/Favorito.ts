import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const favoritoSchema = new mongoose.Schema(
  {
    usuarioId: String,
    restaurantID: String,
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IFavorite extends Document {
  usuarioId: string;
  restaurantID: String;
}

export default mongoose.model<IFavorite>("favorite", favoritoSchema);
