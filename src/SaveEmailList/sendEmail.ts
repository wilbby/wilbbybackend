const nodemailer = require("nodemailer");
import EmailDayli from "./emailsDaily";

export const SendEmailToList = (maillist: [string]) => {
  maillist.forEach((to) => {
    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      service: "gmail",
      port: 465,
      secure: true,
      auth: {
        user: process.env.EMAIL_ADDRESS,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    const mailOptions = {
      from: process.env.EMAIL_ADDRESS,
      to: to,
      subject: "Hola Sushi Lovers de Wilbby ❤️",
      text:
        "Solo hoy en Wilbby tienes 20% de descuento en Ama Sushi Bar Burgos 🍣 🍱",
      html: EmailDayli(),
    };

    mailOptions.to = to;

    transporter.sendMail(mailOptions);
  });
};
