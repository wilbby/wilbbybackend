import dotenv from "dotenv";
import userSchema from "../models/user";
dotenv.config({ path: "variables.env" });
const braintree = require("braintree");

const gateway = new braintree.BraintreeGateway({
  environment: braintree.Environment.Sandbox,
  merchantId: "sbg9bxfj6z6432qf",
  publicKey: "gj8k74zxw296gh4r",
  privateKey: "15d79f7fd1989d5a77ab1e417b6d6bd9",
});

export const CratePaypalUser = (name, lastName, email, userID) => {
  gateway.customer.create(
    {
      firstName: name,
      lastName: lastName,
      company: "Wilbby",
      email: email,
      phone: "34 664 028 161",
      fax: "34 664 028 161",
      website: "www.wilbby.com",
    },
    (err, result) => {
      if (result.success) {
        return new Promise((resolve, reject) => {
          userSchema.findOneAndUpdate(
            { _id: userID },
            { PaypalID: result.customer.id },
            { new: true },
            (error, _usuario) => {
              if (error) reject(error);
              else resolve(_usuario);
            }
          );
        });
      }
    }
  );
};
