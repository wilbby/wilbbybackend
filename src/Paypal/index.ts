import { Request, Response, Router } from "express";
import paypal from "paypal-rest-sdk";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
import orderSchema from "../models/order";
import cardSchema from "../models/card";
import cartSchema from "../models/cartAdd";
import custonoderSchema from "../models/custonorder";
import {
  client_id,
  client_secret,
  audience,
  grant_type,
  oauthURL,
} from "../webHook/config";
import { setOrderToDeliverect } from "../webHook/Deliverect";
import { setOrderToHolded } from "../Holded/Apis";
import { sendNotification } from "./sendNotificationToStore";
var request = require("request");

var options = {
  method: "POST",
  url: oauthURL,
  headers: {
    "content-type": "application/json",
  },
  body: JSON.stringify({
    client_id: client_id,
    client_secret: client_secret,
    audience: audience,
    grant_type: grant_type,
  }),
};

paypal.configure({
  mode: "live", //sandbox or live  &order=${order}
  client_id: process.env.PAYPALCLIENTID || "",
  client_secret: process.env.PAYPALCLIENTSECRET || "",
});

class PaypalRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/paypal", (req: Request, res: Response) => {
      const { price, order, cart, plato } = req.query;
      let create_payment_json = {
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        redirect_urls: {
          return_url: `https://api.wilbby.com/success?price=${price}&order=${order}&cart=${cart}&plato=${plato}`,
          cancel_url: "https://api.wilbby.com/cancel",
        },
        transactions: [
          {
            amount: {
              currency: "EUR",
              total: req.query.price,
            },
            description: "Locatefit S.L (Wilbby)",
          },
        ],
      };
      // @ts-ignore
      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
          res.redirect("/cancel");
        } else {
          // @ts-ignore
          res.redirect(payment.links[1].href);
        }
      });
    });

    this.router.get("/success", (req: Request, res: Response) => {
      var PayerID = req.query.PayerID;
      var paymentId = req.query.paymentId;
      const { price, order, cart, plato } = req.query;
      var execute_payment_json = {
        payer_id: PayerID,
        transactions: [
          {
            amount: {
              currency: "EUR",
              total: price,
            },
          },
        ],
      };
      // @ts-ignore
      paypal.payment.execute(
        // @ts-ignore
        paymentId,
        execute_payment_json,
        function (error, payment) {
          if (error) {
            res.redirect("/cancel");
          }
          if (payment) {
            orderSchema.findOneAndUpdate(
              { _id: order },
              {
                $set: {
                  estado: "Pagado",
                  progreso: "25",
                  status: "active",
                  pagoPaypal: payment,
                },
              },
              (err, order) => {
                if (!err) {
                  setOrderToHolded(order);
                  if (order?.StoreData.channelLinkId) {
                    request(options, function (error, response) {
                      if (error) throw new Error(error);
                      var resp = JSON.parse(response.body);
                      setOrderToDeliverect(
                        order,
                        resp.access_token,
                        resp.token_type
                      );
                    });
                  } else {
                    sendNotification(
                      order?.StoreData.OnesignalID,
                      "Tienes un nuevo pedido a por todas"
                    );
                  }
                }
                cardSchema
                  .deleteMany({ restaurant: String(cart) })
                  .then(function () {
                    console.log("Data deleted card"); // Success
                  })
                  .catch(function (error) {
                    console.log(error); // Failure
                  });
                cartSchema
                  .deleteMany({ usuarioId: String(plato) })
                  .then(function () {
                    console.log("Data deleted cart"); // Success
                  })
                  .catch(function (error) {
                    console.log(error); // Failure
                  });
                res.render("success");
              }
            );
            console.log("Get Payment Response");
          }
        }
      );
    });
    this.router.get("/cancel", (req: Request, res: Response) => {
      res.render("cancel");
    });

    this.router.get("/paypal-custom-order", (req: Request, res: Response) => {
      const { price, order, currency } = req.query;
      let create_payment_json = {
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        redirect_urls: {
          return_url: `https://api.Wilbby.com/success?price=${price}&order=${order}&currency=${currency}`,
          cancel_url: "https://api.Wilbby.com/cancel",
        },
        transactions: [
          {
            amount: {
              currency: currency ? currency : "EUR",
              total: req.query.price,
            },
            description: "Locatefit S.L (Wilbby)",
          },
        ],
      };
      // @ts-ignore
      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
          res.render("cancel");
        } else {
          res.render("cancel");
          // @ts-ignore
          res.redirect(payment.links[1].href);
        }
      });
    });

    this.router.get("/success", (req: Request, res: Response) => {
      var PayerID = req.query.PayerID;
      var paymentId = req.query.paymentId;
      const { price, order, currency } = req.query;
      var execute_payment_json = {
        payer_id: PayerID,
        transactions: [
          {
            amount: {
              currency: currency ? currency : "EUR",
              total: price,
            },
          },
        ],
      };
      // @ts-ignore
      paypal.payment.execute(
        // @ts-ignore
        paymentId,
        execute_payment_json,
        function (error, payment) {
          if (error) {
            res.render("cancel");
          }
          if (payment) {
            custonoderSchema.findOneAndUpdate(
              { _id: order },
              {
                $set: {
                  estado: "Pagado",
                  progreso: "25",
                  status: "active",
                  pagoPaypal: payment,
                },
              }
            );
          }
        }
      );
    });
    this.router.get("/cancel", (req: Request, res: Response) => {
      res.render("cancel");
    });
  }
}

const paypalRouter = new PaypalRouter();
paypalRouter.routes();

export default paypalRouter.router;
