import { Request, Response, Router } from "express";
import dotenv from "dotenv";
import orderSchema from "../models/order";
import cardSchema from "../models/card";
import cartSchema from "../models/cartAdd";
import paypaSchema from "../models/paypalData";
import userSchema from "../models/user";
import custonoderSchema from "../models/custonorder";
dotenv.config({ path: "variables.env" });
const braintree = require("braintree");

const gateway = new braintree.BraintreeGateway({
  environment: braintree.Environment.Sandbox,
  merchantId: "sbg9bxfj6z6432qf",
  publicKey: "gj8k74zxw296gh4r",
  privateKey: "15d79f7fd1989d5a77ab1e417b6d6bd9",
});

class PaypalRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post("/save-paypal", (req: Request, res: Response) => {
      const data = req.body;
      const newPaypal = new paypaSchema({
        email: data.email,
        firstName: data.firstName,
        lastName: data.lastName,
        nonce: data.nonce,
        payerId: data.payerId,
        phone: data.phone,
        userID: data.userID,
      });
      return new Promise((resolve, object) => {
        newPaypal.save(async (error) => {
          if (error) {
            object(
              res.json({
                success: false,
                message: "Hubo un problema con su solicitud",
                data: null,
              })
            );
          } else {
            resolve(
              res.json({
                success: true,
                message: "Cuenta añadida con éxito",
                data: newPaypal,
              })
            );
          }
        });
      });
    });

    this.router.get("/v1/paypal-account", (req: Request, res: Response) => {
      const { userID } = req.query;
      return new Promise((resolve, rejects) => {
        paypaSchema.find({ userID: userID }, (err, resp) => {
          if (err) {
            rejects(
              res.json({
                messages: "Algo va mas intentalo de nuevo",
                success: false,
                data: {},
              })
            );
          } else {
            resolve(
              res.json({
                messages: "Datos obtenido con éxito",
                success: true,
                data: resp,
              })
            );
          }
        });
      });
    });

    this.router.post("/payment-paypal", (req: Request, res: Response) => {
      const { nonce, amount, orderID, restaurantID, usuarioID } = req.body;
      gateway.transaction.sale(
        {
          amount: amount,
          paymentMethodNonce: nonce,
          options: {
            submitForSettlement: true,
          },
        },
        (err, result) => {
          if (!err) {
            orderSchema.findOneAndUpdate(
              { _id: orderID },
              {
                $set: {
                  estado: "Pagado",
                  progreso: "25",
                  status: "active",
                  pagoPaypal: result,
                },
              },
              (err, order) => {
                cardSchema
                  .deleteMany({ restaurant: String(restaurantID) })
                  .then(function () {
                    console.log("Data deleted card"); // Success
                  })
                  .catch(function (error) {
                    console.log(error); // Failure
                  });
                cartSchema
                  .deleteMany({ usuarioId: String(usuarioID) })
                  .then(function () {
                    console.log("Data deleted cart"); // Success
                  })
                  .catch(function (error) {
                    console.log(error); // Failure
                  });
                res.render("success");
              }
            );
            res.json({ message: "Pago realizado con éxito", success: true });
          } else {
            res.json({
              message: "Algo no va bien intentalo de nuevo",
              success: false,
            });
          }
        }
      );
    });

    this.router.post(
      "/payment-paypal-custom-aorder",
      (req: Request, res: Response) => {
        const { nonce, amount, orderID } = req.body;
        gateway.transaction.sale(
          {
            amount: amount,
            paymentMethodNonce: nonce,
            options: {
              submitForSettlement: true,
            },
          },
          (err, result) => {
            if (result.success) {
              custonoderSchema.findOneAndUpdate(
                { _id: orderID },
                {
                  $set: {
                    estado: "Pagado",
                    progreso: "25",
                    status: "active",
                    pagoPaypal: result,
                  },
                },
                (err, order) => {}
              );
              res.json({ message: "Pago realizado con éxito", success: true });
            } else {
              res.json({
                message: "Algo no va bien intentalo de nuevo",
                success: false,
              });
            }
          }
        );
      }
    );

    this.router.get("/client_token", (req, res) => {
      const { paypalID } = req.query;
      gateway.clientToken.generate(
        { customerId: paypalID },
        (err, response) => {
          if (err) {
            res.json(err);
          } else {
            res.json(response.clientToken);
          }
        }
      );
    });

    this.router.post("/create-paypal-client", (req, res) => {
      const { name, lastName, email, userID } = req.body;
      gateway.customer.create(
        {
          firstName: name,
          lastName: lastName,
          company: "Wilbby",
          email: email,
          phone: "34 664 028 161",
          fax: "34 664 028 161",
          website: "www.wilbby.com",
        },
        (err, result) => {
          if (result.success) {
            return (
              new Promise((resolve, reject) => {
                userSchema.findOneAndUpdate(
                  { _id: userID },
                  { PaypalID: result.customer.id },
                  { new: true },
                  (error, _usuario) => {
                    if (error) reject(error);
                    else resolve(_usuario);
                  }
                );
              }),
              res.json(result.customer.id)
            );
          }
        }
      );
    });
  }
}

const paypalRouter = new PaypalRouter();
paypalRouter.routes();

export default paypalRouter.router;
