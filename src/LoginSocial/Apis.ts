import { Request, Response, Router, NextFunction } from "express";
import userSchema from "../models/user";
import jwt from "jsonwebtoken";
import crypto from "crypto";
import appleSigninAuth from "apple-signin-auth";
import dotenv from "dotenv";
import { SaveEmail } from "../mailJet";
import { welcomeEmail } from "../RecoveryPassword/Email/SendEmail";
import { SaveEmailWilbby } from "../SaveEmailList/saveEmail";
import { twiterr_api_key, twitter_api_secret } from "./config.js";
const request = require("request");
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
dotenv.config({ path: "variables.env" });
var bcrypt = require("bcryptjs");
var passport = require("passport");
require("./passport")();

var createToken = function (auth: any) {
  return jwt.sign(
    {
      id: auth.id,
    },
    process.env.SECRETO || "secretToken",
    {
      expiresIn: 60 * 120,
    }
  );
};

function generateToken(req: any, res: any, next: any) {
  req.token = createToken(req.auth);
  return next();
}

function sendToken(req: any, res: any) {
  res.setHeader("x-auth-token", req.token);
  return res.status(200).send(JSON.stringify(req.user));
}

class SocialRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post("/api/v1/auth/twitter/reverse", function (req, res) {
      request.post(
        {
          url: "https://api.twitter.com/oauth/request_token",
          oauth: {
            oauth_callback: "https%3A%2F%2Flocalhost%3A3000%2Flogin",
            consumer_key: twiterr_api_key,
            consumer_secret: twitter_api_secret,
          },
        },
        function (err, r, body) {
          if (err) {
            //@ts-ignore
            return res.send(500, { message: e.message });
          }
          var jsonStr =
            '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
          res.send(JSON.parse(jsonStr));
        }
      );
    });

    this.router.post(
      "/api/v1/auth/twitter",
      (req, res, next) => {
        request.post(
          {
            url: `https://api.twitter.com/oauth/access_token?oauth_verifier`,
            oauth: {
              consumer_key: twiterr_api_key,
              consumer_secret: twitter_api_secret,
              token: req.query.oauth_token,
            },
            form: { oauth_verifier: req.query.oauth_verifier },
          },
          function (err, r, body) {
            if (err) {
              //@ts-ignore
              return res.send(500, { message: err.message });
            }

            const bodyString =
              '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
            const parsedBody = JSON.parse(bodyString);

            req.body["oauth_token"] = parsedBody.oauth_token;
            req.body["oauth_token_secret"] = parsedBody.oauth_token_secret;
            req.body["user_id"] = parsedBody.user_id;

            next();
          }
        );
      },
      passport.authenticate("twitter-token", { session: false }),
      function (req, res, next) {
        if (!req.user) {
          //@ts-ignore
          return res.send(401, "User Not Authenticated");
        }
        req.auth = {
          id: req.user.id,
        };

        return next();
      },
      generateToken,
      sendToken
    );

    this.router.post(
      "/api/v1/auth/facebook",
      passport.authenticate("facebook-token", { session: false }),
      function (req: any, res: any, next: any) {
        if (!req.user) {
          return res.send(401);
        }
        req.auth = {
          id: req.user.id,
        };

        next();
      },
      generateToken,
      sendToken
    );

    this.router.post(
      "/api/auth/apple-web",
      async function (req: any, res: any, next: any) {
        require("mongoose").model("user").schema.add({
          appleToken: String,
          socialNetworks: String,
          isSocial: Boolean,
        });
        const { id_token, user } = req.body;
        const appleIdTokenClaims = await appleSigninAuth.verifyIdToken(
          id_token,
          {
            nonce: "nonce"
              ? crypto.createHash("sha256").update("nonce").digest("hex")
              : undefined,
          }
        );

        // // check if email exists
        let emailExistsApple = await userSchema.findOne({
          appleToken: appleIdTokenClaims.sub,
        });

        if (emailExistsApple) {
          const nuevoUsuario = emailExistsApple;
          res.json({ nuevoUsuario, token: appleIdTokenClaims.sub });
        } else {
          SaveEmailWilbby(user.email, user.name.firstName, user.name.lastName);
          const nuevoUsuario = new userSchema({
            name: user.name.firstName,
            lastName: user.name.lastName,
            email: user.email,
            password: appleIdTokenClaims.sub,
            isSocial: true,
            appleToken: appleIdTokenClaims.sub,
            socialNetworks: "Apple",
          });

          nuevoUsuario.id = nuevoUsuario._id;

          nuevoUsuario.save(async (error: any) => {
            if (error) {
              return res.json(error);
            } else {
              await stripe.customers.create(
                {
                  name: user.name.firstName,
                  email: user.email,
                  description: "Clientes de Wilbby",
                },
                function (err: any, customer: any) {
                  userSchema.findOneAndUpdate(
                    { _id: nuevoUsuario._id },
                    {
                      $set: {
                        StripeID: customer.id,
                      },
                    },
                    (err) => {
                      if (err) {
                        console.log(err);
                      }
                    }
                  );
                }
              );
              return res.json({ nuevoUsuario, token: appleIdTokenClaims.sub });
            }
          });
        }
      }
    );

    this.router.post(
      "/api/v1/auth/google",
      passport.authenticate("google-token", { session: false }),
      function (req: any, res: any, next: any) {
        if (!req.user) {
          return res.send(401);
        }
        req.auth = {
          id: req.user.id,
        };

        next();
      },
      generateToken,
      sendToken
    );

    this.router.post(
      "/api/v1/auth/social/mobile",
      async function (req: Request, res: Response, next: NextFunction) {
        require("mongoose").model("user").schema.add({
          appleToken: String,
          socialNetworks: String,
          isSocial: Boolean,
        });
        let data = req.body;
        const tokens = data.token;
        // // check if email exists
        let emailExistsApple = await userSchema.findOne({
          appleToken: data.token,
        });

        if (emailExistsApple) {
          const nuevoUsuario = emailExistsApple;
          res.json({ nuevoUsuario, token: tokens });
        } else {
          SaveEmail(data.email, data.firstName);
          welcomeEmail(data.email, data.firstName);
          // // check if email exists
          let emailExists = await userSchema.findOne({
            email: data.email,
          });

          if (emailExists) {
            bcrypt.genSalt(10, (err: any, salt: any) => {
              if (err) console.log(err);
              bcrypt.hash(data.token, salt, (err: any, hash: any) => {
                if (err) console.log(err);
                userSchema.findOneAndUpdate(
                  { email: data.email },
                  { password: hash },
                  (err, updated) => {
                    if (err) {
                      res.json(err);
                    }
                    let nuevoUsuario = updated;
                    res.json({ nuevoUsuario, token: tokens });
                  }
                );
              });
            });
          } else {
            const nuevoUsuarios = new userSchema({
              name: data.firstName,
              lastName: data.lastName,
              email: data.email,
              password: data.token,
              isSocial: true,
              city: data.city,
              appleToken: data.token,
              socialNetworks: data.provide,
            });

            nuevoUsuarios.id = nuevoUsuarios._id;

            nuevoUsuarios.save(async (error: any) => {
              if (error) {
                return res.json(error);
              } else {
                await stripe.customers.create(
                  {
                    name: data.firstName,
                    email: data.email,
                    description: "Clientes de Wilbby",
                  },
                  function (err: any, customer: any) {
                    userSchema.findOneAndUpdate(
                      { _id: nuevoUsuarios._id },
                      {
                        $set: {
                          StripeID: customer.id,
                        },
                      },
                      (err, customers) => {
                        if (err) {
                          console.log(err);
                        }
                      }
                    );
                  }
                );

                const nuevoUsuario = nuevoUsuarios;
                return res.json({ nuevoUsuario, token: tokens });
              }
            });
          }
        }
      }
    );
  }
}
const socialRouter = new SocialRouter();
socialRouter.routes();

export default socialRouter.router;
