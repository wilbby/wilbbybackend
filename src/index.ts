import express from "express";
import morgan from "morgan";
import helmet from "helmet";
import mongoose from "mongoose";
import compression from "compression";
import cors from "cors";
import jwt from "jsonwebtoken";
import { ApolloServer } from "apollo-server-express";
import path from "path";
import bodyParser from "body-parser";

import indexRouter from "./routers/index.router";
import socialRouter from "./LoginSocial/Apis";
import recoveryRouter from "./RecoveryPassword/Recovery";
import paypalRouter from "./Paypal";
import confirmNumberRouter from "./Twilio/ConfirmNumber";
import useRauter from "./routers/userRouter";
import stripeRouter from "./stripe/stripe";
import mercadonaRouter from "./MercadonaApis/category";
import saveProductRouter from "./MercadonaApis/saveProduct";
import onesignalRouter from "./OneSignal";
import constctRouter from "./routers/constact.router";
import webHookRouter from "./webHook/apis";
import emailRouter from "./Emails/apiEmails";
import StoreApi from "./Api_Stores/nemu_api";
import { resolvers } from "./GraphQL/Resolvers";
import { typeDefs } from "./GraphQL/Schema";
import engines from "consolidate";
import paypal from "./Paypal/paypal";
import { setDistanceToOrder } from "./getDistance/distance";
import menuSchema from "./models/newMenu/products";
import whatsAppRouterRouter from "./whatsApp/index";

/* const setOrders = async () => {
  menuSchema.updateMany(
    { account: "602fc6967ed2c280b3f5a9e7" },
    {
      $set: {
        storeId: "6050ad74d632f526c761a43d",
      },
    },
    () => {}
  );
};

setOrders(); */
/* 
menuSchema
  .deleteMany({ estado: "Pendiente de pago" })
  .then(function () {
    console.log("Data deleted card"); // Success
  })
  .catch(function (error) {
    console.log(error); // Failure
  }); */

class Server {
  public app: express.Application;
  constructor() {
    this.app = express();
    this.config();
    this.router();
  }

  config() {
    const MONGO_URI = process.env.MONGO_URI || "nodata";
    mongoose.set("useFindAndModify", true);
    mongoose
      .connect(MONGO_URI, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      })
      .then((db) => console.log("DB is conected"));
    //Settings
    this.app.set("port", process.env.PORT || 4000);
    //Middleware
    this.app.use(cors());
    this.app.use(morgan("dev"));
    this.app.use(bodyParser.json({ limit: "50mb" }));
    this.app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(function (req, res, next) {
      //to allow cross domain requests to send cookie information.
      //@ts-ignore
      res.header("Access-Control-Allow-Credentials", true);

      // origin can not be '*' when crendentials are enabled. so need to set it to the request origin
      res.header("Access-Control-Allow-Origin", req.headers.origin);

      // list of methods that are supported by the server
      res.header("Access-Control-Allow-Methods", "OPTIONS,GET,PUT,POST,DELETE");

      res.header(
        "Access-Control-Allow-Headers",
        "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, X-XSRF-TOKEN"
      );

      next();
    });
  }

  router() {
    this.app.use(indexRouter);
    this.app.use("/api/user", useRauter);
    this.app.use(socialRouter);
    this.app.use(recoveryRouter);
    this.app.use(confirmNumberRouter);
    this.app.use(paypalRouter);
    this.app.use(paypal);
    this.app.use(mercadonaRouter);
    this.app.use(constctRouter);
    this.app.use(saveProductRouter);
    this.app.use(whatsAppRouterRouter);
    this.app.use(emailRouter);
    this.app.use(webHookRouter);
    this.app.use(StoreApi);
    this.app.engine("ejs", engines.ejs);
    this.app.set("views", "views");
    this.app.set("view engine", "ejs");
    this.app.use(
      "/assets",
      express.static(path.join(__dirname + "/../uploads"))
    );
    this.app.use(stripeRouter);
    this.app.use(onesignalRouter);
    this.app.use(
      bodyParser.urlencoded({
        parameterLimit: 100000,
        limit: "50mb",
        extended: true,
      })
    );
    this.app.use(bodyParser.json({ limit: "50mb", type: "application/json" }));
  }

  start(paht: any) {
    this.app.listen(this.app.get("port"), () => {
      console.log(
        `Server on port http://localhost:${this.app.get("port")}${paht}`
      );
    });
  }
}

const servers = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers["authorization"];

    if (token !== "null") {
      try {
        // verificarl el token que viene del cliente
        //@ts-ignore
        const usuarioActual = await jwt.verify(token, process.env.SECRETO);
        req.usuarioActual = usuarioActual;

        return {
          usuarioActual,
        };
      } catch (err) {
        // console.log('err: ', err);
      }
    }
  },
});

const server = new Server();
servers.applyMiddleware(server);
server.start(servers.graphqlPath);
