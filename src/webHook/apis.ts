import { Router } from "express";
import restaurantSchema from "../models/restaurant";
import order from "../models/order";
import platoSchema from "../models/nemu/platos";
import menuSchema from "../models/nemu/menu";
import riderSchema from "../models/riders";
import AcompananteSchema from "../models/nemu/acompanante";
import { ordenUpdate } from "./ordenUpdate";
import { sendNotification, sendNotificationRider } from "./sendNotification";

import categorySchema from "../models/newMenu/category";
import bundlesSchema from "../models/newMenu/bundles";
import MenuSchema from "../models/newMenu/Menu";
import modifierGroupsSchema from "../models/newMenu/modifierGroups";
import modifiersSchema from "../models/newMenu/modifiers";
import productSchema from "../models/newMenu/products";

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

class WebHookRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post("/api/deliverect/hook", (req, res) => {
      res.json({
        statusUpdateURL:
          "https://api.wilbby.com/api/deliverect/statusUpdateURL",
        menuUpdateURL: "https://api.wilbby.com/api/deliverect/menuUpdateURL",
        disabledProductsURL:
          "https://api.wilbby.com/api/deliverect/disabledProductsURL",
        snoozeUnsnoozeURL:
          "https://api.wilbby.com/api/deliverect/snoozeUnsnoozeURL",
        busyModeURL: "https://api.wilbby.com/api/deliverect/busyModeURL",
      });
      restaurantSchema.findOneAndUpdate(
        { slug: req.body.channelLocationId },
        { channelLinkId: req.body.channelLinkId },
        { new: true },
        (error) => {
          if (error) {
            res.status(400).end();
          } else {
            res.status(200).end();
          }
        }
      );
    });

    this.router.post("/api/deliverect/statusUpdateURL", async (req, res) => {
      const orden = await order.findById({ _id: req.body.channelOrderId });
      const rider = await riderSchema.findById({
        _id: orden?.riders ? orden?.riders : "5fb7de125fd35b9c72ce2423",
      });
      const orderUpdate = () => {
        switch (req.body.status) {
          case 10:
            sendNotification(
              orden?.userData.OnesignalID,
              `${orden?.StoreData.title} Ha recibido tu pedido tu pedido.`
            );
            break;

          case 20:
            ordenUpdate(orden?._id, "Confirmada", "50", "active");
            sendNotification(
              orden?.userData.OnesignalID,
              `${orden?.StoreData.title} Ha confirmado tu pedido.`
            );
            break;

          case 50:
            sendNotification(
              orden?.userData.OnesignalID,
              `${orden?.StoreData.title} esta preparando tu comida en la cocina.`
            );
            break;

          case 60:
            sendNotification(
              orden?.userData.OnesignalID,
              `${orden?.StoreData.title} ya ha terminado de preparar tu pedido en la cocina.`
            );
            break;

          case 70:
            ordenUpdate(orden?._id, "Listo para recoger", "75", "active");
            sendNotification(
              orden?.userData.OnesignalID,
              `${orden?.StoreData.title} tiene tu pedido listo para recoger.`
            );
            break;

          case 80:
            ordenUpdate(orden?._id, "En camino", "75", "active");
            sendNotification(
              orden?.userData.OnesignalID,
              `Tu pedido ha salido de la tienda y va de camino.`
            );
            break;

          case 90:
            ordenUpdate(orden?._id, "Listo para recoger", "75", "active");
            if (!orden?.llevar) {
              sendNotificationRider(
                rider?.OnesignalID,
                `${orden?.StoreData.title} tiene el pedido listo para recoger.`
              );
            }
            sendNotification(
              orden?.userData.OnesignalID,
              `${orden?.StoreData.title} tiene el pedido listo para recoger por el rider.`
            );
            break;

          case 100:
            ordenUpdate(
              orden?._id,
              "Rechazada por la tienda",
              "0",
              "exception"
            );
            sendNotification(
              orden?.userData.OnesignalID,
              `Upps ${orden?.StoreData.title} no ha podido aceptar tu pedido lo sentimos.`
            );
            break;

          case 110:
            ordenUpdate(
              orden?._id,
              "Rechazada por la tienda",
              "0",
              "exception"
            );
            sendNotification(
              orden?.userData.OnesignalID,
              `Upps ${orden?.StoreData.title} no ha podido aceptar tu pedido lo sentimos.`
            );
            break;

          default:
            break;
        }
      };
      orderUpdate();
      res.status(200).end();
    });

    this.router.post("/api/deliverect/menuUpdateURL", (req, res) => {
      req.body.forEach(async (element) => {
        const restaurant = await restaurantSchema.findOne({
          channelLinkId: element.channelLinkId,
        });

        const newMenuInput = {
          menu: element.menu,
          menuId: element.menuId,
          description: element.description,
          menuImageURL: element.menuImageURL,
          menuType: element.menuType,
          availabilities: element.availabilities,
          snoozedProducts: element.snoozedProducts,
          productTags: element.productTags,
          currency: element.currency,
          validations: element.validations,
          nestedModifiers: element.nestedModifiers,
          channelLinkId: element.channelLinkId,
          storeId: restaurant?._id,
        };

        MenuSchema.deleteMany({ storeId: restaurant?._id })
          .then(function () {
            const newMenu = new MenuSchema(newMenuInput);
            newMenu.save();
          })
          .catch(function (error) {
            console.log(error); // Failure
          });

        element.categories.forEach(async (category) => {
          const dataCategory = category;
          dataCategory.internalId = category._id;
          categorySchema
            .deleteMany({ account: category.account })
            .then(function () {
              const newCategory = new categorySchema(dataCategory);
              newCategory.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });

        var products = element.products;
        var Product = Object.keys(products);

        Product.forEach(async (key) => {
          // product for Key
          const product = products[key];

          product.internalId = key;

          productSchema
            .deleteMany({ parentId: product.parentId })
            .then(async function () {
              const newProduct = new productSchema(product);
              newProduct.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });

        var bundles = element.bundles;
        var Bundle = Object.keys(bundles);

        Bundle.forEach(async (key) => {
          const bundle = bundles[key];

          bundle.internalId = key;

          const newBundle = new bundlesSchema(bundle);
          bundlesSchema
            .deleteMany({ parentId: bundle.parentId })
            .then(async function () {
              newBundle.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });

        var modifierGroup = element.modifierGroups;
        var ModifierGroup = Object.keys(modifierGroup);

        ModifierGroup.forEach(async (key) => {
          const modifierGroups = modifierGroup[key];

          modifierGroups.internalId = key;

          const newmodifierGroupsSchema = new modifierGroupsSchema(
            modifierGroups
          );
          modifierGroupsSchema
            .deleteMany({ parentId: modifierGroups.parentId })
            .then(async function () {
              newmodifierGroupsSchema.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });

        var modifier = element.modifiers;
        var Modifier = Object.keys(modifier);

        Modifier.forEach(async (key) => {
          const modifiers = modifier[key];

          modifiers.internalId = key;

          const newmodifier = new modifiersSchema(modifiers);
          modifiersSchema
            .deleteMany({ parentId: modifiers.parentId })
            .then(async function () {
              newmodifier.save();
            })
            .catch(function (error) {
              console.log(error); // Failure
            });
        });
      });

      res.status(200).end();
    });

    this.router.post("/api/deliverect/snoozeUnsnoozeURL", (req, res) => {
      req.body.operations.forEach((element) => {
        element.data.items.forEach((plus) => {
          if (element.action === "snooze") {
            platoSchema.findOneAndUpdate(
              { plu: plus.plu },
              {
                $set: {
                  out_of_stock: true,
                },
              },
              (e, p) => console.log(e, p)
            );
          } else {
            platoSchema.findOneAndUpdate(
              { plu: plus.plu },
              {
                $set: {
                  out_of_stock: false,
                },
              },
              (e, p) => console.log(e, p)
            );
          }
        });
      });
      res.status(200).end();
    });

    this.router.post("/api/deliverect/busyModeURL", (req, res) => {
      restaurantSchema.findOneAndUpdate(
        { channelLinkId: req.body.channelLinkId },
        {
          $set: {
            open: req.body.status === "PAUSED" ? false : true,
          },
        },
        (e, r) => console.log(e, r)
      );
      res.status(200).end();
    });
  }
}

const webHookRouter = new WebHookRouter();
webHookRouter.routes();

export default webHookRouter.router;
