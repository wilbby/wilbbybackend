import ordenSchema from "../models/order";

export const ordenUpdate = (
  orderID: string,
  estado: string,
  progreso: string,
  status: string
) => {
  ordenSchema.findOneAndUpdate(
    { _id: orderID },
    {
      $set: {
        estado: estado,
        progreso: progreso,
        status: status,
      },
    },
    (err, order) => {}
  );
};
