export const dataOrder = (data: any) => {
  const items = data.platos.map((element) => {
    const subItem =
      element &&
      element.complementos.map((sub) => {
        const item = {
          plu: sub.plu,
          name: sub.name,
          price: 0,
          quantity: 1,
          productType: 1,
          remark: "",
          subItems: [],
        };
        return item;
      });
    const item = {
      plu: element.plato.plu,
      name: element.plato.title,
      price: Number(element.plato.price) * 100,
      quantity: element.plato.cant,
      productType: 1,
      remark: "",
      subItems: subItem,
    };
    return item;
  });

  return {
    channelOrderId: data._id,
    channelOrderDisplayId: data.display_id,
    channelLinkId: data.StoreData.channelLinkId,
    by: "Wilbby",
    orderType: data.llevar ? 1 : 2,
    table: "Wilbby",
    channel: 10000,
    pickupTime: data.schedule ? data.time : data.created_at,
    estimatedPickupTime: data.schedule ? data.time : data.created_at,
    deliveryTime: data.schedule ? data.time : data.created_at,
    deliveryIsAsap: true,
    courier: "Wilbby",
    customer: {
      name: `${data.userData.name} ${data.userData.lastName}`,
      companyName: "Wilbby",
      phoneNumber: data.userData.telefono,
      email: data.userData.email,
    },
    deliveryAddress: {
      street: data.AdreesData
        ? data.AdreesData.formatted_address
        : "NO Addres for takeaway",
      streetNumber: data.AdreesData ? data.AdreesData.puertaPiso : "00",
      postalCode: data.AdreesData
        ? data.AdreesData.formatted_address
        : "000000",
      city: data.AdreesData ? data.AdreesData.formatted_address : "No city",
      extraAddressInfo: data.AdreesData ? data.AdreesData.type : "",
    },
    orderIsAlreadyPaid: true,
    payment: { amount: Number(data.total) * 100, type: 3 },
    note: data.nota,
    items: items,
    decimalDigits: 2,
    numberOfCustomers: 1,
    deliveryCost: Number(data.envio) * 100,
    serviceCharge: Number(data.tarifa) * 100,
    discountTotal: 0,
  };
};
