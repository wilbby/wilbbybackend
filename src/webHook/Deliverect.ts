import request from "request";
import { DeliverectApi, ChannelName } from "./config";
import { dataOrder } from "./data";

export const setOrderToDeliverect = (
  data: any,
  token: string,
  token_type: string
) => {
  var options = {
    method: "POST",
    url: `${DeliverectApi}/${ChannelName}/order/${data.StoreData.channelLinkId}`,
    headers: {
      "Content-Type": "application/json",
      Authorization: `${token_type} ${token}`,
    },
    body: JSON.stringify(dataOrder(data)),
  };
  request(options, function (error, response) {
    if (error) throw new Error(error);
    console.log(response.body);
  });
};
